-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 10, 2019 at 09:26 AM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.2.12-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cartridgesavedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `street_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` int(11) NOT NULL,
  `address_type` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `street_name`, `city`, `province`, `postal_code`, `address_type`, `company_name`, `user_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 'Iriyagolla', 'Rambukkana', 'Sabaragamuwa', 710000, '0', 'Roshan', 2, 1, '2019-01-03 18:30:00', '2019-01-03 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `answer`, `date`, `created_at`, `updated_at`) VALUES
(1, 3, 'No...!!!', '2018-12-27', '2018-12-26 23:28:54', '2018-12-27 01:31:31'),
(3, 4, 'eeeer', '2018-12-31', '2018-12-31 02:56:23', '2018-12-31 02:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `value`, `product_id`, `created_at`, `updated_at`) VALUES
(8, 'color_type', 'Green', 2, '2018-12-26 23:27:36', '2018-12-26 23:27:36'),
(9, 'duty_cycle', '4', 2, '2018-12-26 23:27:36', '2018-12-26 23:27:36'),
(10, 'manufacture_part_no', 'sgddg', 2, '2018-12-26 23:27:36', '2018-12-26 23:27:36'),
(11, 'product_part_no', 'dgdg', 2, '2018-12-26 23:27:36', '2018-12-26 23:27:36'),
(12, 'compatibility', 'Compatible', 2, '2018-12-26 23:27:36', '2018-12-26 23:27:36'),
(13, 'related_compatibility', NULL, 2, '2018-12-26 23:27:36', '2018-12-26 23:27:36'),
(14, 'related_products', '', 2, '2018-12-26 23:27:36', '2018-12-26 23:27:36'),
(15, 'color_type', 'Green', 3, '2018-12-28 04:26:49', '2018-12-28 04:26:49'),
(16, 'duty_cycle', 'fhfh', 3, '2018-12-28 04:26:49', '2018-12-28 04:26:49'),
(17, 'manufacture_part_no', 'sgddg', 3, '2018-12-28 04:26:49', '2018-12-28 04:26:49'),
(18, 'product_part_no', 'dgdg', 3, '2018-12-28 04:26:49', '2018-12-28 04:26:49'),
(19, 'compatibility', 'Genuine', 3, '2018-12-28 04:26:49', '2018-12-28 04:26:49'),
(20, 'related_compatibility', '1', 3, '2018-12-28 04:26:49', '2018-12-28 04:26:49'),
(21, 'related_products', '1', 3, '2018-12-28 04:26:49', '2018-12-28 04:26:49'),
(22, 'color_type', 'Red', 1, '2018-12-28 04:27:13', '2018-12-28 04:27:13'),
(23, 'duty_cycle', 'fhfh', 1, '2018-12-28 04:27:13', '2018-12-28 04:27:13'),
(24, 'manufacture_part_no', 'sgddg', 1, '2018-12-28 04:27:13', '2018-12-28 04:27:13'),
(25, 'product_part_no', 'dgdg', 1, '2018-12-28 04:27:13', '2018-12-28 04:27:13'),
(26, 'compatibility', 'Compatible', 1, '2018-12-28 04:27:13', '2018-12-28 04:27:13'),
(27, 'related_compatibility', '3', 1, '2018-12-28 04:27:13', '2018-12-28 04:27:13'),
(28, 'related_products', '', 1, '2018-12-28 04:27:13', '2018-12-28 04:27:13'),
(29, 'color_type', 'Red', 4, '2018-12-28 04:30:16', '2018-12-28 04:30:16'),
(30, 'duty_cycle', 'fhfh', 4, '2018-12-28 04:30:16', '2018-12-28 04:30:16'),
(31, 'manufacture_part_no', 'sgddg', 4, '2018-12-28 04:30:16', '2018-12-28 04:30:16'),
(32, 'product_part_no', 'dgdg', 4, '2018-12-28 04:30:16', '2018-12-28 04:30:16'),
(33, 'compatibility', 'Genuine', 4, '2018-12-28 04:30:16', '2018-12-28 04:30:16'),
(34, 'related_compatibility', '2', 4, '2018-12-28 04:30:16', '2018-12-28 04:30:16'),
(35, 'related_products', '1,3,2', 4, '2018-12-28 04:30:16', '2018-12-28 04:30:16');

-- --------------------------------------------------------

--
-- Table structure for table `attributesets`
--

CREATE TABLE `attributesets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributesets`
--

INSERT INTO `attributesets` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'color_type', 'color_type', '2018-12-26 23:08:30', '2018-12-26 23:08:30'),
(2, 'duty_cycle', 'duty_cycle', '2018-12-26 23:08:30', '2018-12-26 23:08:30'),
(3, 'manufacture_part_no', 'manufacture_part_no', '2018-12-26 23:08:30', '2018-12-26 23:08:30'),
(4, 'product_part_no', 'product_part_no', '2018-12-26 23:08:30', '2018-12-26 23:08:30'),
(5, 'compatibility', 'compatibility', '2018-12-26 23:08:31', '2018-12-26 23:08:31'),
(6, 'related_compatibility', 'related_compatibility', '2018-12-26 23:08:31', '2018-12-26 23:08:31'),
(7, 'related_products', 'related_products', '2018-12-26 23:08:31', '2018-12-26 23:08:31'),
(8, 'color_type', 'color_type', '2018-12-27 02:38:31', '2018-12-27 02:38:31'),
(9, 'duty_cycle', 'duty_cycle', '2018-12-27 02:38:31', '2018-12-27 02:38:31'),
(10, 'manufacture_part_no', 'manufacture_part_no', '2018-12-27 02:38:31', '2018-12-27 02:38:31'),
(11, 'product_part_no', 'product_part_no', '2018-12-27 02:38:31', '2018-12-27 02:38:31'),
(12, 'compatibility', 'compatibility', '2018-12-27 02:38:31', '2018-12-27 02:38:31'),
(13, 'related_compatibility', 'related_compatibility', '2018-12-27 02:38:31', '2018-12-27 02:38:31'),
(14, 'related_products', 'related_products', '2018-12-27 02:38:31', '2018-12-27 02:38:31');

-- --------------------------------------------------------

--
-- Table structure for table `attributeset_catergory`
--

CREATE TABLE `attributeset_catergory` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Hp', 'Hp Printers...', '1545886062.png', '2018-12-26 23:17:42', '2018-12-26 23:17:42'),
(2, 'Brother', 'Brother printers...', '1545886085.png', '2018-12-26 23:18:05', '2018-12-26 23:18:05'),
(3, 'Canon', 'Canon printers...', '1545886119.png', '2018-12-26 23:18:39', '2018-12-26 23:18:39');

-- --------------------------------------------------------

--
-- Table structure for table `cartridges`
--

CREATE TABLE `cartridges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `color_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `reorder_level` int(11) NOT NULL,
  `delivery_type` tinyint(1) NOT NULL,
  `duty_cycle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacture_part_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_part_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `compatibility` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `related_compatibility` int(11) DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `related_products` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maincat_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `maincat_id`, `created_at`, `updated_at`) VALUES
(1, 'ink-cartridges', NULL, '2018-12-26 23:14:04', '2018-12-26 23:14:04'),
(2, 'toner-cartridges', NULL, '2018-12-26 23:14:25', '2018-12-26 23:14:25');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Red', '2018-12-26 23:14:32', '2018-12-26 23:14:32'),
(2, 'Green', '2018-12-26 23:14:34', '2018-12-26 23:14:34'),
(3, 'yellow', '2018-12-26 23:14:36', '2018-12-31 06:25:35');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_rates`
--

CREATE TABLE `delivery_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `town` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_first_kg` double(10,2) NOT NULL,
  `price_after_first_kg` double(10,2) DEFAULT NULL,
  `cod_price_first_kg` double(10,2) NOT NULL,
  `cod_price_after_first_kg` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_rates`
--

INSERT INTO `delivery_rates` (`id`, `town`, `price_first_kg`, `price_after_first_kg`, `cod_price_first_kg`, `cod_price_after_first_kg`, `created_at`, `updated_at`) VALUES
(1, 'Rambukkana', 50.00, 70.00, 60.00, 80.00, '2018-12-26 23:29:12', '2018-12-26 23:29:12'),
(2, 'Kegall', 45.00, 50.00, 55.00, 60.00, '2018-12-27 05:27:22', '2018-12-27 05:27:22');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `name`, `product_id`, `created_at`, `updated_at`) VALUES
(4, 'Features 1', 2, '2018-12-26 23:27:35', '2018-12-26 23:27:35'),
(5, 'Features 2', 2, '2018-12-26 23:27:35', '2018-12-26 23:27:35'),
(6, 'Features', 3, '2018-12-28 04:26:49', '2018-12-28 04:26:49'),
(7, 'Features 1', 1, '2018-12-28 04:27:12', '2018-12-28 04:27:12'),
(8, 'Features 2', 1, '2018-12-28 04:27:12', '2018-12-28 04:27:12'),
(9, 'Features 3', 1, '2018-12-28 04:27:12', '2018-12-28 04:27:12'),
(10, 'Features', 4, '2018-12-28 04:30:16', '2018-12-28 04:30:16');

-- --------------------------------------------------------

--
-- Table structure for table `image_ups`
--

CREATE TABLE `image_ups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_ups`
--

INSERT INTO `image_ups` (`id`, `name`, `alt`, `type`, `created_at`, `updated_at`) VALUES
(1, '1545886062.png', '', 'png', '2018-12-26 23:17:42', '2018-12-26 23:17:42'),
(2, '1545886085.png', '', 'png', '2018-12-26 23:18:05', '2018-12-26 23:18:05'),
(3, '1545886119.png', '', 'png', '2018-12-26 23:18:39', '2018-12-26 23:18:39'),
(4, '1545886283.png', '', 'png', '2018-12-26 23:21:23', '2018-12-26 23:21:23'),
(5, '1545886320.png', '', 'png', '2018-12-26 23:22:00', '2018-12-26 23:22:00'),
(6, '1545886477.jpg', '', 'jpg', '2018-12-26 23:24:37', '2018-12-26 23:24:37'),
(7, '1545886655.jpg', '', 'jpg', '2018-12-26 23:27:35', '2018-12-26 23:27:35'),
(8, '1545991008.jpg', '', 'jpg', '2018-12-28 04:26:49', '2018-12-28 04:26:49'),
(9, '1545991215.jpg', '', 'jpg', '2018-12-28 04:30:16', '2018-12-28 04:30:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_roles_table', 1),
(2, '2014_10_12_000001_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_10_30_071837_cteate_categories_table', 1),
(5, '2018_10_30_193816_create_brands_table', 1),
(6, '2018_11_01_191513_create_printers_table', 1),
(7, '2018_11_27_063334_create_products_table', 1),
(8, '2018_11_27_063334_cteate_cartridges_table', 1),
(9, '2018_11_27_073024_create_colors_table', 1),
(10, '2018_11_27_073833_cteate_pages_table', 1),
(11, '2018_11_27_083809_cteate_features_table', 1),
(12, '2018_11_29_042535_create_orders_table', 1),
(13, '2018_11_29_042540_create_payments_table', 1),
(14, '2018_11_29_045020_create_order_products_table', 1),
(15, '2018_11_29_050646_create_address_table', 1),
(16, '2018_11_29_051216_create_delivery_rates_table', 1),
(17, '2018_11_29_053203_create_questions_table', 1),
(18, '2018_11_29_054002_create_answers_table', 1),
(19, '2018_12_05_052735_create_images_table', 1),
(20, '2018_12_05_054054_create_attributes_table', 1),
(21, '2018_12_05_054108_create_attributesets_table', 1),
(22, '2018_12_05_054208_create_catergory_attributeset_table', 1),
(23, '2018_12_05_055810_create_product_printer_table', 1),
(24, '2018_12_05_061658_create_permissions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `total` double(10,2) NOT NULL,
  `delivery_total` double(10,2) NOT NULL,
  `total_qty` int(11) NOT NULL,
  `vat` double(10,2) NOT NULL,
  `payment_type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `date`, `user_id`, `total`, `delivery_total`, `total_qty`, `vat`, `payment_type`, `note`, `status`, `created_at`, `updated_at`) VALUES
(1, '2019-01-04', 2, 34.00, 5.00, 5, 3.00, 'er', 'esfsfs', 'pending', '2019-01-03 18:30:00', '2019-01-03 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `total` double(10,2) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_charge` double(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `color_id` int(10) UNSIGNED NOT NULL,
  `pages_nos` int(11) NOT NULL,
  `multiply` int(11) NOT NULL,
  `capacity` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `product_id`, `color_id`, `pages_nos`, `multiply`, `capacity`, `created_at`, `updated_at`) VALUES
(4, 2, 1, 1, 1, '1', '2018-12-26 23:27:35', '2018-12-26 23:27:35'),
(5, 3, 3, 1, 1, '1', '2018-12-28 04:26:48', '2018-12-28 04:26:48'),
(6, 1, 1, 2, 3, '4', '2018-12-28 04:27:12', '2018-12-28 04:27:12'),
(7, 1, 2, 1, 2, '3', '2018-12-28 04:27:12', '2018-12-28 04:27:12'),
(8, 1, 3, 6, 5, '4', '2018-12-28 04:27:12', '2018-12-28 04:27:12'),
(9, 4, 2, 4, 3, '2', '2018-12-28 04:30:15', '2018-12-28 04:30:15'),
(10, 4, 1, 1, 2, '3', '2018-12-28 04:30:15', '2018-12-28 04:30:15');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `payhere_amount` double(10,2) NOT NULL,
  `payhere_currency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_code` int(11) DEFAULT NULL,
  `md5sig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_msg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `index` tinyint(1) NOT NULL DEFAULT '1',
  `create` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `role_id`, `edit`, `index`, `create`, `delete`, `created_at`, `updated_at`) VALUES
(65, 'Category', 3, 1, 1, 1, 1, '2018-12-27 02:38:32', '2018-12-27 02:38:32'),
(66, 'Color', 3, 1, 1, 1, 1, '2018-12-27 02:38:32', '2018-12-27 02:38:32'),
(67, 'Brand', 3, 1, 1, 1, 1, '2018-12-27 02:38:32', '2018-12-27 02:38:32'),
(68, 'Printer', 3, 1, 1, 1, 1, '2018-12-27 02:38:32', '2018-12-27 02:38:32'),
(69, 'Product', 3, 1, 1, 1, 1, '2018-12-27 02:38:32', '2018-12-27 02:38:32'),
(70, 'FAQ', 3, 1, 1, 1, 1, '2018-12-27 02:38:32', '2018-12-27 02:38:32'),
(71, 'Rate', 3, 1, 1, 1, 1, '2018-12-27 02:38:32', '2018-12-27 02:38:32'),
(72, 'User', 3, 1, 1, 1, 1, '2018-12-27 02:38:32', '2018-12-27 02:38:32'),
(81, 'Category', 2, 1, 1, 1, 1, '2018-12-27 05:02:53', '2018-12-27 05:02:53'),
(82, 'Color', 2, 1, 1, 1, 1, '2018-12-27 05:02:53', '2018-12-27 05:02:53'),
(83, 'Brand', 2, 1, 1, 1, 1, '2018-12-27 05:02:53', '2018-12-27 05:02:53'),
(84, 'Printer', 2, 1, 1, 1, 1, '2018-12-27 05:02:53', '2018-12-27 05:02:53'),
(85, 'Product', 2, 0, 0, 0, 0, '2018-12-27 05:02:53', '2018-12-27 05:02:53'),
(86, 'FAQ', 2, 0, 1, 1, 1, '2018-12-27 05:02:53', '2018-12-27 05:02:53'),
(87, 'Rate', 2, 1, 1, 1, 0, '2018-12-27 05:02:53', '2018-12-27 05:02:53'),
(88, 'User', 2, 0, 1, 0, 0, '2018-12-27 05:02:53', '2018-12-27 05:02:53'),
(97, 'Category', 1, 1, 1, 1, 1, '2019-01-02 05:41:23', '2019-01-02 05:41:23'),
(98, 'Color', 1, 1, 1, 1, 1, '2019-01-02 05:41:23', '2019-01-02 05:41:23'),
(99, 'Brand', 1, 1, 1, 1, 1, '2019-01-02 05:41:23', '2019-01-02 05:41:23'),
(100, 'Printer', 1, 1, 1, 1, 1, '2019-01-02 05:41:23', '2019-01-02 05:41:23'),
(101, 'Product', 1, 1, 1, 1, 1, '2019-01-02 05:41:23', '2019-01-02 05:41:23'),
(102, 'FAQ', 1, 1, 1, 1, 1, '2019-01-02 05:41:24', '2019-01-02 05:41:24'),
(103, 'Rate', 1, 1, 1, 1, 1, '2019-01-02 05:41:24', '2019-01-02 05:41:24'),
(104, 'User', 1, 1, 1, 1, 1, '2019-01-02 05:41:24', '2019-01-02 05:41:24');

-- --------------------------------------------------------

--
-- Table structure for table `printers`
--

CREATE TABLE `printers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `printer_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `printers`
--

INSERT INTO `printers` (`id`, `name`, `image`, `brand_id`, `category_id`, `description`, `printer_code`, `created_at`, `updated_at`) VALUES
(1, 'HP-7450', '1545886283.png', 1, 1, 'Description...', 'HP-7450', '2018-12-26 23:21:23', '2018-12-26 23:21:23'),
(2, 'Brother-750', '1545886320.png', 2, 2, 'Description...', 'Brother-750', '2018-12-26 23:22:00', '2018-12-26 23:22:00');

-- --------------------------------------------------------

--
-- Table structure for table `printer_product`
--

CREATE TABLE `printer_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `printer_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `printer_product`
--

INSERT INTO `printer_product` (`id`, `printer_id`, `product_id`, `created_at`, `updated_at`) VALUES
(3, 1, 2, NULL, NULL),
(4, 1, 3, NULL, NULL),
(5, 1, 1, NULL, NULL),
(6, 2, 1, NULL, NULL),
(7, 1, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `reorder_level` int(11) NOT NULL,
  `delivery_type` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat` double(10,2) NOT NULL,
  `weight` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category_id`, `image`, `price`, `stock`, `reorder_level`, `delivery_type`, `description`, `url`, `vat`, `weight`, `created_at`, `updated_at`) VALUES
(1, 'Hp-001', 1, '1545886476.jpg', 3.00, 4, 4, 0, 'Description', 'www', 2.00, 600.00, '2018-12-26 23:24:36', '2018-12-26 23:24:36'),
(2, 'PRA-002', 2, '1545886655.jpg', 23.00, 4, 4, 0, 'Description', 'qqq', 3.00, 700.00, '2018-12-26 23:27:35', '2018-12-26 23:27:35'),
(3, 'Hp-003', 1, '1545991007.jpg', 50.00, 50, 5, 0, 'Description', 'eee', 4.00, 800.00, '2018-12-28 04:26:48', '2018-12-28 04:26:48'),
(4, 'Hp-004', 1, '1545991215.jpg', 50.00, 60, 1, 0, 'Description', 'ffff', 5.00, 900.00, '2018-12-28 04:30:15', '2018-12-28 04:30:15');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `approve` tinyint(1) NOT NULL DEFAULT '1',
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `customer_name`, `email`, `date`, `approve`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 'What is this Printer?', NULL, NULL, '2018-12-27', 1, 1, '2018-12-26 23:25:10', '2018-12-26 23:25:10'),
(2, 'That is good?', NULL, NULL, '2018-12-27', 0, 1, '2018-12-26 23:28:00', '2018-12-27 05:26:22'),
(3, 'This product mach all of printers?', NULL, NULL, '2018-12-27', 0, 2, '2018-12-26 23:28:28', '2018-12-31 02:56:33'),
(4, 'khkj', NULL, NULL, '2018-12-31', 1, 2, '2018-12-31 02:39:53', '2018-12-31 02:39:53');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2018-12-27 02:38:30', '2018-12-27 02:38:30'),
(2, 'User', '2018-12-27 02:38:30', '2018-12-27 02:38:30'),
(3, 'Super Admin', '2018-12-27 02:38:30', '2018-12-27 02:38:30'),
(4, 'Customer', '2019-01-04 18:30:00', '2019-01-04 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'superadmin@gmail.com', '$2y$10$CcnqNfbGoXEgQi.gm1piXODj.yM99xXFat85gJrPhW/4vb72uXw4O', 3, NULL, '2018-12-27 02:38:31', '2018-12-27 02:38:31'),
(2, 'Admin', 'admin@gmail.com', '$2y$10$CcnqNfbGoXEgQi.gm1piXODj.yM99xXFat85gJrPhW/4vb72uXw4O', 1, '5wsnGOCVtcRLc4SJUqUS5Utq1MRlankDxsSIhmB4ksWPZelF7pxTlmfczGpj', '2018-12-27 02:38:31', '2018-12-27 02:38:31'),
(5, 'Amila Jasinghe', 'roshanamila@gmail.com', '$2y$10$2AZtjsyF3veIyPTTLtmxNedgRO2Kv8TuSLWXPOqUxL1kHeMyXjnb.', 2, 'KojGCcsnlcu49kr3Y90EqqoudYEIdObuSpFNOhThE89tcxDAtDKGmFrRCqVF', '2018-12-27 02:40:13', '2018-12-27 02:40:13'),
(6, 'Roshan', 'roshandr@gmail.com', '$2y$10$fNrqGDEtfkp/PcRkgA15COVxhnwzErPwiF/yIK8bjotmGDVilNwtK', 2, '1PZxjrXbWo1z1kw2OYdaS8xMGCMZ0ZA8VANinNgJiWTnSWpVScfekaUiVsVw', '2018-12-27 02:40:31', '2018-12-27 02:40:31'),
(7, 'Customer', 'dgdfgfd@gmail.com', '$2y$10$MAhWZFt0Xfty13F5eXNvUOuEXz2ujfqDdiH13N6PxH0A026LXtchi', 4, NULL, '2019-01-05 12:32:27', '2019-01-05 12:32:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `address_user_id_foreign` (`user_id`),
  ADD KEY `address_order_id_foreign` (`order_id`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_question_id_foreign` (`question_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attributes_product_id_foreign` (`product_id`);

--
-- Indexes for table `attributesets`
--
ALTER TABLE `attributesets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributeset_catergory`
--
ALTER TABLE `attributeset_catergory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attributeset_catergory_attribute_id_foreign` (`attribute_id`),
  ADD KEY `attributeset_catergory_category_id_foreign` (`category_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cartridges`
--
ALTER TABLE `cartridges`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cartridges_name_unique` (`name`),
  ADD UNIQUE KEY `cartridges_url_unique` (`url`),
  ADD KEY `cartridges_category_id_foreign` (`category_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_rates`
--
ALTER TABLE `delivery_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `features_product_id_foreign` (`product_id`);

--
-- Indexes for table `image_ups`
--
ALTER TABLE `image_ups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_product_id_foreign` (`product_id`),
  ADD KEY `pages_color_id_foreign` (`color_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_order_id_foreign` (`order_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `printers`
--
ALTER TABLE `printers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `printers_name_unique` (`name`),
  ADD KEY `printers_brand_id_foreign` (`brand_id`),
  ADD KEY `printers_category_id_foreign` (`category_id`);

--
-- Indexes for table `printer_product`
--
ALTER TABLE `printer_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `printer_product_printer_id_foreign` (`printer_id`),
  ADD KEY `printer_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_name_unique` (`name`),
  ADD UNIQUE KEY `products_url_unique` (`url`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_product_id_foreign` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `attributesets`
--
ALTER TABLE `attributesets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `attributeset_catergory`
--
ALTER TABLE `attributeset_catergory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cartridges`
--
ALTER TABLE `cartridges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `delivery_rates`
--
ALTER TABLE `delivery_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `image_ups`
--
ALTER TABLE `image_ups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `printers`
--
ALTER TABLE `printers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `printer_product`
--
ALTER TABLE `printer_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `address_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `address_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attributes`
--
ALTER TABLE `attributes`
  ADD CONSTRAINT `attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attributeset_catergory`
--
ALTER TABLE `attributeset_catergory`
  ADD CONSTRAINT `attributeset_catergory_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attributeset_catergory_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cartridges`
--
ALTER TABLE `cartridges`
  ADD CONSTRAINT `cartridges_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `features`
--
ALTER TABLE `features`
  ADD CONSTRAINT `features_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pages_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `printers`
--
ALTER TABLE `printers`
  ADD CONSTRAINT `printers_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `printers_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `printer_product`
--
ALTER TABLE `printer_product`
  ADD CONSTRAINT `printer_product_printer_id_foreign` FOREIGN KEY (`printer_id`) REFERENCES `printers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `printer_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
