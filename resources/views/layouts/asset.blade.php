<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{csrf_token()}}" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Cartridgezone</title>

    <link href="{{ asset('img/cartridge_zone.png') }}" rel="shortcut icon">



    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">



    <!-- Styles -->
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/morris.js/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
    
    <!-- bootstrap datepicker -->
    <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

      <!-- Bootstrap Color Picker -->
    <link href="{{ asset('bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
        $( "#agreementdate" ).datepicker();
    } );
    </script>
    <!-- end bootstrap datepicker -->

    <link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">


</head>
<body class="hold-transition skin-blue sidebar-mini hold-transition login-page">



        <main class="py-4">
            @yield('content')
        </main>



    <!-- Scripts -->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}" ></script>
    <script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}" ></script>

    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>

    <script>
        $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
        $('#example3').DataTable({
            'paging'      : false,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
        })
    </script>
    
    <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}" ></script>
    <script src="{{ asset('bower_components/raphael/raphael.min.js') }}" ></script>
    <script src="{{ asset('bower_components/morris.js/morris.min.js') }}" ></script>
    <script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}" ></script>
    <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}" ></script>
    <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}" ></script>
    <script src="{{ asset('bower_components/jquery-knob/dist/jquery.knob.min.js') }}" ></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js') }}" ></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}" ></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" ></script>
    <script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}" ></script>
    <script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}" ></script>
    <script src="{{ asset('dist/js/adminlte.min.js') }}" ></script>
    <script src="{{ asset('dist/js/pages/dashboard.js') }}" ></script>
    <script src="{{ asset('dist/js/demo.js') }}" ></script>

    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}" ></script>

    <!-- Select2 -->
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    

    <!--dataTable-->
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" ></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" ></script>
    <!--end table-->

    <!-- bootstrap color picker -->
    <script src="{{ asset('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>

        <script>
            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()
            //Initialize Select2 Elements
            $('.select2').select2();
            //Date picker
            $('#datepicker').datepicker({
            autoclose: true
            });
            //Start Date picker
            $('#startdate').datepicker({
            autoclose: true
            });
            //End Date picker
            $('#enddate').datepicker({
            autoclose: true
            });
        </script>

    <!--validation-->
    <script src="{{ asset('dist/js/customer/customer.js') }}" ></script>
    <script src="{{ asset('dist/js/validation/jquery.validate.min.js') }}" ></script>
    <script src="{{ asset('dist/js/validation/validation-init.js') }}" ></script>
    <!--end validation-->

</body>
</html>
