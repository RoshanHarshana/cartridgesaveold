<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cheap Printer Ink Cartridges, Laser Toner &amp; Inkjet Printer Cartridges</title>
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/font-awesome.min.css') }}" media="all" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/bootstrap.min.css') }}" media="all" />
    <!-- Main style CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/style.css') }}" media="all" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/responsive.css') }}" media="all" />
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="<?php if($home) echo 'default-body'; ?>">

        <main class="py-4">
            @yield('content')
        </main>





		<!-- jquery main JS -->
		<script src="{{ asset('frontassets/js/jquery.min.js') }}"></script>
		<!-- Bootstrap JS -->
		<script src="{{ asset('frontassets/js/bootstrap.min.js') }}"></script>
		<!-- main JS -->
        <script src="{{ asset('frontassets/js/main.js') }}"></script>
        
</body>
</html>
