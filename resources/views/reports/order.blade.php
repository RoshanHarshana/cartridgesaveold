@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Orders
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Orders List</h3>
            </div>


          <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Customer</th>
                <th>Amount</th>
                <th>Vat</th>
                <th>Delivery Amount</th>
                <th>Quantity</th>
                <th>Ordered Date</th>
                <th>Status</th>   
                <th>View</th>     
              </tr>
              </thead>
              <tbody>
                  <?php $i = 0; ?>
              @foreach($orders as $order)
                  <?php $i++ ?>
              <tr>
                <td>{{ $i }}</td>
                <td>{{ $order->user->name." ".$order->user->lastname }}</td>
                <td>{{ number_format($order->total,2) }}</td>
                <td>{{ number_format($order->vat,2) }}</td>
                <td>{{ number_format($order->delivery_total,2) }}</td>
                <td>{{ $order->total_qty }}</td>
                <td>{{ $order->date }}</td>
                <td>{{ $order->status }}</td>
                <td><a href="{{ url('/') }}/order/view/{{ $order->id }}"><i class="fa fa-edit" style="font-size:25px;  color:green"></i></a></td>
              </tr>
              @endforeach
              </tfoot>
            </table>
          </div>


        </div>
        <!-- /.box -->


      </div>
    </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

@endsection