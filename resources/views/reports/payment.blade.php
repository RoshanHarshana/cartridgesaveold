@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payments
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Payments List</h3>
            </div>


          <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Payment Type</th>
                <th>Amount</th>
                <th>Currency</th>
                <th>Date</th>
                <th>Order Id</th>        
              </tr>
              </thead>
              <tbody>
                  <?php $i = 0; ?>
              @foreach($payments as $payment)
                  <?php $i++ ?>
              <tr>
                <td>{{ $i }}</td>
                <td>{{ $payment->payment_type }}</td>
                <td>{{ number_format($payment->payhere_amount,2) }}</td>
                <td>{{ $payment->payhere_currency }}</td>
                <td>{{ $payment->date }}</td>
                <td>{{ $payment->order_id }}</td>
              </tr>
              @endforeach
              </tfoot>
            </table>
          </div>


        </div>
        <!-- /.box -->


      </div>
    </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

@endsection