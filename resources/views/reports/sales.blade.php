@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Sales List</h3>
            </div>

            <div class="box-header">
                <form method="POST" action="{{ route('searchsales') }}">
                        {{ csrf_field() }}
                        <div id="row">
                          <div class="form-group col-md-6" >
                            <div class="form-group" >
                
                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="startdate" name="startdate"  placeholder="Start date!" required/>
                                </div>
                                <!-- /.input group -->
                              </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="form-group">
                
                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="enddate" name="enddate"  placeholder="End date!" required/>
                                </div>
                                <!-- /.input group -->
                              </div>
                        </div>
                      </div>

                      <div id="row">
                        <div class="form-group col-md-6" style="padding: 0px;">
                            <div class="form-group" >
                                <div class="form-group col-md-12">
                                    <select class="form-control select2" id="cartridge" name="cartridge" style="width: 100%;">
                                        <option value="">--please select cartridge--</option>
                                        @foreach ($products as $product)
                                            <option value="{{$product->id}}">{{$product->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-3" style="padding: 0px;">
                            <div class="form-group" >
                                <div class="form-group col-md-12">
                                    <select class="form-control select2" id="city" name="city" style="width: 100%;">
                                        <option value="">--please select city--</option>
                                        @foreach ($diliveryrates as $diliveryrate)
                                            <option value="{{$diliveryrate->id}}">{{$diliveryrate->town}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-3" style="padding: 0px;">
                        <div class="form-group" >
                            <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                            </div>
                        </div>
                    </div>
                   
                </form>
            </div>

          <div class="box-body">
            <table id="example3" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Customer</th>
                <th>Amount</th>
                <th>Vat</th>
                <th>Delivery Amount</th>
                <th>Quantity</th>
                <th>Ordered Date</th>
                <th>Status</th>   
                <th>View</th>     
              </tr>
              </thead>
              <tbody>
                  <?php 
                    $i = 0; 
                    $sumTot = 0;
                    $sumVat = 0;
                    $sumDel = 0;
                  ?>
              @foreach($orders as $order)
                  <?php $i++ ?>
              <tr>
                <td>{{ $i }}</td>
                <td>{{ $order->user->name." ".$order->user->lastname }}</td>
                <td>{{ number_format($order->total,2) }}</td>
                <td>{{ number_format($order->vat,2) }}</td>
                <td>{{ number_format($order->delivery_total,2) }}</td>
                <td>{{ $order->total_qty }}</td>
                <td>{{ $order->date }}</td>
                <td>{{ $order->status }}</td>
                <td><a href="{{ url('/') }}/order/view/{{ $order->id }}"><i class="fa fa-edit" style="font-size:25px;  color:green"></i></a></td>
              </tr>
              <?php
                $sumTot += $order->total;
                $sumVat += $order->vat;
                $sumDel += $order->delivery_total;
              ?>
              @endforeach
              <tr>
                <td colspan="7" align="right"><strong>Total (LKR)</strong></td>
                <td align="right"><strong>{{number_format(($sumTot+$sumVat+$sumDel),2)}}</strong></td>
                <td></td>
              </tr>
              </tfoot>
            </table>

              <div class="box-footer">
                  <button onclick="myFunction()" class="btn btn-primary" id ="myPrntbtn">Print</button>
              </div>

          </div>


          <div class="box-body" style="display:none;">
            <table class="table table-bordered table-hover" style="width:100%;border:5px;border-spacing: 10px;margin: -10px 0px -10px 0px;" id="forprinter">
              <thead>
              <tr width="100%" style="background:grey">
                <th colspan="2">Customer</th>
                <th>Amount</th>
                <th>Vat</th>
                <th>Delivery Amount</th>
                <th>Quantity</th>
                <th>Ordered Date</th>
                <th>Status</th>   
              </tr>
              </thead>
              <tbody>
                <?php 
                  $i = 0; 
                  $sumTot = 0;
                  $sumVat = 0;
                  $sumDel = 0;
                ?>
              @foreach($orders as $order)
                  <?php $i++ ?>
              <tr width="100%" style="background:grey">
                <td>{{ $i }}</td>
                <td align="right">{{ $order->user->name." ".$order->user->lastname }}</td>
                <td align="right">{{ number_format($order->total,2) }}</td>
                <td align="right">{{ number_format($order->vat,2) }}</td>
                <td align="right">{{ number_format($order->delivery_total,2) }}</td>
                <td align="right">{{ $order->total_qty }}</td>
                <td align="right">{{ $order->date }}</td>
                <td align="right">{{ $order->status }}</td>
              </tr>
              <?php
                $sumTot += $order->total;
                $sumVat += $order->vat;
                $sumDel += $order->delivery_total;
              ?>
              @endforeach
                <tr width="100%">
                  <td colspan="6" align="right"><strong>Total (LKR)</strong></td>
                  <td align="right"><strong>{{number_format(($sumTot+$sumVat+$sumDel),2)}}</strong></td>
                  <td></td>
                </tr>
              </tfoot>
            </table>

            
            <script>
            function myFunction() {
                  var divToPrint = document.getElementById('forprinter');
                  newWin = window.open("");
                  newWin.document.write(divToPrint.outerHTML);
                  newWin.print();
                  newWin.close();
            }
            </script>

          </div>


        </div>
        <!-- /.box -->


      </div>
    </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

@endsection