@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Order Details</h3>
            </div>

            <div class="box-body">
                <?php
                    $orderId = $orders[0]->id;
                    $payments = DB::select('select * from payments where order_id = ?',[$orderId]);
                    $address = DB::select('select * from addresses where order_id = ?',[$orderId]);
                ?>
                
                <table class="table table-bordered table-hover" width="100%" id="orderview">
                    <tr width="100%">
                        <td>
                            <table class="table table-bordered table-hover" width="100%">
                                <thead>
                                    <tr style="background:grey">
                                        <th colspan="2">Order Details</th>   
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="50%">
                                            <strong>Order ID : </strong>{{$orders[0]->id}}<br>
                                            <strong>Date Added : </strong>{{$orders[0]->date}}
                                        </td>
                                        <td width="50%">
                                            <strong>Payment Method : </strong>@if($payments){{$payments[0]->payment_type}}@endif<br>
                                            <strong>Payment Status : </strong>{{$orders[0]->status}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td>
                            <table class="table table-bordered table-hover" width="100%">
                                <thead>
                                    <tr style="background:grey">
                                        <th>Payment Address</th>
                                        <th>Company Name</th>
                                        <th>Customer</th>    
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="33.33%">
                                            @if($address)
                                                <?php
                                                    $selectCity = DB::select('select town from delivery_rates where id = ?',[$address[0]->city]);
                                                ?>
                                                {{$address[0]->street_no}}<br>
                                                {{$address[0]->street_name}}<br>
                                                {{$selectCity[0]->town}}<br>
                                                {{$address[0]->province}}
                                            @else
                                                Not found address!
                                            @endif
                                        </td>
                                        <td width="33.33%">
                                            @if($address)
                                                {{$address[0]->company_name}}
                                            @else
                                            Not found Company!
                                            @endif
                                        </td>
                                        <td width="33.34%">
                                            {{$orders[0]->user->name." ".$orders[0]->user->lastname}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr width="100%">
                        <td>
                            <table class="table table-bordered table-hover" width="100%">
                                <thead>
                                    <tr style="background:grey">
                                        <th width="25%">Cartridge Name</th>
                                        <th width="25%">Quantity</th>   
                                        <th width="25%">Price (LKR)</th>
                                        <th width="25%">Total (LKR)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders[0]->orderproducts as $orderproduct)
                                        <tr>
                                            <td>
                                                {{$orderproduct->product_name}}
                                            </td>
                                            <td>
                                                {{$orderproduct->quantity}}
                                            </td>
                                            <td align="right">
                                                <?php
                                                    $poduct = App\Classes\Productdetails::getProductdetails($orderproduct->product_id);
                                                    $price = $poduct[0]->price;   
                                                    echo number_format($price,2);
                                                ?>
                                            </td>
                                            <td align="right">
                                                <?php
                                                    $poduct = App\Classes\Productdetails::calPrice($orderproduct->quantity, $price); 
                                                    echo $poduct;
                                                ?>
                                            </td>
                                        </tr>
                                    @endforeach 
                                        <tr>
                                            <td colspan="3" align="right"><strong>Sub Total (LKR)</strong></td>
                                            <td align="right"><strong>{{number_format($orders[0]->total,2)}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="right"><strong>Vat (LKR)</strong></td>
                                            <td align="right"><strong>{{number_format($orders[0]->vat,2)}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="right"><strong>Delivery Charge (LKR)</strong></td>
                                            <td align="right"><strong>{{number_format($orders[0]->delivery_total,2)}}</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="right" style="color:green"><strong>Total (LKR)</strong></td>
                                            <td align="right" style="color:green"><strong>{{number_format(($orders[0]->delivery_total+$orders[0]->vat+$orders[0]->total),2)}}</strong>
                                                {{-- {{$payments[0]->payhere_amount}} --}}
                                            </td>
                                        </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>

                <form>

                    <div class="box-body">

                    </div>

                    <div class="box-footer">
                        <button onclick="myFunction('orderview')" class="btn btn-primary" id ="myPrntbtn">Print</button>
                    </div>
                </form>

                

                <script>
                function myFunction(divName) {
                    var printContents = document.getElementById(divName).innerHTML;
                    var originalContents = document.body.innerHTML;

                    document.body.innerHTML = printContents;

                    window.print();

                    document.body.innerHTML = originalContents;
                }
                </script>
          </div>


      </div>
    </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

@endsection