		<!-- footer section start -->
		<footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="footer-content">
                            <a href="callto:94115883335"><i class="fa fa-phone"></i>(+94) 11 588 33 35</a>
                            <p>&copy; Copyright at <a href="#">nimrald.lk</a> - All rights reserved. 2010 - 2018 - <span>Powered by <a href="#">sevensigns.lk</a></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer section end -->

        <!-- jquery main JS -->
        <script src="{{ asset('frontassets/js/jquery.min.js') }}"></script>
        <!-- Bootstrap JS -->
        <script src="{{ asset('frontassets/js/bootstrap.min.js') }}"></script>
        <!-- main JS -->
        <script src="{{ asset('frontassets/js/main.js') }}"></script>
    </body>
</html>