@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit
        <small>Answer</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Answer</h3>
                </div>
                
                @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif
                @if (session('alert2'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif

                {{ Form::model($answer, array('route' => array('answer.update', $answer->id), 'method' => 'PUT', 'files' => true)) }}

                  <div class="box-body">
                    <div class="form-group col-md-12">
                      <label for="Answername" control-label>Answer</label>
                      <textarea class="form-control" rows="5" name="answer" id="answer" placeholder="Enter Answer..." required>{{ $answer->answer }}</textarea>
                    </div>
                  </div>

                  <div class="box-footer">
                    <button id='submit' type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  {{ Form::close() }}
              </div>


          </div>
          
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

 


@endsection
