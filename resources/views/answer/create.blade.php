@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add
        <small>Answers</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New Answers</h3>
                </div>
                
                @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif
                @if (session('alert2'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif

                <form id="answerformstore" method="post" action="{{ route('answer.store') }}">
                {{ csrf_field() }}
                  <div class="box-body">
                  <div class="form-group col-md-12">
                      <label for="productid" control-label>Product</label>
                      <input type="text" name='productid' class="form-control" id="productid" value="{{ $question->product->name }}" readonly/>
                      <input type="hidden" name='questionid' class="form-control" id="questionid" value="{{ $question->id }}"/>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="Questionname" control-label>Question</label>
                      <textarea class="form-control" rows="5" name="question" id="question" placeholder="Enter Question..." readonly>{{ $question->question }}</textarea>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="Answername" control-label>Answer</label>
                      <textarea class="form-control" rows="5" name="answer" id="answer" placeholder="Enter Answer..." required></textarea>
                    </div>
                    @foreach($question->answers as $answer)
                      <div class="form-group col-md-12">
                        <textarea class="form-control" rows="5" readonly>{{ $answer->answer}}</textarea>
                      </div>
                    @endforeach
                  </div>

                  <div class="box-footer">
                    <button id='submit' type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>


          </div>
          
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

 


@endsection
