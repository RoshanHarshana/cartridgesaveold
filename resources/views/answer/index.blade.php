@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Question & Answer
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Question & Answer List</h3>
            </div>

            @if (session('alert'))
                <div class="alert alert-success">
                    {{ session('alert') }}
                </div>
            @endif

            @if (session('alert2'))
                <div class="alert alert-danger">
                    {{ session('alert') }}
                </div>
            @endif

          <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Product</th>
                <th>Question</th>
                <th>Answers</th>
                <th>Add Answers</th>
                <th>Approve</th>               
              </tr>
              </thead>
              <tbody>
                  <?php $i = 0; ?>
              @foreach($question as $question)
                  <?php $i++ ?>
              <tr>
                <td>{{ $i }}</td>
                <td>{{ $question->product->name }}</td>
                <td>{{ $question->question }}</td>
                <td>
                
                  <table border="0" width="100%">
                      <?php $j = 0; ?>
                  @forelse($question->answers as $answer)
                      <?php $j++ ?>
                    <tr>
                      <td width="70%">({{$j}})&nbsp;&nbsp;{{ $answer->answer}}<br><br></td>
                      <td width="15%">
                        <a href="{{ url('/') }}/answer/{{ $answer->id }}/edit"><i class="fa fa-edit" style="font-size:18px;  question:blue"></i></a>
                      </td>
                      <td width="15%">
                        <a href="#" onclick="
                        var result = confirm('Are you Delete this one!');
                          if( result )
                          {
                              event.preventDefault();
                              document.getElementById('delete-form{{ $answer->id }}').submit();
                          }
                            "><i class="fa fa-trash-o" style="font-size:18px;color:red"></i></a>
                  
                          <form id="delete-form{{ $answer->id }}" action="{{ route('answer.destroy',[$answer->id]) }}"
                                  method="POST" style="display: non;">
                                  <input type="hidden" name="_method" value="delete">
                                  {{ csrf_field() }}
                          </form>
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td><p style="color:red">No Answers!</p></td>
                    </tr>
                    @endforelse
                  </table>

                </td>
                <td><a href="{{ url('/') }}/answer/create/{{ $question->id }}"><i class="fa fa-pencil" style="font-size:25px;  question:blue"></i></a></td>  
                <td>
                  @if($question->approve =='1')
                  <a href="{{ url('/') }}/answer/{{ $question->id }}/{{ $question->approve }}"><p style="color:green">Yes</p></a>
                  @else
                  <a href="{{ url('/') }}/answer/{{ $question->id }}/{{ $question->approve }}"><p style="color:red">No</p></a>
                  @endif
                </td>    
              </tr>
              @endforeach
              
              </tfoot>
            </table>
                        
          </div>


        </div>
        <!-- /.box -->


      </div>
    </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <script>

      function deletebtn()
      {
        swal({
          title: "Are you sure?",
          text: "Delete this Record!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              swal("Poof! Your record has been deleted!", {
              icon: "success",
            });
          } else {
            swal("This record is safe!", {icon: "success",} );
          }
        });
      }


      
  </script>

@endsection