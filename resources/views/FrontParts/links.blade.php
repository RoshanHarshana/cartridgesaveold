<li class="nav-item active">
    <a class="nav-link" href="{{ url('/') }}/checkout/cart">
    <i class="fa fa-shopping-cart" style="font-size:20px"></i>&nbsp;&nbsp;Shopping Cart
    <span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
    </a>
</li>