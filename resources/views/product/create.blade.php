@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add
        <small>Products</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add new product</h3>
                </div>
                
                @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif
                
                @if (session('alert2'))
                    <div class="alert alert-danger">
                        {{ session('alert2') }}
                    </div>
                @endif
                
                <form id="productformstore" method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="box-body">
                    <div class="form-group col-md-12">
                      <label for="productname" control-label>Name</label>
                      <input type="text" name='productname' class="form-control" id="productname" placeholder="Enter name" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="brandid">Brand</label>
                      <select class="form-control select2" id="brandid" name="brandid" style="width: 100%;" required>
                        <option value="">--please select--</option>

                        @foreach ($brands as $brand)
                          <option value="{{$brand->id}}">{{$brand->name}}</option>
                        @endforeach

                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="printerid">Printers</label>
                      <select class="form-control select2" multiple="multiple" name="printers[]" id="printers" data-placeholder="Select Printers" style="width: 100%;" required>
                        <option value="">--please select--</option>
                        @foreach ($printer as $printer)
                          <option value="{{ $printer->id }}">{{ $printer->name }}</option> 
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="category">Category</label>
                      <select class="form-control select2" id="category" name="category" style="width: 100%;" required>
                        <option value="">--please select--</option>
                        @foreach ($category as $category)
                          <option value="{{ $category->id }}">{{ $category->name }}</option> 
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="color" control-label>Color</label>
                      <input type="text" name='color' class="form-control" id="color" placeholder="Enter color">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="features" control-label>Paper Yield Details</label>
                      <table class="table table-bordered" id="dynamic_field">  
                        <tr>  
                            <td width="22%">
                                <select class="form-control" name="colortype[]" style="width: 100%;">
                                  <option value="">-Color-</option>
                                  @foreach ($color as $key => $name)
                                    <option value="{{ $key }}">{{ $name }}</option> 
                                  @endforeach
                                </select>
                            </td> 
                            
                            <td width="22%">
                              <input type="number" min="1" name="pages[]" placeholder="Pages" class="form-control" />
                            </td> 
                            <td width="22%">
                              <input type="number" min="1" name="multiply[]" class="form-control" placeholder="Multiply">
                            </td>
                            <td width="22%">
                              <input type="text" name="capacity[]" class="form-control" placeholder="Capacity">
                            </td>  
                            <td width="12%">
                              <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                            </td>  
                        </tr>  
                    </table>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="price" control-label>Price (Rs.)</label>
                      <input type="number" min="1" name='price' step="0.01" class="form-control" id="price" placeholder="Enter price" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="stock" control-label>Stock</label>
                      <input type="number" min="1" name='stock' class="form-control" id="stock" placeholder="Enter stock" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="reorderlevel" control-label>Reorder Level</label>
                      <input type="number" min="1" name='reorderlevel' class="form-control" id="reorderlevel" placeholder="Enter reorder level" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="deliverytype">Delivery Type</label>
                      <select class="form-control select2" id="deliverytype" name="deliverytype" style="width: 100%;" required>
                          <option value="0">Free Delivery</option> 
                          <option value="1">Charge By Weight</option>
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="dutycycle" control-label>Duty Cycle</label>
                      <input type="text" name='dutycycle' class="form-control" id="dutycycle" placeholder="Enter duty cycle">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="manparnum" control-label>Manufacturer Part Number</label>
                      <input type="text" name='manpernum' class="form-control" id="manpernum" placeholder="Enter manufacturer part number">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="proparnum" control-label>Product Part Number</label>
                      <input type="text" name='proparnum' class="form-control" id="proparnum" placeholder="Enter product part number">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="compatibility" control-label>Compatibility</label>
                      <select class="form-control select2" id="compatibility" name="compatibility" style="width: 100%;" required>
                        <option value="">--Select Compatibility--</option> 
                        <option value="Compatible">Compatible</option> 
                        <option value="Genuine">Genuine</option> 
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="recompatibility" control-label>Related Compatibility (You can select four!)</label>
                      <select class="form-control select2" multiple="multiple" id="recompatibility" name="recompatibility[]" data-placeholder="Select related compatibility" style="width: 100%;">
                        <option value="">--please select--</option>
                        @foreach ($product as $key => $name)
                          <option value="{{ $key }}">{{ $name }}</option> 
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="description" control-label>Description</label>
                      <textarea class="form-control" rows="5" name="description" id="description" placeholder="Enter description..." required></textarea>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="vat" control-label>Vat (%)</label>
                      <input type="number" min="0" name='vat' step="0.01" class="form-control" id="vat" value="0" placeholder="Enter vat" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="urlcode" control-label>Url Code</label>
                      <input type="text" name='urlcode' class="form-control" id="urlcode" placeholder="Enter url code" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="printerid">Related Products</label>
                      <select class="form-control select2" multiple="multiple" name="reproduct[]" id="reproduct" data-placeholder="Select related products" style="width: 100%;">
                        <option value="">--please select--</option>
                        @foreach ($product as $key => $name)
                          <option value="{{ $key }}">{{ $name }}</option> 
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="weight" control-label>Weight (Kg)</label>
                      <input type="number" name='weight' class="form-control" step="any" id="weight" placeholder="Enter weight" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="productimage">File input</label>
                      <input type="file" id="image" name="image" required />
    
                      <p class="help-block">Upload product image!</p>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="features" control-label>Features</label>

                        <table class="table table-bordered" id="dynamic_field_two">  
                          <tr>  
                              <td width="80%">
                                <textarea class="form-control" rows="3" name="featuresone[]" placeholder="Enter Feature..." required></textarea>
                              </td>  
                              <td width="20%">
                                <button type="button" name="addSecond" id="addSecond" class="btn btn-success">Add More</button>
                              </td>  
                          </tr>  
                      </table>
                    </div>

                  </div>

                  <div class="box-footer">
                    <button id='submit' type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>


          </div>
          
    </div>


{{-- Add filds --}}

<script type="text/javascript">
  $(document).ready(function(){      
    var i=1;  


    $('#add').click(function(){  
          i++;  
          $('#dynamic_field').append(
            `<tr id="row${i}" class="dynamic-added">  
                <td>
                    <select class="form-control" name="colortype[]" style="width: 100%;" required>
                      <option value="">--select color--</option>
                      @foreach ($color as $key => $name)
                        <option value="{{ $key }}">{{ $name }}</option> 
                      @endforeach
                    </select>
                </td> 
                <td>
                  <input type="number" min="1" name="pages[]" placeholder="Enter amount of pages" class="form-control" required />
                </td> 
                <td>
                  <input type="number" min="1" name="multiply[]" class="form-control" placeholder="Enter multiply" required>
                </td>
                <td>
                  <input type="text" name="capacity[]" class="form-control" placeholder="Enter capacity">
                </td>  
                <td>
                    <button type="button" name="remove" id="${i}" class="btn btn-danger btn_remove">X</button>
                </td>  
            </tr>`
            );  
    });

    $(document).on('click', '.btn_remove', function(){  
          var button_id = $(this).attr("id");   
          $('#row'+button_id+'').remove();  
    });  


  });  
</script>

{{--End add filds --}}


{{-- Add feature --}}

<script type="text/javascript">
  $(document).ready(function(){      
    var i=1;  


    $('#addSecond').click(function(){  
          i++;  
          $('#dynamic_field_two').append(
            `<tr id="row${i}" class="dynamic-added">  
                <td>
                  <textarea class="form-control" rows="3" name="featuresone[]" placeholder="Enter Feature..."></textarea>
                </td> 
                <td>
                    <button type="button" name="remove" id="${i}" class="btn btn-danger btn_remove_two">X</button>
                </td>  
            </tr>`
            );  
    });

    $(document).on('click', '.btn_remove_two', function(){  
          var button_id = $(this).attr("id");   
          $('#row'+button_id+'').remove();  
    });  


  });  
</script>

{{--End add feature --}}


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
 
 


@endsection
