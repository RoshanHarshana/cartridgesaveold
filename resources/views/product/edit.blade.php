@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')


    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit
        <small>Component</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit component</h3>
                </div>
                
                @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif

                @if (session('alert2'))
                    <div class="alert alert-danger">
                        {{ session('alert2') }}
                    </div>
                @endif

                {{ Form::model($product, array('route' => array('product.update', $product->id), 'method' => 'PUT', 'files' => true)) }}

                  <div class="box-body">
                    <div class="form-group col-md-12">
                      <label for="productname" control-label>Name</label>
                      <input type="text" name='productname' class="form-control" id="productname" value="{{ $product->name }}" placeholder="Enter name" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="brandid">Brand</label>
                      <select class="form-control select2" id="brandid" name="brandid" style="width: 100%;" required>
                        <option value="{{ $product->brand_id }}">{{ $product->brand->name }}</option>

                        @foreach ($brands as $brand)
                          <option value="{{$brand->id}}">{{$brand->name}}</option>
                        @endforeach

                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="printerid">Printer</label>
                      <select class="form-control select2" multiple="multiple" name="printers[]" id="printers" data-placeholder="Select Printers" style="width: 100%;">
                        @foreach($product->printers as $printer)
                          <option value="{{ $printer->id }}" selected>{{ $printer->name}}</option>
                        @endforeach
                        @foreach ($printers as $printer)
                          <option value="{{ $printer->id }}">{{ $printer->name }}</option> 
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="category">Category</label>
                      <select class="form-control select2" id="category" name="category" style="width: 100%;" required>
                          <option value="{{ $product->category->id }}">{{ $product->category->name }}</option>
                        @foreach ($category as $category)
                          <option value="{{ $category->id }}">{{ $category->name }}</option> 
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="color" control-label>Color</label>
                      <input type="text" name='color' class="form-control" id="color"
                        @foreach($attributes as $attribute)
                          @if ( $attribute->name == 'color_type' )
                          value="{{ $attribute->value}}"
                          @endif
                        @endforeach
                       placeholder="Enter color" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="features" control-label>Paper Yield Details</label>
                      <table class="table table-bordered" id="dynamic_field">  

                          @if($product->category->name != 'ribbon-cartridges')
                            <?php $i = 1; ?>
                            @foreach($pages as $page)
                            
                            <?php $i++ ?>
                            <tr id="row{{ $i }}" class="dynamic-added">  
                                <td>
                                    <select class="form-control" name="colortype[]" style="width: 100%;" required>
                                      <option value="{{$page->color_id}}">{{$page->color->name}}</option>
                                      @foreach ($color as $key => $name)
                                        <option value="{{ $key }}">{{ $name }}</option>
                                      @endforeach
                                    </select>
                                    <!-- to check server one -->
                                </td> 
                                <td>
                                  <input type="number" min="1" name="pages[]" placeholder="Enter amount of pages" value="{{$page->pages_nos}}" class="form-control" required />
                                </td> 
                                <td>
                                  <input type="number" min="1" name="multiply[]" class="form-control" value="{{$page->multiply}}" placeholder="Enter multiply" required>
                                </td>
                                <td>
                                  <input type="text" name="capacity[]" class="form-control" value="{{$page->capacity}}" placeholder="Enter capacity">
                                </td>  
                                <td>
                                    <button type="button" name="remove" id="{{ $i }}" class="btn btn-danger btn_remove">X</button>
                                </td>  
                            </tr>
                            @endforeach 
                          @endif

                          <tr>  
                            <td width="22%">
                                <select class="form-control" name="colortype[]" style="width: 100%;">
                                  <option value="">-Color-</option>
                                  @foreach ($color as $key => $name)
                                    <option value="{{ $key }}">{{ $name }}</option> 
                                  @endforeach
                                </select>
                            </td> 
                            <td width="22%">
                              <input type="number" min="1" name="pages[]" placeholder="Pages" class="form-control" />
                            </td> 
                            <td width="22%">
                              <input type="number" min="1" name="multiply[]" class="form-control" placeholder="Multiply" />
                            </td>
                            <td width="22%">
                              <input type="text" name="capacity[]" class="form-control" placeholder="Capacity" />
                            </td>  
                            <td width="12%">
                              <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                            </td>  
                        </tr> 

                    </table>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="price" control-label>Price (Rs.)</label>
                      <input type="number" min="1" name='price' step="0.01" class="form-control" id="price" value="{{ $product->price }}" placeholder="Enter price" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="stock" control-label>Stock</label>
                      <input type="number" min="1" name='stock' class="form-control" id="stock" value="{{ $product->stock }}" placeholder="Enter stock" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="deliverytype">Delivery Type</label>
                      <select class="form-control select2" id="deliverytype" name="deliverytype" style="width: 100%;" required>
                          @if ( $product->delivery_type == 0 )
                            <option value="0">Free Delivery</option> 
                            <option value="1">Charge By Weight</option>
                          @else
                            <option value="1">Charge By Weight</option> 
                            <option value="0">Free Delivery</option>
                          @endif
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="reorderlevel" control-label>Reorder Level</label>
                      <input type="number" min="1" name='reorderlevel' class="form-control" id="reorderlevel" value="{{ $product->reorder_level }}" placeholder="Enter reorder level" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="dutycycle" control-label>Duty Cycle</label>
                      <input type="text" name='dutycycle' class="form-control" id="dutycycle" 
                      @foreach($attributes as $attribute)
                          @if ( $attribute->name == 'duty_cycle' )
                          value="{{ $attribute->value}}"
                          @endif
                        @endforeach
                      placeholder="Enter duty cycle" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="manparnum" control-label>Manufacturer Part Number</label>
                      <input type="text" name='manpernum' class="form-control" id="manpernum" 
                      @foreach($attributes as $attribute)
                          @if ( $attribute->name == 'manufacture_part_no' )
                          value="{{ $attribute->value}}"
                          @endif
                        @endforeach
                      placeholder="Enter manufacturer part number">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="proparnum" control-label>Product Part Number</label>
                      <input type="text" name='proparnum' class="form-control" id="proparnum" 
                      @foreach($attributes as $attribute)
                          @if ( $attribute->name == 'product_part_no' )
                          value="{{ $attribute->value}}"
                          @endif
                        @endforeach
                      placeholder="Enter product part number">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="compatibility" control-label>Compatibility</label>
                      <select class="form-control select2" id="compatibility" name="compatibility" style="width: 100%;" required>
                        @foreach($attributes as $attribute)
                          @if ( $attribute->name == 'compatibility' )
                            <option value="{{ $attribute->value}}">{{ $attribute->value}}</option> 
                          @endif
                        @endforeach
                        <option value="Compatible">Compatible</option> 
                        <option value="Genuine">Genuine</option> 
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="recompatibility" control-label>Related Compatibility (You can select four!)</label>
                      <select class="form-control select2" id="recompatibility" name="recompatibility[]" multiple="multiple" data-placeholder="Select related compatibility" style="width: 100%;">
                        @foreach($attributes as $attribute)
                          @if ( $attribute->name == 'related_compatibility' )
                          @foreach (explode(',', $attribute->value) as $info)
                              @if ($info != '')
                                <option value="{{ $info }}" selected>
                                  <?php
                                    $item = DB::table('products')->select('name')->where('id', $info)->get()->toArray();
                                    if ($item) 
                                    {
                                      print_r($item[0]->name); 
                                    }
                                  ?>
                                </option>
                              @endif
                            @endforeach
                            <!-- @if ($attribute->value !='')
                              <option value="{{ $attribute->value}}" selected>
                                <?php
                                  // $relatItem = DB::table('products')->select('name')->where('id', $attribute->value)->get()->toArray();
                                  // if ($relatItem) 
                                  // {
                                  //   print_r($relatItem[0]->name); 
                                  // }
                                ?>
                              </option>
                            @endif -->
                          @endif
                        @endforeach
                          <option>--select--</option> 
                        @foreach ($prod as $key => $name)
                          <option value="{{ $key }}">{{ $name }}</option> 
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="description" control-label>Description</label>
                      <textarea class="form-control" rows="5" name="description" id="description" placeholder="Enter description..." required>{{ $product->description }}</textarea>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="vat" control-label>Vat (%)</label>
                      <input type="number" min="0" name='vat' step="0.01" class="form-control" id="vat" value="{{ $product->vat }}" placeholder="Enter vat">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="urlcode" control-label>Url Code</label>
                      <input type="text" name='urlcode' class="form-control" id="urlcode" value="{{ $product->url }}" placeholder="Enter url code" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="printerid">Related Products</label>
                      <select class="form-control select2" multiple="multiple" name="reproduct[]" id="reproduct" data-placeholder="Select related products" style="width: 100%;">
                        @foreach($attributes as $attribute)
                          @if ( $attribute->name == 'related_products' )
                            @foreach (explode(',', $attribute->value) as $info)
                              @if ($info != '')
                                <option value="{{ $info }}" selected>
                                  <?php
                                  
                                    $item = DB::table('products')->select('name')->where('id', $info)->get()->toArray();
                                    if ($item) 
                                    {
                                      print_r($item[0]->name); 
                                    }
                                  ?>
                                </option>
                              @endif
                            @endforeach
                          @endif
                        @endforeach
                        @foreach ($prod as $key => $name)
                          <option value="{{ $key }}">{{ $name }}</option> 
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="weight" control-label>Weight (Kg)</label>
                      <input type="number" name='weight' value="{{ $product->weight }}" class="form-control" step="any" id="weight" placeholder="Enter weight" required>
                    </div>

                    <div class="form-group col-md-6">
                      <label for="componentimage">File input</label>
                      <input type="file" id="image" name="image" value="{{ $product->image }}" />
    
                      <p class="help-block">Upload component image!</p>
                    </div>

                    <div class="form-group col-md-6">
                      <img src="{{ url('/') }}/image/{{ $product->image }}" width="150px" height="150px" />
                      </div>

                      <div class="form-group col-md-12">
                        <label for="features" control-label>Features</label>
  
                          <table class="table table-bordered" id="dynamic_field_two">  

                            <?php $j = 1; ?>
                            @foreach($features as $feature)
                            <?php $j++ ?>
                            <tr id="rowTwo{{ $j }}" class="dynamic-added">  
                              <td>
                                <textarea class="form-control" rows="3" name="featuresone[]" placeholder="Enter Feature...">{{ $feature->name }}</textarea>
                              </td> 
                              <td>
                                  <button type="button" name="remove" id="{{ $j }}" class="btn btn-danger btn_remove_two">X</button>
                              </td>  
                            </tr>
                            @endforeach

                            <tr>  
                              <td width="80%">
                                <textarea class="form-control" rows="3" name="featuresone[]" placeholder="Enter Feature..."></textarea>
                              </td>  
                              <td width="20%">
                                <button type="button" name="addSecond" id="addSecond" class="btn btn-success">Add More</button>
                              </td>  
                          </tr>

                        </table>
                      </div>

                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button id='submit' type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  {{ Form::close() }}
              </div>


          </div>
          
    </div>


{{-- Add filds --}}

<script type="text/javascript">
  $(document).ready(function(){      
    var i=1;  


    $('#add').click(function(){  
          i++;  
          $('#dynamic_field').append(
            `<tr id="row${i}" class="dynamic-added">  
                <td>
                    <select class="form-control" name="colortype[]" style="width: 100%;" required>
                      <option value="">--select color--</option>
                      @foreach ($color as $key => $name)
                        <option value="{{ $key }}">{{ $name }}</option> 
                      @endforeach
                    </select>
                </td> 
                <td>
                  <input type="number" min="1" name="pages[]" placeholder="Enter amount of pages" class="form-control" required />
                </td> 
                <td>
                  <input type="number" min="1" name="multiply[]" class="form-control" placeholder="Enter multiply" required>
                </td>
                <td>
                  <input type="text" name="capacity[]" class="form-control" placeholder="Enter capacity">
                </td>  
                <td>
                    <button type="button" name="remove" id="${i}" class="btn btn-danger btn_remove">X</button>
                </td>  
            </tr>`
            );  
    });

    $(document).on('click', '.btn_remove', function(){  
          var button_id = $(this).attr("id");   
          $('#row'+button_id+'').remove();  
    });  


  });  
</script>

{{--End add filds --}}


{{-- Add feature --}}

<script type="text/javascript">
  $(document).ready(function(){      
    var j=1;  


    $('#addSecond').click(function(){  
          j++;  
          $('#dynamic_field_two').append(
            `<tr id="rowTwo${j}" class="dynamic-added">  
                <td>
                  <textarea class="form-control" rows="3" name="featuresone[]" placeholder="Enter Feature..."></textarea>
                </td> 
                <td>
                    <button type="button" name="remove" id="${j}" class="btn btn-danger btn_remove_two">X</button>
                </td>  
            </tr>`
            );  
    });

    $(document).on('click', '.btn_remove_two', function(){  
          var button_id = $(this).attr("id");   
          $('#rowTwo'+button_id+'').remove();  
    });  


  });  
</script>

{{--End add feature --}}


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
 
 


@endsection
