@extends('beautymail::templates.widgets')

@section('content')

	@include('beautymail::templates.widgets.newfeatureStart')

        <img src="http://demo.sevensigns.lk/cartridgesave/public/img/favicon.png" alt="">
        <br>
		<h4 class="secondary"><strong>Order reminder!</strong></h4>
        <p><?php echo date("Y-m-d"); ?></p>
        <br>


        <style>
                #orders {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                border-radius:10px;
                width: 100%;
                }
                
                #orders td, #orders th {
                border: 1px solid #ddd;
                padding: 4px;
                }
                
                #orders tr:nth-child(even){background-color: #f2f2f2;}
                
                #orders tr:hover {background-color: #ddd;}
                
                #orders th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #4CAF50;
                color: white;
                }

        </style>

        
        <!-- welcome row -->
        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding:25px 35px">
            <tbody><tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tbody><tr>
                            <td align="center">
                                <h4 class="secondary"><strong>Your Orders</strong></h4>


                                <table id="orders" style="border: 1px solid #ddd; font-size:10px" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="25%">Cartridge Name</th>
                                            <th width="25%">Quantity</th>   
                                            <th width="25%">Price (LKR)</th>
                                            <th width="25%">Total (LKR)</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($orders[0]->orderproducts as $orderproduct)
                                            <tr>
                                                <td>
                                                    {{$orderproduct->product_name}}
                                                </td>
                                                <td align="center">
                                                    {{$orderproduct->quantity}}
                                                </td>
                                                <td align="right">
                                                    <?php
                                                        $poduct = App\Classes\Productdetails::getProductdetails($orderproduct->product_id);
                                                        $price = $poduct[0]->price;   
                                                        echo number_format($price,2);
                                                    ?>
                                                </td>
                                                <td align="right">
                                                    <?php
                                                        $poduct = App\Classes\Productdetails::calPrice($orderproduct->quantity, $price); 
                                                        echo $poduct;
                                                    ?>
                                                </td>
                                            </tr>
                                        @endforeach 
                                            <tr>
                                                <td colspan="3" align="right"><strong>Sub Total (LKR)</strong></td>
                                                <td align="right"><strong>{{number_format($orders[0]->total,2)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="right"><strong>Vat (LKR)</strong></td>
                                                <td align="right"><strong>{{number_format($orders[0]->vat,2)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="right"><strong>Delivery Charge (LKR)</strong></td>
                                                <td align="right"><strong>{{number_format($orders[0]->delivery_total,2)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="right" style="color:green"><strong>Total (LKR)</strong></td>
                                                <td align="right" style="color:green"><strong>{{number_format(($orders[0]->delivery_total+$orders[0]->vat+$orders[0]->total),2)}}</strong>
                                                    {{-- {{$payments[0]->payhere_amount}} --}}
                                                </td>
                                            </tr>
                                    </tbody>
                                </table>

                                <br>
                                <p>If you want view order's history.</p>
                                <a href="{{ url('/') }}/customer/view/{{$userId}}" style="background-color: #4CAF50;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;border-radius: 5px; hover:red">Click here...</a>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
        </tbody></table>

        <p>Thanks <br> Nimrald</p>

    @include('beautymail::templates.widgets.newfeatureEnd')  

@stop