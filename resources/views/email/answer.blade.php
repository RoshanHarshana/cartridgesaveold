@extends('beautymail::templates.widgets')

@section('content')

	@include('beautymail::templates.widgets.newfeatureStart')

        <img src="http://demo.sevensigns.lk/cartridgesave/public/img/favicon.png" alt="">
        <br>
		<h4 class="secondary"><strong>Answer reminder!</strong></h4>
        <p><?php echo date("Y-m-d"); ?></p>
        <br>
        <h4 class="secondary">Nimreld added a new answer for your question!</h4>
        <br>
        <p><strong>Question</strong></p>
        <p>{{$getResut->question}}</p>
        <br>
        <p><strong>Answer</strong></p>
        <p>{{$answer}}</p>
        <br>

        <p>Thanks <br> Nimrald</p>

    @include('beautymail::templates.widgets.newfeatureEnd')  

@stop