@extends('beautymail::templates.widgets')

@section('content')

	@include('beautymail::templates.widgets.articleStart')

        <img src="http://demo.sevensigns.lk/cartridgesave/public/img/favicon.png" alt="">
        <br>
		<h4 class="secondary"><strong>Re-order level reminder!</strong></h4>
        <p><?php echo date("Y-m-d"); ?></p>
        <br>
            <p style="color:red">Low quentity ({{$productsDetails->stock}}) of <strong>{{$productsDetails->name}}</strong>. Out of re-order level!</p>
        <br>

        <p>Thanks <br> Nimrald</p>

    @include('beautymail::templates.widgets.articleEnd')  

@stop