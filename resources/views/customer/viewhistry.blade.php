<?php $home = 'home'; ?>
@include('frontheaderforcustomer')
        <!-- form area start -->
        <section class="form-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div>
                            <table class="table table-hover" style="background-color:whitesmoke; width:100%; border-radius:10px; font-size:12px">
                                <thead>
                                <tr>
                                  <th>#</th>
                                  {{-- <th>Customer</th> --}}
                                  <th>Cartridge Names</th>
                                  <th>Quantity</th>
                                  <th>Ordered Date</th>
                                  <th>Status</th> 
                                  <th>Amount (LKR)</th>
                                  <th>Vat (LKR)</th>
                                  <th>Delivery Amount (LKR)</th>
                                  <th>total (LKR)</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $i = 0; 
                                        $sum = 0;
                                    ?>
                                @foreach($orders as $order)
                                    <?php $i++ ?>
                                <tr>
                                  <td>{{ $i }}</td>
                                  {{-- <td>{{ $order->user->name." ".$order->user->lastname }}</td> --}}
                                  <td>
                                      @foreach ($order->orderproducts as $orderproduct)
                                        {{$orderproduct->product_name}} - ({{$orderproduct->quantity}})
                                      @endforeach
                                  </td>
                                  <td>{{ $order->total_qty }}</td>
                                  <td>{{ $order->date }}</td>
                                  <td>{{ $order->status }}</td>
                                  <td>{{ number_format($order->total,2) }}</td>
                                  <td>{{ number_format($order->vat,2) }}</td>
                                  <td>{{ number_format($order->delivery_total,2) }}</td>
                                  <td>{{ number_format(($order->delivery_total+$order->vat+$order->total),2) }}</td>
                                </tr>
                                <?php
                                    $sum += $order->delivery_total+$order->vat+$order->total;
                                ?>
                                @endforeach
                                <tr>
                                    <td colspan="8" align="right"><strong>Total (LKR)</strong></td>
                                    <td><strong>{{number_format($sum,2)}}</strong></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- form area end -->
@include('frontfooterforcustomer')















































