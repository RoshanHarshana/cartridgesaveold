@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add
        <small>Delivery Rates</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Delivery Rates</h3>
                </div>
                
                @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif
                @if (session('alert2'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif

                <form id="colorformstore" method="post" action="{{ route('diliveryrate.store') }}">
                {{ csrf_field() }}
                  <div class="box-body">
                    <div class="form-group col-md-12">
                      <label for="town" control-label>Town</label>
                      <input type="text" name='town' class="form-control" id="town" required placeholder="Enter color Town!">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="priceforfirst" control-label>Price for first 1Kg (Rs.)</label>
                      <input type="number" min="1" name='priceforfirst' class="form-control" id="priceforfirst" required placeholder="Enter rate!">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="priceforafter" control-label>Price for after 1Kg(s) (Rs.)</label>
                      <input type="number" min="1" name='priceforafter' class="form-control" id="priceforafter" placeholder="Enter rate!">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="cashonpriceforfirst" control-label>Cash on delivery price for first 1Kg (Rs.)</label>
                      <input type="number" min="1" name='cashonpriceforfirst' class="form-control" id="cashonpriceforfirst" required placeholder="Enter rate!">
                    </div>

                    <div class="form-group col-md-12">
                      <label for="cashonpriceforafter" control-label>Cash on delivery price for after 1Kg(s) (Rs.)</label>
                      <input type="number" min="1" name='cashonpriceforafter' class="form-control" id="cashonpriceforafter" placeholder="Enter rate!">
                    </div>

                  </div>

                  <div class="box-footer">
                    <button id='submit' type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>


          </div>
          
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

 


@endsection
