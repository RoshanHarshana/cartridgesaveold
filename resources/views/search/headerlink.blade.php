<?php 
$home = '';
?>
@include('frontheader')

		<section class="item-area">
			<div class="container">  

                    <div class="single-item">
                        <div class="row flexbox-center">
                            <div class="col-lg-12 d-block d-md-none">
                                <div class="item-info">
                                    <h3>{{ $resultBrands[0]->name }}</h3>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-2 col-md-3">
                                <div class="item-info">
                                    <img src="{{ url('/') }}/image/{{ $resultBrands[0]->image }}" alt="search" />
                                    <input type="hidden" value="{{ $resultBrands[0]->id }}" name="brand" id="brand" />
                                    <input type="hidden" value="{{ $catId }}" name="catId" id="catId" />
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10 col-md-9">
                                <div class="item-info">
                                    <h3 class="d-none d-md-block">{{ $resultBrands[0]->name }}</h3>
                                    <p>{{ $resultBrands[0]->description }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="product-search">
                        <h2>Search for {{ $resultBrands[0]->name }} Cartridges</h2>
                        <div class="product-search-inner">
                            <div class="formclass formclass-other">
                                <input type="text" placeholder="e.g. Epson Stylus DX7450" name="searchvalotherBrand" id="searchvalotherBrand" required />
            <button type="submit" onclick="specificBrand();" class="theme-btn" id="searchvalotherBrandbtn"><img src="{{ asset('frontassets/img/arrow_more_button.png') }}" alt="arrow_more_button" /></button>
                            </div>
                        </div>
                    </div>   
                    
                    
                    {{-- <div class="product-search-item">
                        <div class="header-wrap">
                            <h2>Related Cartridges</h2>
                        </div>
                        <div class="content-wrap">
                            <ul class="clearfix">
                                @foreach ($resultPrinters as $resultPrinter)

                                <input type="hidden" value="{{$resultPrinter->category->name}}" name="category" id="category" />
                                <input type="hidden" value="{{ $resultBrands[0]->name }}" name="brand" id="brand" />

                                <li><a href="{{ url('/') }}/{{$resultPrinter->category->name}}/{{ $resultBrands[0]->name }}/{{$resultPrinter->printer_code}}.html">{{$resultBrands[0]->name}} - {{$resultPrinter->printer_code}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div> --}}


                    <div class="product-search-item product-search-item-search">
                        <div class="header-wrap">
                            <h2>Related Cartridges</h2>
                        </div>
                        <div class="content-wrap">
                            @if ($resultProductsCount == 0)
                                No Related Cartridges
                            @else
                                <ul class="clearfix">

                                    @foreach ($resultProducts as $resultProduct)
                                        <input type="hidden" value="{{$resultProduct->category->name}}" name="category" id="category" />
                                        <input type="hidden" value="{{ $resultBrands[0]->name }}" name="brand" id="brand" />
    
                                        <li style="color:lightblue"><a href="{{ url('/') }}/{{$resultProduct->url}}.html">{{$resultBrands[0]->name}} - {{$resultProduct->url}}</a></li>
                                    @endforeach
    
                                </ul>
                            @endif
                        </div>
                    </div>

            </div>
        </div>
                    
    
                    

@include('frontfootertwo')
<script>
    function specificBrand()
    {
        var searchresult = document.getElementById('searchvalotherBrand').value;
        var brand = document.getElementById('brand').value;
        var catId = document.getElementById('catId').value;
        if(searchresult !='')
        {
            location.href = "{{ url('/') }}/search/"+brand+"/"+searchresult+"/"+catId;
        }
    }
</script>
<script>
    var input = document.getElementById("searchvalotherBrand");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("searchvalotherBrandbtn").click();
        }
    });
</script>