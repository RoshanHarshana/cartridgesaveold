<?php 
$home = '';
?>

@section('description')
{{$resultProducts[0]->category->name}} for {{$resultProducts[0]->brand->name}} {{$resultProducts[0]->url}} cartridges. Huge range of {{$resultProducts[0]->brand->name}} cartridges. <?php echo $deliveryType = App\Classes\CeckDeliveryType::checkDelitype($resultProducts[0]->delivery_type); ?> Reliable cartridges. Reliable delivery. Every time!.
@stop

@include('frontheader')

		<section class="item-area">
			<div class="container">

                @foreach ($resultProducts as $resultProduct)
                <input type="hidden" name="urlprintercode" id="urlprintercode" value="{{$resultProduct->url}}" />
                <input type="hidden" name="urlprintercategory" id="urlprintercategory" value="{{$resultProduct->category->name}}" />
                <input type="hidden" name="urlprinterbrand" id="urlprinterbrand" value="{{$resultProduct->brand->name}}" />
                <input type="hidden" name="urlprintername" id="urlprintername" value="{{$resultProduct->name}}" />
                    <div class="single-item">
                        <div class="row flexbox-center">
                            <div class="col-lg-12 d-block d-md-none">
                                <div class="item-info">

                                <h3>{{$resultProduct->name}}</h3>

                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-3 flexbox-center">
                                <div class="item-info">
                                    {{-- <img src="{{ url('/') }}/image/{{ $resultProduct->image }}" alt="item" /> --}}
                                    <a class="fancybox-thumb" rel="fancybox-thumb" href="{{ url('/') }}/image/{{ $resultProduct->image }}" title="Image">
                                        <p><img src="{{ url('/') }}/image/{{ $resultProduct->image }}" alt="item" /></p>
                                    </a>
                                </div>
                                <div class="item-pages d-block d-md-none">
                                    @if($resultProduct->category->name != 'ribbon-cartridges')
                                        @foreach($resultProduct->pages as $page)
                                            <?php
                                                $pageColor = $page->color->name;
                                            ?>
                                            @if ($page->capacity == '')
                                                @if ($pageColor == 'black')
                                                <div class="item-page">
                                                    <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_black.png') }}" alt="dot_black" /></span>
                                                    <p>{{ $page->pages_nos }} Pages</p>
                                                </div>
                                                @elseif($pageColor == 'cyan')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_cyan.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                @elseif($pageColor == 'magenta')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_magenta.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                @elseif($pageColor == 'yellow')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_yellow.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                @elseif($pageColor == 'color')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_color.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                @elseif($pageColor == 'light cyan')
                                                <div class="item-page">
                                                    <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_cayan.png') }}" alt="dot_black" /></span>
                                                    <p>{{ $page->pages_nos }} Pages</p>
                                                </div>
                                                @elseif($pageColor == 'light magenta')
                                                <div class="item-page">
                                                    <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_magenta.png') }}" alt="dot_black" /></span>
                                                    <p>{{ $page->pages_nos }} Pages</p>
                                                </div>
                                                @endif
                                            @else
                                                @if ($pageColor == 'black')
                                                <div class="item-page">
                                                    <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_black.png') }}" alt="dot_black" /></span>
                                                    <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                </div>
                                                @elseif($pageColor == 'cyan')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_cyan.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                @elseif($pageColor == 'magenta')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_magenta.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                @elseif($pageColor == 'yellow')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_yellow.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                @elseif($pageColor == 'color')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_color.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                @elseif($pageColor == 'light cyan')
                                                <div class="item-page">
                                                    <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_cayan.png') }}" alt="dot_black" /></span>
                                                    <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                </div>
                                                @elseif($pageColor == 'light magenta')
                                                <div class="item-page">
                                                    <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_magenta.png') }}" alt="dot_black" /></span>
                                                    <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-6">
                                <div class="item-info">
                                    <h3 class="d-none d-md-block">{{$resultProduct->name}}</h3>
                                    <ul>
                                        @foreach($resultProduct->features as $feature)
                                        <li>{{ $feature->name }}</li>
                                        @endforeach
                                    </ul>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            {{-- <a href="{{ url('/') }}/{{$resultProduct->name}}.html" class="product-more-link"> Product Details </a> --}}
                                        </div>
                                        <div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-3 flexbox-center">
                                <div class="item-info w-80">
                                    @if($resultProduct->category->name != 'ribbon-cartridges')
                                        <?php $pages = 0; ?>
                                        @foreach($resultProduct->pages as $page)
                                            {{-- {{ $page->color->name }}-pages={{ $page->pages_nos }}-multiply={{ $page->multiply }}-capacity={{ $page->capacity }}<br> --}}
                                            <?php $pages += $page->pages_nos ?>
                                        @endforeach
                                    @endif
                                    
                                        <?php
                                            $proId = $resultProduct->id;
                                            $price = $resultProduct->price;
                                            $vat = $resultProduct->vat;
                                            $deliveryTyp = $resultProduct->delivery_type;
                                            //echo $proId;
                                        ?>

                                    <div class="item-pages">

                                    @if($resultProduct->category->name != 'ribbon-cartridges')
                                        <h4>
                                            <?php 
                                                $ppvalue = App\Classes\Ppvalue::CalppValue($pages, $price);
                                                if($resultProducts[0]->category->name != 'ribbon-cartridges')
                                                {
                                                    echo number_format($ppvalue,2)." LKR per page";
                                                }
                                            ?>
                                        </h4>

                                            @foreach($resultProduct->pages as $page)
                                                <?php
                                                    $pageColor = $page->color->name;
                                                ?>
                                                @if ($page->capacity == '')
                                                    @if ($pageColor == 'black')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_black.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                    @elseif($pageColor == 'cyan')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_cyan.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages</p>
                                                        </div>
                                                    @elseif($pageColor == 'magenta')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_magenta.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages</p>
                                                        </div>
                                                    @elseif($pageColor == 'yellow')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_yellow.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages</p>
                                                        </div>
                                                    @elseif($pageColor == 'color')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_color.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages</p>
                                                        </div>
                                                    @elseif($pageColor == 'light cyan')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_cayan.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                    @elseif($pageColor == 'light magenta')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_magenta.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                    @endif

                                                @else
                                                    @if ($pageColor == 'black')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_black.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                    @elseif($pageColor == 'cyan')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_cyan.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                        </div>
                                                    @elseif($pageColor == 'magenta')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_magenta.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                        </div>
                                                    @elseif($pageColor == 'yellow')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_yellow.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                        </div>
                                                    @elseif($pageColor == 'color')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_color.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                        </div>
                                                    @elseif($pageColor == 'light cyan')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_cayan.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                    @elseif($pageColor == 'light magenta')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_magenta.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                    @endif
                                                @endif
                                                
                                            @endforeach
                                        @endif

                                        <a href="#askQue" class="product-ask-question"> Ask a Question >> </a>

                                    </div>
                                    
                                </div>

                                <div class="item-info text-md-right d-block d-md-none d-lg-block">

                                    <div class="price-wrap">
                                        <p class="price">LKR <span class="color"> <span class="big">{{$price}}</span>.00</span></p>
                                        <!-- <p class="price-with-vat">
                                             LKR 
                                             <?php 
                                            //  echo $withvatPrice = App\Classes\Calvat::withVat($price, $vat);
                                            //  if($vat == '' || $vat == 0)
                                            //  {
                                            //     echo " without VAT";
                                            //  }
                                            //  else
                                            //  {
                                            //     echo " in VAT ";
                                            //  }
                                             ?>
                                        </p> -->
                                        <p class="delivery"> <?php echo $deliveryType = App\Classes\CeckDeliveryType::checkDelitype($deliveryTyp); ?> </p>
                                    </div>

                                    <div class="quantity-wrap d-none d-lg-block">
                                        <div class="quantity-wrap-inner">
                                            <button class="quantity-wrap-btn in"></button>
                                            <input type="number" class="quantity-wrap-input quantity-wrap-input-product" value="1" value="1" id="productNumber" name="productNumber" readonly  data-id="{{ $resultProduct->id }}">
                                            <button class="quantity-wrap-btn de"></button>
                                        </div>
                                    </div>

                                    {{-- <button data-fancybox data-src="#confirm-content" href="javascript:;" >name</button> --}}
                                    
                                    {{-- <a href="{{ url('/') }}/add-to-cart/{{ $resultProduct->id }}/{{ $resultProduct->name }}.html" class="theme-btn theme-btn2 theme-cart-btn d-none d-lg-block">Add to Cart </a> --}}

                                    <a href="#" data-id="{{ $resultProduct->id }}" data-fancybox data-src="#confirm-content" data-stock="{{ $resultProduct->stock }}" data-url="{{ $resultProduct->url }}" class="theme-btn theme-btn2 theme-cart-btn d-none d-lg-block product-add-cart-product">Add to Cart </a>
                                </div>

                            </div>

                            <div class="col-12 flexbox-center">
                                <div class="item-info item-info-responsive text-md-right">


                                    <?php 
                                        $vat = $resultProduct->vat;
                                        $price = $resultProduct->price;
                                    ?>
                                    
                                    <div class="quantity-wrap">
                                        <div class="quantity-wrap-inner">
                                            <button class="quantity-wrap-btn in"></button>
                                            <input type="number" class="quantity-wrap-input quantity-wrap-input-mobile-product" value="1" value="1" id="productNumber" name="productNumber" readonly  data-id="{{ $resultProduct->id }}">
                                            <button class="quantity-wrap-btn de"></button>
                                        </div>
                                    </div>

                                    {{-- <a href="{{ url('/') }}/add-to-cart/{{ $resultProduct->id }}/{{ $resultProduct->name }}.html" class="theme-btn theme-btn2 theme-cart-btn">Add to Cart <img src="{{ asset('frontassets/img/arrow_more_button.png') }}" alt="arrow_more_button" /></a> --}}

                                    <a href="#" data-id="{{ $resultProduct->id }}" data-stock="{{ $resultProduct->stock }}" data-url="{{ $resultProduct->url }}" class="theme-btn theme-btn2 theme-cart-btn product-add-cart-mobile-product">Add to Cart </a>

                                    <div class="price-wrap  d-none d-md-block">
                                        <p class="price">LKR <span class="color"> <span class="big">{{$price}}</span>.00</span></p>
                                        <!-- <p class="price-with-vat">
                                            LKR 
                                            <?php 
                                            // echo $withvatPrice = App\Classes\Calvat::withVat($price, $vat);
                                            // if($vat == '' || $vat == 0)
                                            //  {
                                            //     echo " without VAT";
                                            //  }
                                            //  else
                                            //  {
                                            //     echo " in VAT ";
                                            //  }
                                            ?>
                                        </p> -->
                                        <p class="delivery"><?php echo $deliveryType = App\Classes\CeckDeliveryType::checkDelitype($deliveryTyp); ?></p>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                
            <?php 
                $compatibleProducts = App\Classes\Compatibility::getProductset($proId);
                $proIdTwo = 0;
                //echo $compatibleProducts;
            ?>

            <!-- --------------------------------------------------------------- -->
            @if ($compatibleProducts != '')

                <div class="space"></div>

                <div class="single-item suggestion-item save-money">
                    <div class="row">
                        <div class="col-12">
                            <h1>Save Money - Compatible Devices</h1>
                        </div>
                    </div>
                    <div class="row">
                        @foreach (explode(',', $compatibleProducts) as $compatibleProducts)
                        <?php 
                            $compatiProduct = App\Classes\Compatibility::getProduct($compatibleProducts);
                            $compatibility = App\Classes\Compatibility::getCompatibilitydetails($proId);

                            $proIdTwo = $compatiProduct[0]->id;
                            $price = $compatiProduct[0]->price;
                            $vat = $compatiProduct[0]->vat;
                        ?>
                                <?php $pages = 0; ?>
                            @foreach($compatiProduct[0]->pages as $page)
                                {{-- {{ $page->color->name }}-pages={{ $page->pages_nos }}-multiply={{ $page->multiply }}-capacity={{ $page->capacity }}<br> --}}
                                <?php $pages += $page->pages_nos ?>
                            @endforeach



                            <div class="col-md-6 col-12 search-related-products">
                                <div class="row align-items-center">
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-4">
                                        <div class="item-info">
                                            <img src="{{ url('/') }}/image/{{$compatiProduct[0]->image}}" alt="item" />
                                        </div>
                                    </div>
                                    <div class="col-xl-9 col-lg-9 col-md-8 col-8">
                                        <div class="item-info content-info">
                                            <p class="name-tag"><a href="{{ url('/') }}/{{$compatiProduct[0]->url}}.html">{{ $compatiProduct[0]->name }}</a></p>
    
                                            <p>LKR : <span class="price_tag">
                                                <?php
                                                    echo "LKR : ".number_format($price,2);
                                                ?> 
                                            </span> 
                                            @if($resultProduct->category->name != 'ribbon-cartridges') 
                                                <span class="prepage-tag">
                                                    <?php
                                                        $ppvalue = App\Classes\Ppvalue::CalppValue($pages, $price);
                                                        if($resultProducts[0]->category->name != 'ribbon-cartridges')
                                                        {
                                                            echo number_format($ppvalue,2)." LKR per page";
                                                        }
                                                    ?>
                                                </span> 
                                            @endif
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach
                        </div>
                    </div>
                @endif
                    <!-- --------------------------------------------------------------- -->

                    <?php 
                        $relatedProducts = App\Classes\Compatibility::getRelatedproduct($resultProduct->id);
                        //echo $relatedProducts;
                        //$divNum = count(explode(',', $relatedProducts));
                    ?>

                    @if ($relatedProducts)


                    <div class="space-small"></div>
                        
                        <div class="single-item">
                            <div class="row">
                                <div class="col-12">
                                    <div class="header-wrap-underline tab-heading-mobile">
                                        <h2>Other cartridges and multipacks in this range</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row tab-content-mobile">

                                
                                @foreach (explode(',', $relatedProducts) as $info)
                                    @if ($info != '')
                                        <?php                
                                            $item = DB::table('products')->select('name','price','image','category_id','brand_id','url')->where('id', $info)->get()->toArray();
                                            // $categoryFind = DB::select('select name from categories where id = ?', [$item[0]->category_id]);
                                            // $brandFind = DB::select('select name from brands where id = ?', [$item[0]->brand_id]);
                                        ?>

                                        <div class="col-md-6 col-12 search-related-products">
                                            <div class="row align-items-center">
                                                <div class="col-xl-3 col-lg-3 col-md-4 col-4">
                                                    <div class="item-info">
                                                        <img src="{{ url('/') }}/image/<?php print_r($item[0]->image);  ?>" alt="item" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-9 col-lg-9 col-md-8 col-8">
                                                    <div class="item-info content-info">
                                                        <p class="name-tag"><a href="{{ url('/') }}/{{$item[0]->url}}.html"><?php print_r($item[0]->name);  ?></a></p>
                
                                                        <p>LKR : <span class="price_tag"> <?php print_r($item[0]->price);  ?></span></p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
        
                            </div>
                        </div>

                    @endif
                    
                    <div class="space"></div>

                    <div class="product-description">
                        <div class="row">
                            <div class="col-lg-12 col-12">
                                <div class="product-description-inner product-description-inner-no  left-child">
                                    <div class="content">
                                        <div class="product-search-item">
                                        <div class="heading-wrap tab-heading-mobile">
                                            <h2>This item is guaranteed to work in the following printers</h2>
                                        </div>
                                        <div class="content-wrap tab-content-mobile">
                                            <ul class="clearfix">
                
                                                @foreach($resultProduct->printers as $printer)
                                                    <li style="color:lightblue"><a href="{{ url('/') }}/{{$printer->category->name}}/{{$printer->brand->name}}/{{$printer->printer_code}}/{{$printer->id}}.html">> {{ $printer->name }}</a></li>
                                                @endforeach
                            
                
                                            </ul>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="space"></div>
    
                    <div class="product-description">
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <div class="product-description-inner left-child">
                                    <div class="content">
                                        <div class="heading-wrap tab-heading-mobile" >
                                            <h2>Product Description</h2>
                                        </div>
                                        <p>
                                            {{$resultProduct->description}}
                                        </p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="product-description-inner right-child">
                                    <div class="content">
                                        <div class="heading-wrap tab-heading-mobile">
                                            <h2>Product Specification</h2>
                                        </div>
                                        <table class="table  table-borderless tab-content-mobile">
    
                                            <tbody>
                                                <tr>
                                                    <td width="50%"> <strong> Manufacturer Part No:</strong></td>
                                                    <td>
                                                        <?php 
                                                            $manufacture = App\Classes\Manufacturepartno::getmanufacturePart($proId);
                                                            echo $manufacture;
                                                        ?>
                                                    </td>
                                                </tr>
                                                @if($resultProduct->category->name != 'ribbon-cartridges') 
                                                <tr>
                                                    <td><strong> Duty Cycle:</strong></td>
                                                    <td>
                                                        <?php 
                                                            $duty = App\Classes\Dutycycle::getDutycycle($proId);
                                                            echo $duty;
                                                        ?>
                                                    </td>
                                                </tr>
                                                @endif
                                                <tr>
                                                    <td><strong> Brand:</strong></td>
                                                    <td>
                                                        {{$resultProduct->brand->name}}
                                                        
                                                    </td>
                                                </tr>
                                                @if($resultProduct->category->name != 'ribbon-cartridges') 
                                                @if ($resultProduct->category->name != 'toner-cartridges')
                                                <tr>
                                                    <td><strong>Capacity:</strong> </td>
                                                    <td>
                                                        <?php
                                                            $i = 0;
                                                            $len = count($resultProduct->pages);    
                                                        ?>
                                                        @foreach($resultProduct->pages as $page)
                                                        <?php
                                                            if ($i == $len - 1) 
                                                            {
                                                                $mark = '';
                                                            }
                                                            else 
                                                            {
                                                                $mark = ' + ';
                                                            }
                                                            $i++;   
                                                        ?>
                                                            {{ $page->capacity }} {{$mark}}
                                                        @endforeach
                                                    </td>
                                                </tr>
                                                @endif
                                                <tr>
                                                    <td><strong> Colour:</strong></td>
                                                    <td>
                                                        <?php 
                                                            $color = App\Classes\Colortype::getColortype($proId);
                                                            echo $color;
                                                        ?>
                                                        <input type="hidden" id="color" name="color" value="{{$color}}" />
                                                    </td>
                                                </tr>
                                                @endif
                                                <tr>
                                                    <td><strong> Product Type:</strong></td>
                                                    <td>
                                                        <?php
                                                            $catName = $resultProduct->category->name;
                                                            if($catName == 'ink-cartridges'){
                                                                $catNewname = 'Ink Cartridges';
                                                            }
                                                            elseif ($catName == 'toner-cartridges') {
                                                                $catNewname = 'Toner Cartridges';
                                                            }
                                                            elseif ($catName == 'ribbon-cartridges') {
                                                                $catNewname = 'Ribbon Cartridges';
                                                            }
                                                            elseif ($catName == 'bulk-ink') {
                                                                $catNewname = 'Bulk-Ink';
                                                            }
                                                            elseif ($catName == 'powder') {
                                                                $catNewname = 'Powder';
                                                            }
                                                            echo $catNewname;
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				
			</div>
		</section>
        <!-- item area end -->


        <!-- qestions & answers area -->
        <section class="questions-wrap" id="askQue">
            <div class="container">
                <div class="header-wrap">
                    <h2>Customer Questions & Answers</h2>
                </div>
                <div class="d-block d-md-flex justify-content-between align-items-center">
                    <div> <p>Can't find the answer you're looking for? Ask us a question we're happy to help!</p> </div>
                    <div> <a id="askQuebtn" class="theme-btn theme-btn2 theme-cart-btn questions-btn">Ask a Question <img src="{{ asset('frontassets/img/arrow_more_button.png') }}" alt="arrow_more_button" /></a> </div>
                </div>

                <div class="questions-form" id="questions-form">
                    @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                        @if (session('alert'))
                            <script>
                                //$('#QueForm').show();
                                $(document).ready(function(){
                                    $("#askQuebtn").click(); 
                                });
                            </script>
                        @endif
                    </div>
                    @endif
                    @if (session('alert2'))
                        <div class="alert alert-danger">
                            {{ session('alert2') }}
                            @if (session('alert2'))
                                <script>
                                    //$('#QueForm').show();
                                    $(document).ready(function(){
                                        $("#askQuebtn").click(); 
                                    });
                                </script>
                            @endif
                        </div>
                    @endif
                    <form action="{{ route('customerQuestion') }}" id="QueForm" method="post">
                            @csrf
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" class="form-control" id="quename" name="quename" placeholder="Enter Name!" required>
                            <input type="hidden" class="form-control" id="queproid" name="queproid" value="{{$resultProduct->id}}" />
                        </div>
                        <div class="form-group">
                            <label for=""> Email</label>
                            <input type="email" class="form-control" id="queemail" name="queemail" placeholder="Enter Email!" required>
                        </div>
                        <div class="form-group">
                            <label for="">Questions </label>
                            <textarea class="form-control" id="questions" name="questions" rows="3" required></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="theme-btn theme-btn2 theme-cart-btn">Ask a Question</button>
                        </div>
                    </form>
                </div>

                <?php 

                    $productAnsid = $resultProduct->id;
                    $question = App\Classes\Answer::getAnswers($productAnsid);

                    if($question)
                        { 

                ?>
                @foreach ($question as $question)
                    
                    <div class="questions-wrap-item">
                        <div class="item-details questions-details">
                            <p>{{ $question->question }}</p>

                            <div class="user-details">
                                <?php
                                    //for check date... 
                                    $oldDate = $question->date;
                                    $echoDate = App\Classes\Datediff::calDatediff($oldDate);

                                    //for check customer... 
                                    $checkTocus = $question->customer_name;
                                    $custName = App\Classes\Customer::checkCustomer($checkTocus);
                                    $forAnsers = 'System Administrator';
                                    
                                ?>
                                <span>{{$custName}} - {{$echoDate}}</span>
                            </div>
                        </div>
                        @forelse($question->answers as $answer)
                            <div class="item-details answer-details">
                                <p>{{ $answer->answer}}</p>

                                <div class="user-details">
                                    <span>{{$forAnsers}} - {{$echoDate}}</span>
                                </div>
                            </div>
                        @endforeach
                    </div>

                @endforeach

                <?php
                    }
                ?>

                {{-- <div class="header-wrap">
                        <h2>Most popular HP printers</h2>
                    </div>

                    <div class="questions-wrap-similler">
                        <ul class="clearfix">
                            <li> lorem ipsam </li>
                            <li> lorem ipsam </li>
                            <li> lorem ipsam </li>
                            <li> lorem ipsam </li>
                            <li> lorem ipsam </li>
                        </ul>
                    </div> --}}

            </div>
        </section>
        <!--end qestions & answers area -->


        <script>
            window.addEventListener('load', function() {
                var urlprinterbrand = document.getElementById('urlprinterbrand').value;
                var urlprintername = document.getElementById('urlprintername').value;
                var urlprintercode = document.getElementById('urlprintercode').value;
                var color = document.getElementById('color').value;
                var urlprintercategory = document.getElementById('urlprintercategory').value;

                var removeAddspace = urlprintercategory.replace('-', ' ');
                var firstWord = removeAddspace.replace(/ .*/,'');

                var x = urlprintercode+" "+removeAddspace+" "+color+","+urlprintercode+" printer cartridge";
                document.getElementById("titleId").innerHTML = x;
            });
        </script>


@include('frontfootertwo')
