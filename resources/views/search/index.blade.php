<?php 
$home = '';
$UrlPast = "search=".$searchid;
?>
@include('frontheader')

		<!-- search name area start -->
		<section class="search-name search-result">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="search-item">
							<h2 class="index-hed">Search Results for "{{$searchid}}"</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 pagination-wrap d-md-flex justify-content-md-between">
                        <span>{{$resultPrintersCount + $resultProductsCount}} results</span>
                        <div class="pagination pagination-list">
                            {!! $articlesLink !!}
                        </div>
					</div>
				</div>
			</div>
        </section>

        
        <div class="pagination">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-md-right d-none d-md-block">
                        
                    </div>
                </div>
            </div>
        </div>
		<!-- search name area end -->
		<?php //if($printer): ?>
		@include('searchprinter')
		<?php //endif ?>
		<!-- item area start -->
		<section class="item-area">
			<div class="container">

                @foreach ($resultProducts as $resultProduct)
                    <div class="single-item single-search">
                        <div class="row flexbox-center">
                            <div class="col-lg-12 d-block d-md-none">
                                <div class="item-info">
                                    <a href="{{ url('/') }}/{{$resultProduct->url}}.html"><h3>{{$resultProduct->name}}</h3></a>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-3 flexbox-center">
                                <div class="item-info">
                                    {{-- <img src="{{ url('/') }}/image/{{ $resultProduct->image }}" alt="item" /> --}}
                                    <a class="fancybox-thumb" rel="fancybox-thumb" href="{{ url('/') }}/image/{{ $resultProduct->image }}" title="Image">
                                        <p><img src="{{ url('/') }}/image/{{ $resultProduct->image }}" alt="item" /></p>
                                    </a>
                                </div>
                                <div class="item-pages d-block d-md-none">
                                        





                                <!--  -->
                                <?php 
                                    $vat = $resultProduct->vat;
                                    $price = $resultProduct->price;
                                ?>
                                <div class="item-info text-md-right d-block d-md-none d-lg-none mobile-wrap-price">

                                    <div class="price-wrap">
                                        <p class="price">LKR <span class="color"> <span class="big">{{$price}}</span>.00</span></p>
                                        <!-- <p class="price-with-vat">
                                                LKR 
                                                <?php 
                                            //  echo $withvatPrice = App\Classes\Calvat::withVat($price, $vat);
                                            //  if($vat == '' || $vat == 0)
                                            //  {
                                            //     echo " without VAT";
                                            //  }
                                            //  else
                                            //  { 
                                            //     echo " in VAT ";
                                            //  }
                                                ?>
                                        </p> -->
                                        <p class="delivery"> 
                                            <?php 
                                            echo $withvatPrice = App\Classes\CeckDeliveryType::checkDelitype($resultProduct->delivery_type);
                                            ?> 
                                        </p>
                                    </div>

                                    <div class="quantity-wrap d-block d-md-none">
                                        <div class="quantity-wrap-inner">
                                            <button class="quantity-wrap-btn in"></button>
                                                <input type="number" class="quantity-wrap-input" value="1" id="productNumber" name="productNumber" readonly  data-id="{{ $resultProduct->id }}"/>
                                            <button class="quantity-wrap-btn de"></button>
                                        </div>
                                    </div>

                                    <a href="#" data-id="{{ $resultProduct->id }}" data-stock="{{ $resultProduct->stock }}" data-url="{{ $UrlPast }}"  class="theme-btn theme-btn2 theme-cart-btn d-inline-block d-md-none product-add-cart">Add to Cart </a>
                                </div>




                                <!--  -->







                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-6">
                                <div class="item-info d-none d-md-block">
                                    <h4 class="d-none d-md-block">{{$resultProduct->name}}</h4>
                                    <ul>
                                        @foreach($resultProduct->features as $feature)
                                        <li>{{ $feature->name }}</li>
                                        @endforeach
                                    </ul>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            {{-- <a href="{{ url('/') }}/{{str_replace(' ','-',$resultProduct->name)}}.html" class="product-more-link"> Product Details </a> --}}
                                            <a href="{{ url('/') }}/{{$resultProduct->url}}.html" class="product-more-link"> Product Details </a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-xl-5 col-lg-5 col-md-3 flexbox-center">
                                <div class="item-info w-80">
                                        <?php $pages = 0; ?>
                                        @foreach($resultProduct->pages as $page)
                                            {{-- {{ $page->color->name }}-pages={{ $page->pages_nos }}-multiply={{ $page->multiply }}-capacity={{ $page->capacity }}<br> --}}
                                            <?php $pages += $page->pages_nos ?>
                                        @endforeach
                                    
                                        <?php
                                            $proId = $resultProduct->id;
                                            $price = $resultProduct->price;
                                            $vat = $resultProduct->vat;
                                        ?>
            
                                    <!-- to check server one -->

                                    <div class="item-pages">

                                        @if($resultProduct->category->name != 'ribbon-cartridges')
                                            <h4>
                                                <?php 
                                                    $ppvalue = App\Classes\Ppvalue::CalppValue($pages, $price);
                                                    if($resultProduct->category->name != 'ribbon-cartridges')
                                                    {
                                                        echo number_format($ppvalue,2)." LKR per page";
                                                    }
                                                ?>
                                            </h4>

                                            @foreach($resultProduct->pages as $page)
                                                <?php
                                                    $pageColor = $page->color->name;
                                                ?>
                                                @if ($page->capacity == '')
                                                    @if ($pageColor == 'black')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_black.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                    @elseif($pageColor == 'cyan')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_cyan.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages</p>
                                                        </div>
                                                    @elseif($pageColor == 'magenta')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_magenta.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages</p>
                                                        </div>
                                                    @elseif($pageColor == 'yellow')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_yellow.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages</p>
                                                        </div>
                                                    @elseif($pageColor == 'color')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_color.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages</p>
                                                        </div>
                                                    @elseif($pageColor == 'light cyan')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_cayan.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                    @elseif($pageColor == 'light magenta')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_magenta.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages</p>
                                                    </div>
                                                    @endif
                                                @else
                                                    @if ($pageColor == 'black')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_black.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                    @elseif($pageColor == 'cyan')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_cyan.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                        </div>
                                                    @elseif($pageColor == 'magenta')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_magenta.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                        </div>
                                                    @elseif($pageColor == 'yellow')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_yellow.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                        </div>
                                                    @elseif($pageColor == 'color')
                                                        <div class="item-page">
                                                            <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_color.png') }}" alt="dot_black" /></span>
                                                            <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                        </div>
                                                    @elseif($pageColor == 'light cyan')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_cayan.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                    @elseif($pageColor == 'light magenta')
                                                    <div class="item-page">
                                                        <span class="dot-img-wrap"><img src="{{ asset('frontassets/img/dot_light_magenta.png') }}" alt="dot_black" /></span>
                                                        <p>{{ $page->pages_nos }} Pages ({{ $page->capacity }})</p>
                                                    </div>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif

                                    </div>


                                    

                                </div>

                                
                                <div class="item-info text-md-right d-none d-md-none d-lg-block">

                                    <div class="price-wrap">
                                        <p class="price">LKR <span class="color"> <span class="big">{{$price}}</span>.00</span></p>
                                        <!-- <p class="price-with-vat">
                                             LKR 
                                             <?php 
                                            //  echo $withvatPrice = App\Classes\Calvat::withVat($price, $vat);
                                            //  if($vat == '' || $vat == 0)
                                            //  {
                                            //     echo " without VAT";
                                            //  }
                                            //  else
                                            //  { 
                                            //     echo " in VAT ";
                                            //  }
                                             ?>
                                        </p> -->
                                        <p class="delivery"> 
                                            <?php 
                                            echo $withvatPrice = App\Classes\CeckDeliveryType::checkDelitype($resultProduct->delivery_type);
                                            ?> 
                                        </p>
                                    </div>

                                    <div class="quantity-wrap d-none d-lg-block">
                                        <div class="quantity-wrap-inner">
                                            <button class="quantity-wrap-btn in"></button>
                                                <input type="number" class="quantity-wrap-input" value="1" id="productNumber" name="productNumber" readonly  data-id="{{ $resultProduct->id }}"/>
                                            <button class="quantity-wrap-btn de"></button>
                                        </div>
                                    </div>

                                    <a href="#" data-id="{{ $resultProduct->id }}" data-stock="{{ $resultProduct->stock }}" data-url="{{ $UrlPast }}"  class="theme-btn theme-btn2 theme-cart-btn d-none d-lg-block product-add-cart">Add to Cart </a>
                                </div>
                            </div>

                            <div class="col-12 flexbox-center d-none d-md-block">
                                <div class="item-info item-info-responsive text-md-right">


                                    <?php 

                                        $vat = $resultProduct->vat;
                                        $price = $resultProduct->price;

                                    ?>
                                    
                                    <div class="quantity-wrap">
                                        <div class="quantity-wrap-inner">
                                            <button class="quantity-wrap-btn in"></button>
                                            <input type="number" class="quantity-wrap-input quantity-wrap-input-mobile" value="1" id="productNumber" name="productNumber" readonly data-id="{{ $resultProduct->id }}">
                                            <button class="quantity-wrap-btn de"></button>
                                        </div>
                                    </div>

                                    <a href="#" data-id="{{ $resultProduct->id }}" data-stock="{{ $resultProduct->stock }}" data-url="{{ $UrlPast }}" class="theme-btn theme-btn2 theme-cart-btn product-add-cart-mobile">Add to Cart </a>

                                    <div class="price-wrap  d-none d-md-block">
                                        <p class="price">LKR <span class="color"> <span class="big">{{$price}}</span>.00</span></p>
                                        <!-- <p class="price-with-vat">
                                             LKR 
                                             <?php 
                                            //  echo $withvatPrice = App\Classes\Calvat::withVat($price, $vat);
                                            //  if($vat == '' || $vat == 0)
                                            //  {
                                            //     echo " without VAT";
                                            //  }
                                            //  else
                                            //  {
                                            //     echo " in VAT ";
                                            //  }
                                             ?>
                                        </p> -->
                                        <p class="delivery">
                                            <?php 
                                            echo $withvatPrice = App\Classes\CeckDeliveryType::checkDelitype($resultProduct->delivery_type);
                                            ?> 
                                        </p>
                                    </div>
                                
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
				
				
			</div>
		</section>
        

        <script>
            window.addEventListener('load', function() {
                var x = "Cartridgezone - Search";
                document.getElementById("titleId").innerHTML = x;
            });
        </script>


@include('frontfootertwo')
