<!-- search name area start -->
<section class="search-name">
        <div class="container">
                    @foreach ($resultPrinters as $resultPrinter)
                    @if($resultPrinter->category->name == 'toner-cartridges' || $resultPrinter->category->name == 'powder' || $resultPrinter->category->name == 'bulk-ink' || $resultPrinter->category->name == 'ink-cartridges' || $resultPrinter->category->name == 'ribbon-cartridges')
                        
                    <?php
                        $cartridge = $resultPrinter->category->name;
                        $brandName = $resultPrinter->brand->name;
                        $printerName = $resultPrinter->name;

                        $result = App\Classes\Search::countCartridgeShow($brandName, $printerName);
                    ?>
                        @if($result > 0)
                        <div class="single-item single-item-search-name">
                            <div class="row flexbox-center">
                                <div class="col-lg-12 d-block d-md-none">
                                    <div class="item-info">
                                        <h3>
                                            <?php
                                                $catName = $resultPrinter->category->name;
                                                if($catName == 'ink-cartridges'){
                                                    $catNewname = 'Ink-Cartridges';
                                                }
                                                elseif ($catName == 'toner-cartridges') {
                                                    $catNewname = 'Toner-Cartridges';
                                                }
                                                elseif ($catName == 'ribbon-cartridges') {
                                                    $catNewname = 'Ribbon-Cartridges';
                                                }
                                                elseif ($catName == 'bulk-ink') {
                                                    $catNewname = 'Bulk-Ink';
                                                }
                                                elseif ($catName == 'powder') {
                                                    $catNewname = 'Powder';
                                                }
                                                ?>
                                                {{ $catNewname }}- {{$resultPrinter->name}}
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-2 col-md-3 col-4">
                                    <div class="item-info">
                                        <img src="{{ url('/') }}/image/{{ $resultPrinter->image }}" alt="item" />
                                    </div>
                                </div>
                                <div class="col-xl-9 col-lg-10 col-md-9 col-8">
                                    <div class="item-info">
                                        <h3 class="d-none d-md-block">{{ $catNewname }}- {{$resultPrinter->name}}</h3>
                                        <ul>
                                            <li>{{$result}} Cartridges for this printer</li>
                                        </ul>
                                        <a href="{{ url('/') }}/{{$resultPrinter->category->name}}/{{$resultPrinter->brand->name}}/{{$resultPrinter->printer_code}}/{{$resultPrinter->id}}.html" class="theme-btn theme-btn2">View Cartridges </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @else 
                    <div class="single-item">
                        <div class="row flexbox-center">
                            <P style="color:red">This category name doesn't match for URL name! Please check that back-end!</p>
                        </div>
                    </div>
                @endif
            @endforeach

        </div>
    </section>
    <!-- search name area end -->