<?php 
$home = '';
?>
@include('frontheader')

    <!-- item area start -->
		<section class="item-area">
                <div class="container">
                    
                    <div class="billing-wrap">

                        <div class="account-wrap" style="display">
                                <form class="account-inner" action="{{ route('done') }}" method="post">
                                        {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-7 col-12">
                                            <!-- payment method -->
                                            <div style="display">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h2><br></h2>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="d-flex  flex-column justify-content-center successfull-wrap">
                                                            <div>
                                                                <img src="{{ asset('frontassets/img/tick.png') }}" alt="">
                                                            </div>
                                                            <p> Your order has been successfully placed.. </p>
                                                            {{-- <p><span>Cash on Delivery</span></p> --}}
                                                        </div>              
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- done  -->



                                        <div class="col-md-5 col-12">

                                            @include('shop.summerytwo')

                                        </div>
                                    </div>
                                </form>
                        </div>
    
    
                    </div>
    
    
                    <!--  -->
    
                </div>
            </section>

@include('frontfootertwo')