<?php 
$home = '';
?>
@include('frontheader')

    <!-- item area start -->
		<section class="item-area">
                <div class="container">
                    
                    <div class="billing-wrap">

                        <div class="account-wrap" style="display">
                            
                                <form class="account-inner" method="post" action="#" id="paymentForm" onsubmit="return validate(this);">
    
                                    <div class="row">
                                            <div class="col-md-7 col-12">
                                        <!-- payment method -->
                                        <div style="display">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h2>Payment Method</h2>
                                                    </div>
                                                </div>
                                                <div class="row" style="padding-top:15px">
    
                                                    <div class="col-12">
                                                        <div class="custom-control custom-radio check-input">
                                                            <input type="radio" id="w2" name="paymethod" class="custom-control-input" onchange="calCashondelivery()" value="cashon">
                                                            <label class="custom-control-label" for="w2">Cash on Delivery</label>
                                                            <input type="hidden" value="{{$diliveryCharge}}" id="diliveryCharge" name="diliveryCharge" />
                                                            <input type="hidden" value="{{$getCharge[0]->cod_price_first_kg}}" id="firstkg" name="firstkg" />
                                                            <input type="hidden" value="{{$getCharge[0]->cod_price_after_first_kg}}" id="afterkg" name="afterkg" />
                                                        </div>
                                                    </div>
    
                                                    <div class="col-12">
                                                        <div class="custom-control custom-radio check-input">
                                                            <input type="radio" id="w1" name="paymethod" class="custom-control-input" onchange="changecartDetails()" value="cardpay">
                                                            <label class="custom-control-label" for="w1">Card Payment <img src="{{ asset('frontassets/img/cards.png') }}" alt="" srcset=""> </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-12">
                                                        <div class="card-pay-wrap">

                                                                <input type="hidden" name="merchant_id" value="1210984">
                                                                <input type="hidden" name="return_url" value="{{ route('finished') }}">
                                                                {{-- {{ route('get-Api-details') }} --}}
                                                                <input type="hidden" name="cancel_url" value="http://sample.com/cancel">
                                                                <input type="hidden" name="notify_url" value="{{ route('getapidetails') }}">  

                                                                {{-- <br><br>Item Details<br> --}}
                                                                <input type="hidden" name="order_id" value="{{$orderId}}">
                                                                <input type="hidden" name="items" value="Cartriges"><br>
                                                                <input type="hidden" name="currency" value="LKR">
                                                                <input type="hidden" name="amount" id="amount" value="0">
                                                                {{-- <br><br>Customer Details<br> --}}
                                                                <input type="hidden" name="first_name" value="{{$customer[0]->name}}">
                                                                <input type="hidden" name="last_name" value="{{$customer[0]->lastname}}"><br>
                                                                <input type="hidden" name="email" value="{{$customer[0]->email}}">
                                                                <input type="hidden" name="phone" value="" placeholder="Enter Phone number"><br>
                                                                <input type="hidden" name="address" value="" placeholder="Enter Address">
                                                                <input type="hidden" name="city" value="" placeholder="Enter City">
                                                                <input type="hidden" name="country" value="Sri Lanka"><br><br> 
                                                                {{-- <input type="submit" value="Buy Now">   --}}

                                                                <?php $i = 0; ?>
                                                                @foreach($products as $product)
                                                                <?php $i++ ?>
                                                                    <input type="hidden" name="item_name_{{$i}}" value="{{$product['item']['name']}}">
                                                                    <input type="hidden" name="item_number_{{$i}}" value="{{$product['item']['id']}}"><br>
                                                                    <input type="hidden" name="amount_{{$i}}" value="{{$product['item']['price']}}">
                                                                    <input type="hidden" name="quantity_{{$i}}" value="{{$product['qty']}}">
                                                                @endforeach
                                                        </div>                   
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 col-12">
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <!-- done  -->



                                        <div class="col-md-5 col-12">

                                            @include('shop.summerytwo')

                                        </div>
                                    </div>
    
                                </form>
                            
                        </div>
    
    
                    </div>
    
    
                    <!--  -->
    
                </div>
            </section>

<script>
function calCashondelivery() {

    //alert('hii');

    $("#paymentForm").attr("action","{{ route('getcashdetails') }}");
    
    var weight = document.getElementById('sumweight').value;
    var firstkg = document.getElementById('firstkg').value;
    var afterkg = document.getElementById('afterkg').value;
    var diliveryCharge = document.getElementById('diliveryCharge').value;
    var SubTotal = document.getElementById('SubTotal').value;

    //alert(weight);

    if(weight > 1)
    {
        var for1stkg = (1 * firstkg);

        if((weight - 1)>0)
        {
            var foraftertkg = ((weight-1) * afterkg);
        }
    }
    else if(weight == 1)
    {
        var for1stkg = (1 * firstkg);

        var foraftertkg = 0;
    }
    else
    {
        var for1stkg = 0;
        var foraftertkg = 0;
    }

    var sumOfdelivery = for1stkg + foraftertkg;
    var roundAmu = sumOfdelivery.toFixed(2);
    var sumOffinaldelivery = Number(roundAmu) + Number(diliveryCharge);
    var finalSubtot = Number(SubTotal) + Number(roundAmu);

    //alert(foraftertkg);

    document.getElementById('deliveryChange').innerHTML = sumOffinaldelivery.toFixed(2);
    document.getElementById('changeSubtot').innerHTML = finalSubtot.toFixed(2);
    document.getElementById('amount').value = finalSubtot;
    
    //alert(finalSubtot);
}


function changecartDetails() {

    $("#paymentForm").attr("action","https://sandbox.payhere.lk/pay/checkout");

    var diliveryCharge = document.getElementById('diliveryCharge').value;
    var subtotal = document.getElementById('SubTotal').value;
    
    var dileNumber = Number(diliveryCharge);
    var subNumber = Number(subtotal);

    var finalOne = (dileNumber + subNumber);

    document.getElementById('amount').value = subNumber;
    document.getElementById('changeSubtot').innerHTML = subNumber.toFixed(2);
    document.getElementById('deliveryChange').innerHTML = dileNumber.toFixed(2);
}


function validate(form) {
    var off_payment_method = document.getElementsByName('paymethod');
    var ischecked_method = false;
    for ( var i = 0; i < off_payment_method.length; i++) {
        if(off_payment_method[i].checked) {
            ischecked_method = true;
            break;
        }
    }
    if(!ischecked_method)   { //payment method button is not checked
        alert("Please choose Payment Method!!!");
        return false;
    }
}

</script>

@include('frontfootertwo')