<?php 
$home = '';
?>
@include('frontheader')

    <!-- item area start -->
		<section class="item-area">
                <div class="container">
                    
                    <div class="billing-wrap">

                        <div class="account-wrap" style="display">
                            
                                <form action="{{ route('delivery') }}" method="post" id="checkout-form" class="account-inner">
                                        {{ csrf_field() }}
    
                                    <div class="row">
                                        <div class="col-md-7 col-12">
                                            <!-- billing address -->
                                            <div style="display">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h2>Billing Details</h2>
                                                    </div>
                                                </div>

                                                @if($countAddNumbers !=0)
                                                    @foreach ($customer as $customer)
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12 col-12">
                                                            <div class="form-group">
                                                                <label for="">First Name</label>
                                                                <input type="text" id="name" name="name" class="form-control" value="{{ $customer->name }}" required /> 
                                                            </div>                           
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 col-12">
                                                            <div class="form-group">
                                                                <label for="">Last Name</label>
                                                                <input type="text" id="lastname" name="lastname" class="form-control" value="{{ $customer->lastname }}" /> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    @endforeach

                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="">Email</label>
                                                                    <input type="email" value="{{$email}}" id="email" name="email" class="form-control" id="" placeholder="Email" readonly>
                                                                    <input type="hidden" id="countNumbers" name="countNumbers" class="form-control" value="{{$countNumbers}}" />  
                                                                    <input type="hidden" id="getuserId" name="getuserId" class="form-control" value="{{$getuserId}}" /> 
                                                                </div> 
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="">Phone No</label>
                                                                    <input type="number" id="phonenumber" name="phonenumber" value="{{ $getAddress->phone }}" class="form-control" required />
                                                                    <input type="hidden" id="getAddressId" name="getAddressId" value="{{ $getAddress->id }}" class="form-control" />
                                                                </div> 
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="">Address</label>
                                                                    <input type="text" id="addressOne" name="addressOne" value="{{ $getAddress->street_no }}" class="form-control" required />
                                                                    <input type="text" id="addressTwo" name="addressTwo" value="{{ $getAddress->street_name }}" class="form-control" required />
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label for="">Town / City</label>
                                                                    <select id="city" name="city" class="form-control" required>
                                                                        <option value="{{ $getAddress->city }}" disable>
                                                                            <?php
                                                                              $cityFind = DB::select('select town from delivery_rates where id = ?', [$getAddress->city]);
                                                                              echo $cityFind[0]->town;
                                                                            ?> 
                                                                        </option>
                                                                        @foreach ($dilivery as $diliverys)
                                                                            <option value="{{ $diliverys->id }}">{{ $diliverys->town }}</option>
                                                                        @endforeach
                                                                    
                                                                    </select>
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label for="">Province</label>
                                                                    <input type="text" id="province" name="province" class="form-control" value="{{ $getAddress->province }}" required />
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label for="">Postcode</label>
                                                                    <input type="number" id="postcode" name="postcode" value="{{ $getAddress->postal_code }}" class="form-control" required />
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label for="">Company name</label>
                                                                    <input type="text" id="companyname" name="companyname" class="form-control" value="{{ $customer->lastname }}" required /> 
                                                                </div> 
                                                            </div>
                                                        </div>

                                                    @else

                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="">First Name</label>
                                                                    <input type="text" class="form-control" id="name" name="name" placeholder="First Name" required />
                                                                </div>                           
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="">Last Name</label>
                                                                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" />
                                                                </div> 
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="">Email</label>
                                                                    <input type="email" value="{{$email}}" id="email" name="email" class="form-control" id="" placeholder="Email" readonly>
                                                                    <input type="hidden" id="countNumbers" name="countNumbers" class="form-control" value="{{$countNumbers}}" />  
                                                                    <input type="hidden" id="getuserId" name="getuserId" class="form-control" value="{{$getuserId}}" /> 
                                                                </div> 
                                                            </div>
                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="">Phone No</label>
                                                                    <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder="Phone No" required>
                                                                    <input type="hidden" id="getAddressId" name="getAddressId" value="no" class="form-control" />
                                                                </div> 
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                <div class="form-group">
                                                                    <label for="">Address</label>
                                                                    <input type="text" class="form-control" id="addressOne" name="addressOne" placeholder="Street No" required>
                                                                    <input type="text" class="form-control" id="addressTwo" name="addressTwo" placeholder="Address" required>
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label for="">Town / City</label>
                                                                    <select id="city" name="city" class="form-control" required onchange="selectcity();">
                                                                        <option value="" disable> Select </option>
                                                                        @foreach ($dilivery as $diliverys)
                                                                            <option value="{{ $diliverys->id }}">{{ $diliverys->town }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div> 
                                                                <div class="form-group">
                                                                        <label for="">Province</label>
                                                                        <input type="text" id="province" name="province" class="form-control" placeholder="Province" required/>
                                                                    </div> 
                                                                <div class="form-group">
                                                                    <label for="">Postcode</label>
                                                                    <input type="number" class="form-control" id="postcode" name="postcode" placeholder="Postcode" required />
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label for="">Company name</label>
                                                                    <input type="text" class="form-control" id="companyname" name="companyname" placeholder="Company name" required />
                                                                </div> 
                                                            </div>
                                                        </div>

                                                    @endif

                                            </div>
                                        
    
                                        </div>



                                        <div class="col-md-5 col-12">

                                            @include('shop.summeryone')

                                        </div>
                                    </div>

                                    <script>
                                        function selectcity()
                                        {
                                            var city = $("#city").val();
                                            //alert(city);
                                            $.ajax({
                                                url: "{{ url('/') }}/city/"+city,
                                                method: 'GET',
                                                success: function(data){
                                                    $( "#province" ).val(data.cityDetails.province);
                                                    $( "#postcode" ).val(data.cityDetails.post_code);
                                                    // console.log(data);
                                                }
                                            })
                                        }
                                    </script>
    
                                </form>
                            
                        </div>
    
    
                    </div>
    
    
                    <!--  -->
    
                </div>
            </section>

@include('frontfootertwo')