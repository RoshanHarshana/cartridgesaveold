<?php 
$home = '';
?>
@include('frontheader')

@if(Session::has('cart'))
<!-- item area start -->
    <section class="item-area">
        <div class="container">
            
            <div class="billing-wrap padding-none">

                    <div class="cart-wrap" style="display">
                        <div class="d-flex header-cart">
                            <div class="cart-col image-col"></div>
                            <div class="cart-col name-col"><strong>Name</strong></div>
                            <div class="cart-col delivery-col"><strong>Delivery</strong></div>
                            <div class="cart-col quantity-col"> <strong>Quantity</strong> </div>
                            <!-- <div class="cart-col vat-col"> <p>Price excluding VAT</p> </div> -->
                            <div class="cart-col vat-col"> <strong>Price</strong> </div>
                            <div class="cart-col total-col"> <strong>Subtotal</strong> </div>
                        </div>



                        <?php
                            $sumVat = 0;
                            $sumWeight = 0;
                            $totalQty = 0;
                        ?>
                        @foreach($products as $product)
                            <div class="d-flex item-cart clearfix">
                                <div class="cart-col image-col">
                                    <div class="cart-item">
                                        <button data-id="{{ $product['item']['id'] }}" data-stock="{{ $product['item']['stock'] }}" class="clear-btn product-cart-remove"></button>
                                        <img src="{{ url('/') }}/image/{{ $product['item']['image'] }}" alt="search">
                                    </div>
                                </div>
                                <?php
                                    $delivery = $product['item']['delivery_type'];
                                    $deliveryType = App\Classes\CeckDeliveryType::checkDelitype($delivery);
                                ?>
                                <div class="cart-col name-col"> <p class="semi-bold ">{{ $product['item']['name'] }}</p> </div>
                                <div class="cart-col delivery-col"> <p>{{ $deliveryType }}</p> </div>
                                <div class="cart-col quantity-col"> 
                                    <div class="quantity-wrap">
                                        <div class="quantity-wrap-inner">
                                            <button data-id="{{ $product['item']['id'] }}" data-qty="{{ $product['qty'] }}" data-stock="{{ $product['item']['stock'] }}" data-side="max" class="quantity-wrap-btn in product-cart-change"></button>
                                                <input type="number" value="{{$product['qty']}}" readonly class="quantity-wrap-input quantity-wrap-input-change">
                                            <button data-id="{{ $product['item']['id'] }}" data-qty="{{ $product['qty'] }}" data-stock="{{ $product['item']['stock'] }}" data-side="min" class="quantity-wrap-btn de product-cart-change"></button>
                                        </div>
                                    </div>
                                    <!-- <a class="clear-btn" href="{{ url('/') }}/remove/{{ $product['item']['id'] }}">X</a> -->
                                </div>
                                <div class="cart-col vat-col"> <p>{{ number_format($product['price'],2) }}</p> </div>
                                <div class="cart-col total-col"> <p>{{ number_format($product['price'],2) }}</p> </div>
                                <?php
                                    $vat = $product['item']['vat'];
                                    $price = $product['item']['price'];
                                    $weight = $product['item']['weight'];
                                    $qty = $product['qty'];
 
                                $withvatPrice = App\Classes\Calvat::calvatOne($price, $vat, $qty);
                                $vatPrice = App\Classes\Calvat::calvatTwo($price, $vat, $qty);

                                //dd($vatPrice);
                                // $sumVat += $vatPrice;
                                $sumVat = 0;
                                $sumWeight += $weight * $qty;
                                $totalQty += $qty;

                                ?>
                            </div>
                        @endforeach




                        <div class="total-cart">
                            <div class="d-flex">
                                <div class="cart-col total-calc-col"> <p>Subtotal</p> </div>
                                <div class="cart-col total-col"> <p class="big-price"> <span>LKR {{number_format($totalPrice,2)}}</span>  </p> </div>
                            </div>
                            <div class="d-flex">
                                <div class="cart-col total-calc-col"> <p>Delivery</p> </div>
                                <div class="cart-col total-col"> <p class="big-price"> <span>LKR {{number_format($diliveryCharge,2)}}</span> </p> </div>
                            </div>
                            <!-- <div class="d-flex">
                                <div class="cart-col total-calc-col"> <p>VAT</p> </div>
                                <div class="cart-col total-col"> <p>LKR {{number_format($sumVat,2)}}</p> </div>
                            </div> -->
                        </div>

                        <div class="total-cart">
                            <div class="d-flex">
                                <!-- <div class="cart-col total-calc-col"> <p>Total including VAT and delivery</p> </div> -->
                                <div class="cart-col total-calc-col"> <p class="semi-bold ">Total including with delivery</p> </div>
                                <div class="cart-col total-col"> <p class="semi-bold ">LKR 
                                    <span>
                                        <?php
                                            $SubTotal = App\Classes\SubTotal::calsubTotal($totalPrice, $sumVat, $diliveryCharge);
                                            echo number_format($SubTotal,2);
                                        ?>
                                    </span> 
                                </p> </div>
                            </div>
                            <div class="d-flex flex-row-reverse">
                                <div class="cart-col checkout"> <a href="{{ url('/') }}/checkout" class="theme-btn theme-btn2"> Go to Checkout </a> </div>
                            </div>
                        </div>
                        
                    </div>

            </div>

        </div>
    </section>

@else



<!-- item area start -->
<section class="item-area">
    <div class="container">
        
        <div class="billing-wrap">

            <div class="account-wrap" style="display">
                    <form class="account-inner">
                        <div class="row">
                            <div class="col-md-5 col-12">
                                <!-- payment method -->
                                <div style="display">
                                    <div class="row">
                                        <div class="col-12">
                                            <h2>No Items</h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div>
                                                <p><a href="{{ url('/') }}/">Back</a></p>
                                            </div>              
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- done  -->

                        </div>
                    </form>
            </div>


        </div>


        <!--  -->

    </div>
</section>


 
@endif

@include('frontfootertwo')