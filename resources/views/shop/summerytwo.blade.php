<div class="row">
    <div class="col-12 itemcart">
        <h2>Items in Cart</h2>


        <?php
        $sumVat = 0;
        $sumWeight = 0;
        $totalQty = 0;
        ?>
        @foreach($products as $product)
                <?php
                    $qty = $product['qty'];
                    $productPrice = $product['price'];
                    $productName = $product['item']['name'];
                    $productUrl = $product['item']['url'];
                    $productBrand = $product['item']['brand_id'];
                    $vat = $product['item']['vat'];
                    $price = $product['item']['price'];
                    $weight = $product['item']['weight'];

                $withvatPrice = App\Classes\Calvat::calvatOne($price, $vat, $qty);

                $brand = App\Classes\Getbrand::SearchBrand($productBrand);

                $vatPrice = App\Classes\Calvat::calvatTwo($price, $vat, $qty);

                $sumVat += $vatPrice;
                $sumWeight += $weight* $qty;
                $totalQty += $qty;

                $delivery = $product['item']['delivery_type'];
                $deliveryType = App\Classes\CeckDeliveryType::checkDelitype($delivery);

                ?>

            <div class="d-flex summary-items">
                <div class="flex-fill"> <p class="spec-para">{{$brand}} - {{$productUrl}} </p><span><strong style="font-size:13px">(X{{$qty}})</strong></span> <p class="small-small"> {{$productName}}</p> </div>
                <div class="price-wrap"> <p>LKR {{number_format($productPrice,2)}}</p> </div>
            </div>

        @endforeach

        <?php
            $totalPrice = $totalPrice;
            $SubTotal = App\Classes\SubTotal::calsubTotal($totalPrice, $sumVat, $diliveryCharge);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-12 itemcart">
        <br>
    </div>
</div>

<div class="row">
    <div class="col-12 subtotal">
        <div class="summery-h2"><h2>Summary</h2></div>
        <div class="d-flex">
            <div class="flex-fill"><p>Subtotal</p></div>
            <div class="price-wrap"><p>LKR {{number_format($totalPrice,2)}}</p></div>
        </div>
        <div class="d-flex">
            <div class="flex-fill"><p>Delivery</p></div>
            <div class="price-wrap"><p>LKR <label id="deliveryChange">{{number_format($diliveryCharge,2)}}</label></p></div>
        </div>
        <!-- <div class="d-flex">
            <div class="flex-fill"><p>Vat</p></div>
            <div class="price-wrap"><p>LKR {{number_format($sumVat,2)}}</p></div>
        </div> -->
        <div class="d-flex text-align-right subtotal-total">
            <div class="flex-fill"><strong>Total</strong></div>
            <div class="price-wrap"><strong>LKR <label id="changeSubtot">{{number_format($SubTotal,2)}}</label></strong></div>
        </div>
        <input type="hidden" id="totalqty" name="totalqty" class="form-control" value="{{$totalQty}}" /> 
        <input type="hidden" id="sumweight" name="sumweight" class="form-control" value="{{$sumWeight}}" /> 
        <input type="hidden" id="vatTotal" name="vatTotal" class="form-control" value="{{$sumVat}}" />
        <input type="hidden" id="total" name="total" class="form-control" value="{{$totalPrice}}" /> 
        <input type="hidden" value="{{$SubTotal}}" id="SubTotal" name="SubTotal" />
    </div>
</div>

<div class="row">
    <div class="col-12 itemcart">
        <div class="d-flex flex-row-reverse">
            <div class="next">
                 {{-- <a href="#" class="theme-btn theme-btn2"> Next <img src="{{ asset('frontassets/img/arrow_more_button.png') }}" alt="arrow_more_button" /></a>  --}}

                 <button class="theme-btn theme-btn3" type="submit" class="btn btn-success">Next</button>
            </div>
        </div>
    </div>
</div>