<!DOCTYPE HTML>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="@yield('description')"/>
		<meta charset="UTF-8">
		{{-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> --}}
		
		<title id="titleId">Cheap Printer Ink Cartridges, Laser Toner &amp; Inkjet Printer Cartridges</title>
		<link href="{{ asset('img/favicon.png') }}" rel="shortcut icon">
		<!-- Font Awesome CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/font-awesome.min.css') }}" media="all" />
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/bootstrap.min.css') }}" media="all" />
		<!-- Fanacy box -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/jquery.fancybox.min.css') }}" media="all" />
		<!-- Main style CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/style.css') }}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/style-v2.css') }}" media="all" />
		<!-- Responsive CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/responsive.css') }}" media="all" />
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
		@media print {
			#myPrntbtn {
				display :  none;
			}
		}
		</style>
	</head>
	<body class="<?php if($home) echo 'default-body'; ?>">
		<!-- Page loader -->
		<div id="preloader"></div>