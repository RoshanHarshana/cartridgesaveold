<?php $home = ''; ?>
@include('frontheader')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqXYAhrMxBcZm4jU9mlbiC3Utaoxnxw6w"></script>

    <section class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Contact</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="map-wrap">
                        <div id="map">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="contact-details">
                        <div class="contact-details-item location">
                            <p>
                                <strong>Address</strong> 
                                <!-- BCON Solutions (PVT) LTD 34/1/42, <br>
                                St. Sebastian Hill, Hulftsdorp,  <br>
                                Colombo 12 -->
                                No 16, Sri Bodhirukkarama Road, <br>
                                Colombo 06.
                            </p>
                        </div>

                        <div class="contact-details-item phone">
                            <p>
                                <strong>Phone</strong> 
                                <a href="callto:94114365179">+94 11 436 51 79</a>
                            </p>
                        </div>

                        <div class="contact-details-item mail">
                            <p>
                                <strong>Email</strong> 
                                <!-- asiri@nimrald.com -->
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 6.8676528, lng:  79.8611496},
                zoom: 12,
                disableDefaultUI: true,
                styles: [
                        {
                            "featureType": "administrative",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": "-100"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.province",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 65
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": "50"
                                },
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": "-100"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "lightness": "30"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "lightness": "40"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "hue": "#ffff00"
                                },
                                {
                                    "lightness": -25
                                },
                                {
                                    "saturation": -97
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels",
                            "stylers": [
                                {
                                    "lightness": -25
                                },
                                {
                                    "saturation": -100
                                }
                            ]
                        }
                    ]
            });
            var iconBase = "{{ url('/') }}/frontassets/img/contact_location.png";
            new google.maps.Marker({
                position : { lat : parseFloat(6.8676528) , lng :  parseFloat(79.8611496)},
                icon: iconBase,
                map: map
            });
        }

        google.maps.event.addDomListener(window, 'load', initMap);
    </script>

@include('frontfootertwo')