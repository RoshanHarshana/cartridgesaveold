@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <?php

      $sideRole = auth()->user()->role_id;
      $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);

      $products = DB::select('select * from products');
      $numProducts = count($products);

      $printers = DB::select('select * from printers');
      $numPrinters = count($printers);

      $questions = DB::select('select * from questions');
      $numQuestions = count($questions);

      $orders = DB::select('select * from orders where status =?', ['pending']);
      $numOrders = count($orders);
      
    ?>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $numOrders; ?></h3>

              <p>New Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{ url('/') }}/orderreport" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $numProducts; ?></h3>

              <p>Products</p>
            </div>
            <div class="icon">
              <i class="fa fa-wrench"></i>
            </div>
            @foreach ($sidePermissions as $sidePermission)
                @if ($sidePermission->name == 'Product')
                  @if ($sidePermission->index == 1)
                    <a href="{{ url('/') }}/product" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  @endif
                @endif
            @endforeach
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $numPrinters; ?></h3>

              <p>Printers</p>
            </div>
            <div class="icon">
              <i class="fa fa-print"></i>
            </div>
            @foreach ($sidePermissions as $sidePermission)
                @if ($sidePermission->name == 'Printer')
                  @if ($sidePermission->index == 1)
                    <a href="{{ url('/') }}/printer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  @endif
                @endif
            @endforeach
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $numQuestions; ?></h3>

              <p>Questions & Answers</p>
            </div>
            <div class="icon">
              <i class="fa fa-question-circle-o"></i>
            </div>
            @foreach ($sidePermissions as $sidePermission)
                @if ($sidePermission->name == 'FAQ')
                  @if ($sidePermission->index == 1)
                    <a href="{{ url('/') }}/answer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  @endif
                @endif
            @endforeach
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
              <li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
              <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
              <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
            </div>
          </div>
          <!-- /.nav-tabs-custom -->

          <!-- Questions & Answers -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Questions & Answers</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
              <ul class="todo-list">

                  @foreach ($questions as $question)
                  <?php
                    $questionId = $question->id;
                    $answers = DB::select('select * from answers where question_id = ?', [$questionId]);
                    if ($answers) { 
                      
                    }
                    else 
                    {
                      ?>
                        <li>
                            <!-- drag handle -->
                            <span class="handle">
                                  <i class="fa fa-ellipsis-v"></i>
                                  <i class="fa fa-ellipsis-v"></i>
                                </span>
                            <!-- todo text -->
                            <span class="text" style="color:red;">{{$question->question}}</span>
                            <!-- General tools such as edit or delete-->
                            <div class="tools">

                                @foreach ($sidePermissions as $sidePermission)
                                    @if ($sidePermission->name == 'FAQ')
                                      @if ($sidePermission->edit == 1)
                                        <a href="{{ url('/') }}/answer/create/{{ $question->id }}"><i class="fa fa-edit"></i></a>
                                      @endif
                                    @endif
                                @endforeach

                                @foreach ($sidePermissions as $sidePermission)
                                    @if ($sidePermission->name == 'FAQ')
                                      @if ($sidePermission->delete == 1)
                                        <a href="#" onclick="
                                        var result = confirm('Are you Delete this one!');
                                          if( result )
                                          {
                                              event.preventDefault();
                                              document.getElementById('delete-form{{ $question->id }}').submit();
                                          }
                                  "><i class="fa fa-trash-o"></i></a>
                                  
                                          <form id="delete-form{{ $question->id }}" action="{{ url('/question/delete', ['id' => $question->id]) }}"
                                                  method="POST" style="display: non;">
                                                  <input type="hidden" name="_method" value="delete">
                                                  {{ csrf_field() }}
                                          </form>
                                      @endif
                                    @endif
                                @endforeach
                            </div>
                          </li>
                      <?php
                    }
                    
                  ?>

                  @endforeach

              </ul>
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

          <!-- solid sales graph -->
          <div class="box box-solid bg-teal-gradient">
            <div class="box-header">
              <i class="fa fa-th"></i>

              <h3 class="box-title">Sales Graph</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <div class="box-body border-radius-none">
              <div class="chart" id="line-chart" style="height: 250px;"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-border">
              <div class="row">
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60"
                         data-fgColor="#39CCCC">

                  <div class="knob-label">Mail-Orders</div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60"
                         data-fgColor="#39CCCC">

                  <div class="knob-label">Online</div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4 text-center">
                  <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60"
                         data-fgColor="#39CCCC">

                  <div class="knob-label">In-Store</div>
                </div>
                <!-- ./col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')

    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

@endsection