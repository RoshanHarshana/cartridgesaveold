@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add
        <small>Brand</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add new Brand</h3>
                </div>
                
                @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif

                @if (session('alert2'))
                    <div class="alert alert-danger">
                        {{ session('alert2') }}
                    </div>
                @endif

                <form id="brandformstore" method="post" action="{{ route('brand.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                  <div class="box-body">
                    <div class="form-group col-md-12">
                      <label for="brandnamee" control-label>Brand Name</label>
                      <input type="text" name='brandname' class="form-control" id="brandname" required placeholder="Enter brand name!">
                    </div>
                    <div class="form-group col-md-12">
                      <label for="brandsimage">File input</label>
                      <input type="file" id="image" name="image" size="1MB" required />
                      <p class="help-block">Upload printer image!</p>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="description" control-label>Description</label>
                      <textarea class="form-control" rows="5" name="description" id="description" placeholder="Enter description..." required></textarea>
                    </div>
                  </div>

                  <div class="box-footer">
                    <button id='submit' type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>


          </div>
          
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

 


@endsection
