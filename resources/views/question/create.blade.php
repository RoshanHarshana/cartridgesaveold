@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add
        <small>Question</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New Question</h3>
                </div>
                
                @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif
                @if (session('alert2'))
                    <div class="alert alert-danger">
                        {{ session('alert2') }}
                    </div>
                @endif

                <form id="Questionformstore" method="post" action="{{ route('question.store') }}">
                {{ csrf_field() }}
                  <div class="box-body">
                    <div class="form-group col-md-12">
                      <label for="productid" control-label>Product</label>
                      <select class="form-control select2" id="productid" name="productid" style="width: 100%;" required>
                        <option value="">--please select--</option>

                        @foreach ($product as $product)
                          <option value="{{$product->id}}">{{$product->name}}</option>
                        @endforeach

                      </select>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="Questionname" control-label>Question</label>
                      <textarea class="form-control" rows="5" name="question" id="question" placeholder="Enter Question..." required></textarea>
                    </div>

                  </div>

                  <div class="box-footer">
                    <button id='submit' type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>


          </div>
          
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

 


@endsection
