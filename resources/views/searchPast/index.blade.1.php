<?php
    $UrlPast = "search=".$searchid;
?>
@include('FrontParts/links')<br><br>

@foreach ($resultPrinters as $resultPrinter)
    @if($resultPrinter->category->name == 'toner-cartridges' || $resultPrinter->category->name == 'ink-cartridges' || $resultPrinter->category->name == 'ribbon-cartridges')
        {{$resultPrinter->name}} - <a href="{{ url('/') }}/{{$resultPrinter->category->name}}/{{$resultPrinter->brand->name}}/{{$resultPrinter->name}}.html">/{{$resultPrinter->category->name}}/{{$resultPrinter->brand->name}}/{{$resultPrinter->name}}</a><br>
    @else 
        <P style="color:red">This category name doesn't match for URL name! Please check that back-end!</p>
    @endif
@endforeach
________________________________________________________________________________________________________________________________________________<br><br>
@foreach ($resultProducts as $resultProduct)
    <img src="{{ url('/') }}/image/{{ $resultProduct->image }}" width="250px" height="250px" /><br>
    {{$resultProduct->name}} - <a href="{{ url('/') }}/{{$resultProduct->name}}.html">{{$resultProduct->name}}</a><br>
    @foreach($resultProduct->features as $feature)
        {{ $feature->name }}<br>
    @endforeach<br><br>

    <?php $sum = 0 ?>
    @foreach($resultProduct->pages as $page)
        {{ $page->color->name }}-pages={{ $page->pages_nos }}-multiply={{ $page->multiply }}-capacity={{ $page->capacity }}<br>
        <?php $sum += $page->pages_nos ?>
    @endforeach<br><br>

    <?php 
        $vat = $resultProduct->vat;
        $price = $resultProduct->price;
    ?>
    Sum = {{ $sum }}<br>
    PP value = {{ $sum/$price }}<br>
    Rs.{{$price}}/=<br>
    vat {{$vat}}%<br>
    With vat Rs.
    <?php 
    echo $withvatPrice = App\Classes\Calvat::withVat($price, $vat);
    ?><br>
    <br><br>

   
    <p><a href="{{ url('/') }}/add-to-cart/{{ $resultProduct->id }}/{{ $UrlPast }}" class="btn btn-primary" role="button">Add To Cart</a></p>
    <br><br><br>

@endforeach