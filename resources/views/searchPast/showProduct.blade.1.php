@include('FrontParts/links')<br>

@foreach ($resultProducts as $resultProduct)
    <img src="{{ url('/') }}/image/{{ $resultProduct->image }}" width="250px" height="250px" /><br>
    {{ $resultProduct->name }}-{{ $resultProduct->category->name }}<br>
    @foreach($resultProduct->features as $feature)
        {{ $feature->name }}<br>
    @endforeach<br><br>

    <?php $pages = 0 ?>
    @foreach($resultProduct->pages as $page)
        {{ $page->color->name }}-pages={{ $page->pages_nos }}-multiply={{ $page->multiply }}-capacity={{ $page->capacity }}<br>
        <?php $pages += $page->pages_nos ?>
    @endforeach<br><br>

    <?php

        $proId = $resultProduct->id;
        $price = $resultProduct->price;
        $vat = $resultProduct->vat;

    ?>

    <!-- get compatibility... -->
    ________________________________________________________________________________________________________________________________________________<br><br>
    Compatibility<br>
    <?php 
        $compatiProduct = App\Classes\Compatibility::getCompatibility($proId);
    ?>
    @foreach ($compatiProduct as $compatiProduct)
        {{ $compatiProduct->name }}
    @endforeach
    <br>
    ________________________________________________________________________________________________________________________________________________<br><br>
    <!-- get end compatibility... -->
    
    Pages = {{ $pages }}<br>
    PP value = 
    <?php 
    echo $ppvalue = App\Classes\Ppvalue::CalppValue($pages, $price);
    ?><br>
    Rs.{{ $price }}/=<br>
    vat {{ $vat }}%<br>
    With vat Rs.
    <?php 
    echo $withvatPrice = App\Classes\Calvat::withVat($price, $vat);
    ?><br>

    Description = {{ $resultProduct->description }}<br>

    <!-- {{-- get attributes... --}} -->
    <?php 
        $idProd = $resultProduct->id;
        $attributes = App\Attribute::where('product_id', $idProd)->get();
    ?>
    <!-- {{-- End get attributes... --}} -->

    @foreach ($attributes as $attribute)
        @if($attribute->name == 'related_compatibility')
        Related Compatibility = {{ $attribute->value }}<br>
        @endif
    @endforeach
    ________________________________________________________________________________________________________________________________________________<br><br>

    <p><a href="{{ url('/') }}/add-to-cart/{{ $resultProduct->id }}/{{ $resultProduct->name }}.html" class="btn btn-primary" role="button">Add To Cart</a></p>
    <br><br><br>

@endforeach