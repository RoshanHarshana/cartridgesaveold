@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categories
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Categories List</h3>
            </div>

            @if (session('alert'))
                <div class="alert alert-success">
                    {{ session('alert') }}
                </div>
            @endif

            @if (session('alert2'))
                <div class="alert alert-danger">
                    {{ session('alert') }}
                </div>
            @endif

            <?php
              $sideRole = auth()->user()->role_id;
              $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
            ?>

          <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Edit</th>
                <th>Delete</th>                
              </tr>
              </thead>
              <tbody>
                  <?php $i = 0; ?>
              @foreach($category as $category)
                  <?php $i++ ?>
              <tr>
                <td>{{ $i }}</td>
                <td>{{ $category->name }}</td>
                <td>
                  @foreach ($sidePermissions as $sidePermission)
                      @if ($sidePermission->name == 'Category')
                        @if ($sidePermission->edit == 1)
                          <a href="{{ url('/') }}/category/{{ $category->id }}/edit"><i class="fa fa-edit" style="font-size:25px;  color:green"></i></a>
                        @else
                          <p style="color:red">No Access!!!</p>
                        @endif
                      @endif
                  @endforeach
                </td>
                <td>
                  @foreach ($sidePermissions as $sidePermission)
                      @if ($sidePermission->name == 'Category')
                        @if ($sidePermission->delete == 1)
                          
                        <a href="#" onclick="
                        var result = confirm('Are you Delete this one!');
                          if( result )
                          {
                              event.preventDefault();
                              document.getElementById('delete-form{{ $category->id }}').submit();
                          }
                  "><i class="fa fa-trash-o" style="font-size:25px;color:red"></i></a>
                  
                          <form id="delete-form{{ $category->id }}" action="{{ route('category.destroy',[$category->id]) }}"
                                  method="POST" style="display: non;">
                                  <input type="hidden" name="_method" value="delete">
                                  {{ csrf_field() }}
                          </form>

                        @else
                          <p style="color:red">No Access!!!</p>
                        @endif
                      @endif
                  @endforeach
                </td>
                        
              </tr>
              @endforeach
              
              </tfoot>
            </table>
                        
          </div>


        </div>
        <!-- /.box -->


      </div>
    </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

@endsection