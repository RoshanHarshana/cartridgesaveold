
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <i class="fa fa-user-circle-o" style="font-size:40px;color:white"></i>
      </div>
      <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
        <i class="fa fa-circle text-success"></i> Online
      </div>
    </div>
    <!-- search form -->
    {{-- <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form> --}}
    <!-- /.search form -->


    <?php

      $sideRole = auth()->user()->role_id;
      $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
      
    ?>


    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

      {{-- <li class="treeview">
        <a href="#">
          <i class="fa fa-clone"></i>
          <span>Manage Categories</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @foreach ($sidePermissions as $sidePermission)
              @if ($sidePermission->name == 'Category')
                @if ($sidePermission->create == 1)
                  <li><a href="{{ url('/category/create') }}"><i class="fa fa-circle-o"></i>Add Category</a></li>
                @endif
                @if ($sidePermission->index == 1)
                  <li><a href="{{ url('/category') }}"><i class="fa fa-circle-o"></i>View Categories</a></li>
                @endif
              @endif
          @endforeach
        </ul>
      </li> --}}

      {{-- <li class="treeview">
        <a href="#">
          <i class="fa fa-adjust"></i>
          <span>Manage Colors</span>
          <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @foreach ($sidePermissions as $sidePermission)
              @if ($sidePermission->name == 'Color')
                @if ($sidePermission->create == 1)
                  <li><a href="{{ url('/color/create') }}"><i class="fa fa-circle-o"></i>Add Color</a></li>
                @endif
                @if ($sidePermission->index == 1)
                  <li><a href="{{ url('/color') }}"><i class="fa fa-circle-o"></i>View Colors</a></li>
                @endif
              @endif
          @endforeach
        </ul>
      </li> --}}

        <li class="treeview">
          <a href="#">
            <i class="fa fa-diamond"></i>
            <span>Manage Brands</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @foreach ($sidePermissions as $sidePermission)
                @if ($sidePermission->name == 'Brand')
                  @if ($sidePermission->create == 1)
                    <li><a href="{{ url('/brand/create') }}"><i class="fa fa-circle-o"></i>Add Brand</a></li>
                  @endif
                  @if ($sidePermission->index == 1)
                    <li><a href="{{ url('/brand') }}"><i class="fa fa-circle-o"></i>View Brands</a></li>
                  @endif
                @endif
            @endforeach
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-print"></i>
            <span>Manage Printers</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @foreach ($sidePermissions as $sidePermission)
                @if ($sidePermission->name == 'Printer')
                  @if ($sidePermission->create == 1)
                    <li><a href="{{ url('/printer/create') }}"><i class="fa fa-circle-o"></i>Add Printer</a></li>
                  @endif
                  @if ($sidePermission->index == 1)
                    <li><a href="{{ url('/printer') }}"><i class="fa fa-circle-o"></i>View Printers</a></li>
                  @endif
                @endif
            @endforeach
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-wrench"></i>
            <span>Manage Products</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @foreach ($sidePermissions as $sidePermission)
                @if ($sidePermission->name == 'Product')
                  @if ($sidePermission->create == 1)
                    <li><a href="{{ url('/product/create') }}"><i class="fa fa-circle-o"></i>Add Product</a></li>
                  @endif
                  @if ($sidePermission->index == 1)
                    <li><a href="{{ url('/product') }}"><i class="fa fa-circle-o"></i>View Products</a></li>
                  @endif
                @endif
            @endforeach
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-question-circle-o"></i>
            <span>Manage FAQ</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @foreach ($sidePermissions as $sidePermission)
                @if ($sidePermission->name == 'FAQ')
                  @if ($sidePermission->create == 1)
                    <li><a href="{{ url('/question/create') }}"><i class="fa fa-circle-o"></i>Add Questions</a></li>
                  @endif
                  @if ($sidePermission->index == 1)
                    <li><a href="{{ url('/question') }}"><i class="fa fa-circle-o"></i>View Questions</a></li>
                    <li><a href="{{ url('/answer') }}"><i class="fa fa-circle-o"></i>Questions & Answers</a></li>
                  @endif
                @endif
            @endforeach
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-money"></i>
            <span>Manage Delivery Rates</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @foreach ($sidePermissions as $sidePermission)
                @if ($sidePermission->name == 'Rate')
                  @if ($sidePermission->create == 1)
                    <li><a href="{{ url('/diliveryrate/create') }}"><i class="fa fa-circle-o"></i>Add Rates</a></li>
                  @endif
                  @if ($sidePermission->index == 1)
                    <li><a href="{{ url('/diliveryrate') }}"><i class="fa fa-circle-o"></i>View Rates</a></li>
                  @endif
                @endif
            @endforeach
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Manage Users</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @foreach ($sidePermissions as $sidePermission)
                @if ($sidePermission->name == 'User')
                  @if ($sidePermission->create == 1)
                    <li><a href="{{ url('/user/create') }}"><i class="fa fa-circle-o"></i>Add User</a></li>
                  @endif
                  @if ($sidePermission->index == 1)
                    <li><a href="{{ url('/user') }}"><i class="fa fa-circle-o"></i>View Users</a></li>
                  @endif
                @endif
            @endforeach
          </ul>
        </li>


        @if ($sideRole == 1 || $sideRole == 3)
          <li class="treeview">
            <a href="#">
              <i class="fa fa-book"></i>
              <span>Manage Reports</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ url('/orderreport') }}"><i class="fa fa-circle-o"></i>Order Report</a></li>
                <li><a href="{{ url('/paymentreport') }}"><i class="fa fa-circle-o"></i>Payment Report</a></li>
                <li><a href="{{ url('/salesreport') }}"><i class="fa fa-circle-o"></i>Sales Report</a></li>
                {{-- <li><a href="{{ url('/mail') }}"><i class="fa fa-circle-o"></i>Mail</a></li> --}}
            </ul>
          </li>
        @endif

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
