		<!-- footer section start -->
		<footer class="footer footer-home">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="footer-content">
                                <a href="callto:94114365179"><i class="fa fa-phone"></i>(+94) 11 436 51 79</a>
                                <p>&copy; Copyright at <a href="#">cartridgezone.lk</a> - All rights reserved. 2010 - 2019 - <span>Powered by <a  href="//sevensigns.lk" target="_blank">sevensigns.lk</a></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- footer section end -->

            <!-- jquery main JS -->
            <script src="{{ asset('frontassets/js/jquery.min.js') }}"></script>
            <!-- Bootstrap JS -->
            <script src="{{ asset('frontassets/js/bootstrap.min.js') }}"></script>
            <!-- Fanacy box -->
		    <script src="{{ asset('frontassets/js/jquery.fancybox.min.js') }}"></script>
            <!-- main JS -->
            <script src="{{ asset('frontassets/js/main.js') }}"></script>

            <!--javascript function- after press btn-->
            <script>
                function presssubmit()
                {
                    var searchresult = document.getElementById('searchval').value;
                    var section = document.getElementById('section').value;
                    if(searchresult !='')
                    {
                        location.href = "{{ url('/') }}/search="+section+"/"+searchresult;
                    }
                }
            </script>
            <!--end function-->

            <!--javascript function- after press btn-->
            <script>
                $('#searchval').keypress(function (e) {
                    var key = e.which;
                    if(key == 13)  // the enter key code
                    {
                        $('#searchvalbtn').trigger('click');
                        return false;  
                    }
                });

                $(window).scroll(function() {
                    if($(window).scrollTop() == $(document).height() - $(window).height()) {
                        // ajax call get data from server and append to the div
                    }
                });
            </script>
            <!--end function-->


            <!-- payment turms and conditions-->
            <div class="confirm-popup" id="confirm-terms" style="border-radius:10px; width:800px; text-align:left; font-size:12px">
                <!-- <table style="width:200px">
                    <tr>
                        <td style="text-align:left">otifhjgthjgtrfhfg rfthhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhtgh hfrtgggggggggggggggggggggg</td>
                        <td>otifhjgthjgtrfhfg rfthhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhtgh hfrtgggggggggggggggggggggg</td>
                    </tr>
                    <tr>
                        <td>otifhjgthjgtrfhfg rfthhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhtgh hfrtgggggggggggggggggggggg</td>
                        <td>otifhjgthjgtrfhfg rfthhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhtgh hfrtgggggggggggggggggggggg</td>
                    </tr>
                </table> -->

                <div class="article delivery_information contents_for_lightbox">
                <h2>Delivery Options</h2>
                <table class="delivery_options table" style="font-size:12px">
                <tbody>
                <tr><th></th><th>Timescale</th><th>Method</th><th class="last">Charge</th></tr>
                <tr class="free">
                <td class="ref"><strong>Free next-day</strong></td>
                <td>98% of orders arrive the next working day</td>
                <td>Courier</td>
                <td class="last">FREE</td>
                </tr>
                <tr>
                <td class="ref"><strong>Guaranteed next-day</strong></td>
                <td><a class="info" href="#" data-hasqtip="0" aria-describedby="qtip-0">Guaranteed to arrive on the next working day <span class="fa fa-info-circle"></span></a>

                <div class="more_info right hide">
                <strong>Guaranteed next-day</strong><br>
                <!-- In the extremely unlikely event that the courier does not attempt delivery on the next working day, you will be given:<br>
                - A 10% refund of the total order value<br>
                - A refund of £2.95 next-day courier charge<br>
                - A further voucher giving you 10% off your next order<br> -->
                </div>
                </td>
                <td>Courier</td>
                <td class="last">£2.95</td>
                </tr>
                <tr>
                <td class="ref"><strong>Guaranteed pre-12</strong></td>
                <td><a class="info" href="#" data-hasqtip="1" aria-describedby="qtip-1">Guaranteed to arrive before 12 noon on the next working day <span class="fa fa-info-circle"></span></a>

                <div class="more_info right hide">
                <strong>Guaranteed pre-12</strong><br>
                <!-- In the extremely unlikely event that the courier does not attempt delivery before 12 noon on the next working day, you will be given:<br>
                - A 10% refund of the total order value<br>
                - A refund of £7.95 pre-12 courier charge<br>
                - A further voucher giving you 10% off your next order<br> -->
                </div>
                </td>
                <td>Courier</td>
                <td class="last">£7.95</td>
                </tr>
                </tbody>
                </table>
                <!-- * Depending on size and value, some orders may be upgraded from Royal Mail to Courier. -->
                *Delivery schedule – please keep as it is & please only change the cut off time 5.15pm to 4.15 p.m.
                <br><br>
                <h2>Delivery Schedule</h2>
                <table>
                    <tr>
                        <td>
                            <table class="schedule">
                            <tbody>
                            <tr><th>Order placed</th><th class="last">Expect to receive</th></tr>
                            <tr>
                            <td class="ref"><strong>Monday, before 5:15pm</strong></td>
                            <td class="last">Next day (Tuesday)</td>
                            </tr>
                            <tr>
                            <td class="ref"><strong>Tuesday, before 5:15pm</strong></td>
                            <td class="last">Next day (Wednesday)</td>
                            </tr>
                            <tr>
                            <td class="ref"><strong>Wednesday, before 5:15pm</strong></td>
                            <td class="last">Next day (Thursday)</td>
                            </tr>
                            <tr>
                            <td class="ref"><strong>Thursday, before 5:15pm</strong></td>
                            <td class="last">Next day (Friday)</td>
                            </tr>
                            <tr>
                            <td class="ref"><strong>Friday, before 5:15pm</strong></td>
                            <td class="last">Monday</td>
                            </tr>
                            <tr>
                            <td class="ref"><strong>Saturday or Sunday</strong></td>
                            <td class="last">Tuesday</td>
                            </tr>
                            </tbody>
                            </table>
                        </td>
                        <td style="width:20px"></td>
                        <td>
                            <div class="extra_delivery_info">
                            <h3>Bank & Mercantile Holidays.</h3>
                            We do not despatch or deliver orders on bank & mercantile holidays
                            <br><br>
                            <h3>North & Eastern Province</h3>
                            Deliveries may take 2 to 3 days longer to arrive
                            <br><br>
                            <h3>International deliveries</h3>
                            Keep as same & only change UK to Sri Lanka.
                            </div>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
            <!-- payment turms and conditions -->



        </body>
    </html>