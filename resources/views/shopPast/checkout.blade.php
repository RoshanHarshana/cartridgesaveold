@extends('layouts.app')

@section('content')

        <hr>

<div class="row">
    <div class="col-sm-1">

    </div>
    <div class="col-sm-6">
        <h1>Checkout!</h1>
        <h4>Your Total Is: ${{ $total }}</h4>
        <div id="charge-error" class="alert alert-danger {{ !Session::has('error') ? 'hidden' : '' }}">
            {{ Session::get('error') }}
        </div>
        <form action="{{ route('checkemail') }}" method="post" id="checkout-form">
                {{ csrf_field() }}
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="name">Email-Address</label>
                        <input type="email" id="email" name="email" class="form-control" required placeholder="Enter Email!" /> 
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <button class="btn btn-success" type="submit">Continue!</button>
                    </div>
                </div>
                <div class="col-xs-4">
                
                </div>
            </div>
            
            <div class="row" style="display:none" id="myDIV">
                    This is my DIV element.
            </div>
        </form>

    </div>

    <div class="col-sm-1">

    </div>
</div>


@endsection