@extends('layouts.my')

@section('content')

        @include('parts.navigation')

        <hr>

@if(Session::has('success'))
    <div class="row">
        <div class="col-sm-1">
    
        </div>
    
        <div class="col-sm-10">
            <div id="charge-message" class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        </div>
    
        <div class="col-sm-1">
    
        </div>
    </div>
@endif


@foreach ($products->chunk(6) as $productChunk)
    
<div class="row">
    <div class="col-sm-1">

    </div>

    <div class="col-sm-10">
        @foreach ($productChunk as $product)
            <div class="col-sm-2">
                <div class="thumbnail">
                <img src="{{$product->imagePath}}" width="100px" height="150px" alt="..." style="border-radius:5px;">
                <div class="caption">
                    <h3>{{$product->title}}</h3>
                    <p>{{$product->description}}</p>
                    <div><strong>${{$product->price}}</strong></div>
                    <p><a href="{{ route('product.addToCart', ['id' => $product->id]) }}" class="btn btn-primary" role="button">Add To Cart</a></p>
                </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="col-sm-1">

    </div>
</div>

@endforeach


@endsection