@extends('layouts.app')

@section('content')

        <hr>

<div class="row">
    <div class="col-sm-1">

    </div>
    <div class="col-sm-6">
        <h1>Checkout!</h1>
        <h4>Your Total Is: ${{ $totalPrice }}</h4>
        <div id="charge-error" class="alert alert-danger {{ !Session::has('error') ? 'hidden' : '' }}">
            {{ Session::get('error') }}
        </div>
        <form action="{{ route('delivery') }}" method="post" id="checkout-form">
                {{ csrf_field() }}
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="name">Email-Address</label>
                        <input type="text" id="email" name="email" class="form-control" value="{{$email}}" required readonly />
                        <input type="text" id="countNumbers" name="countNumbers" class="form-control" value="{{$countNumbers}}" />  
                        <input type="text" id="getuserId" name="getuserId" class="form-control" value="{{$getuserId}}" /> 
                    </div>
                </div>
                <div class="col-xs-4">
                    
                </div>
                <div class="col-xs-4">
                
                </div>

                @if($countNumbers != 0)
                    @foreach ($customer as $customer)
                        <div class="form-group">
                                <label for="name">First Name</label>
                                <input type="text" id="name" name="name" class="form-control" value="{{ $customer->name }}" required /> 
                        </div>
                        <div class="form-group">
                                <label for="name">Last Name</label>
                                <input type="text" id="lastname" name="lastname" class="form-control" value="{{ $customer->lastname }}" /> 
                        </div>
                        <div class="form-group">
                                <label for="name">Company Name</label>
                                <input type="text" id="companyname" name="companyname" class="form-control" value="{{ $customer->lastname }}" /> 
                        </div>
                    @endforeach
                    @foreach ($getAddress as $getAddress)
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="phone-name">Phone Number</label>
                                <input type="number" id="phonenumber" name="phonenumber" value="{{ $getAddress->phone }}" class="form-control" required /> 
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" id="addressOne" name="addressOne" value="{{ $getAddress->street_no }}" class="form-control" required /><br>
                                <input type="text" id="addressTwo" name="addressTwo" value="{{ $getAddress->street_name }}" class="form-control" required /><br>
                                <label for="city">City</label><br>
                                <input type="text" id="city" name="city" class="form-control" value="{{ $getAddress->city }}" required /><br>
                                <label for="city">Province</label><br>
                                <input type="text" id="province" name="province" class="form-control" value="{{ $getAddress->province }}" /><br>
                                <label for="postcode">Postcode</label><br>
                                <input type="number" id="postcode" name="postcode" value="{{ $getAddress->postal_code }}" class="form-control" required />
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="form-group">
                            <label for="name">First Name</label>
                            <input type="text" id="name" name="name" class="form-control" required /> 
                    </div>
                    <div class="form-group">
                            <label for="name">Last Name</label>
                            <input type="text" id="lastname" name="lastname" class="form-control" /> 
                    </div>
                    <div class="form-group">
                            <label for="name">Company Name</label>
                            <input type="text" id="companyname" name="companyname" class="form-control" /> 
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="phone-name">Phone Number</label>
                            <input type="number" id="phonenumber" name="phonenumber" class="form-control" required /> 
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" id="addressOne" name="addressOne" class="form-control" required /><br>
                            <input type="text" id="addressTwo" name="addressTwo" class="form-control" required /><br>
                            <label for="city">City</label><br>
                            <input type="text" id="city" name="city" class="form-control" required /><br>
                            <label for="city">Province</label><br>
                                <input type="text" id="province" name="province" class="form-control" /><br>
                            <label for="postcode">Postcode</label><br>
                            <input type="number" id="postcode" name="postcode" class="form-control" required />
                        </div>
                    </div>
                @endif
                
                <div class="col-xs-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Next</button>
                    </div>
                </div>

                
            </div>
            </div>
            
            <div class="row" style="display:none" id="myDIV">
                    This is my DIV element.
            </div>






            @if(Session::has('cart'))
            <div class="row">
                <div class="col-sm-1">

                </div>
                <div class="col-sm-6">
                    <ul class="list-group">
                        <?php
                            $sumVat = 0;
                            $sumWeight = 0;
                            $totalQty = 0;
                        ?>
                        @foreach($products as $product)
                            <li class="list-group-item">
                                <?php
                                    $qty = $product['qty'];
                                ?>
                                <span class="badge">{{ $qty }}</span>
                                <strong>{{ $product['item']['name'] }}</strong>
                                <span class="label label-success">{{ $product['price'] }}</span>
                                &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;<a href="{{ url('/') }}/reduce/{{ $product['item']['id'] }}">Reduce by 01</a>
                                &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;<a href="{{ url('/') }}/remove/{{ $product['item']['id'] }}">Reduce All</a>
                                <?php
                                    $vat = $product['item']['vat'];
                                    $price = $product['item']['price'];
                                    $weight = $product['item']['weight'];
                                ?>
                                &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;Vat = {{ $vat }}
                                &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;Weight = {{ $weight }} g
                                &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;With vat = 
                                <?php 
                                $withvatPrice = App\Classes\Calvat::calvatOne($price, $vat, $qty);
                                echo $withvatPrice;
                                ?><br>
                                &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;Without vat = 
                                <?php 
                                $vatPrice = App\Classes\Calvat::calvatTwo($price, $vat, $qty);
                                echo $vatPrice;
                                $sumVat += $vatPrice;
                                $sumWeight += $weight;
                                $totalQty += $qty;
                                ?><br>
                                &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;Delivery Type = 
                                <?php 
                                $delivery = $product['item']['delivery_type'];
                                $deliveryType = App\Classes\CeckDeliveryType::checkDelitype($delivery);
                                echo $deliveryType;
                                ?><br>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-1">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-sm-1">
                
                </div>
                <div class="col-sm-6">
                    <strong>Total : {{$totalPrice}}<input type="text" id="total" name="total" class="form-control" value="{{$totalPrice}}" /> </strong><br>
                    <strong>Vat : {{$sumVat}}<input type="text" id="vatTotal" name="vatTotal" class="form-control" value="{{$sumVat}}" /> </strong><br>
                    <strong>All Weight : {{$sumWeight}}<input type="text" id="sumweight" name="sumweight" class="form-control" value="{{$sumWeight}}" /> </strong><br>
                    <strong>Full Qty : {{$totalQty}}<input type="text" id="totalqty" name="totalqty" class="form-control" value="{{$totalQty}}" /> </strong><br>
                    <strong>
                        Sub Total : 
                        <?php
                            $SubTotal = App\Classes\SubTotal::calsubTotal($totalPrice, $sumVat);
                            echo $SubTotal;
                        ?>
                        <input type="text" id="withvatTotal" name="withvatTotal" class="form-control" value="{{$SubTotal}}" />
                    </strong>
                </div>
                <div class="col-sm-1">
                    
                </div>
            </div>  

            @else
            <div class="row">
                <div class="col-sm-1">
                
                </div>
                <div class="col-sm-6">
                <h2>No Items</h2>
                <a href="{{ url('/') }}/">Back</a>
                </div>
                <div class="col-sm-1">
                    
                </div>
            </div>  
            @endif






        </form>
        {{-- <p>Don't have an account! <a href="{{ route('user.signup') }}">Sign Up!</a></p> --}}
    </div>

    <div class="col-sm-1">

    </div>
</div>

@endsection