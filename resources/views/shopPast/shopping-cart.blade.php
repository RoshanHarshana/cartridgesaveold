@extends('layouts.app')

@section('content')

@if(Session::has('cart'))
<div class="row">
    <div class="col-sm-1">

    </div>
    <div class="col-sm-6">
        <ul class="list-group">
            <?php
                $sumVat = 0;
                $sumWeight = 0;
                $totalQty = 0;
            ?>
            @foreach($products as $product)
                <li class="list-group-item">
                    <?php
                        $qty = $product['qty'];
                    ?>
                    <span class="badge">{{ $qty }}</span>
                    <strong>{{ $product['item']['name'] }}</strong>
                    <span class="label label-success">{{ $product['price'] }}</span>
                    &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;<a href="{{ url('/') }}/reduce/{{ $product['item']['id'] }}">Reduce by 01</a>
                    &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;<a href="{{ url('/') }}/remove/{{ $product['item']['id'] }}">Reduce All</a>
                    <?php
                        $vat = $product['item']['vat'];
                        $price = $product['item']['price'];
                        $weight = $product['item']['weight'];
                    ?>
                    &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;Vat = {{ $vat }}
                    &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;Weight = {{ $weight }} g
                    &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;With vat = 
                    <?php 
                    $withvatPrice = App\Classes\Calvat::calvatOne($price, $vat, $qty);
                    echo $withvatPrice;
                    ?><br>
                    &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;Without vat = 
                    <?php 
                    $vatPrice = App\Classes\Calvat::calvatTwo($price, $vat, $qty);
                    echo $vatPrice;
                    $sumVat += $vatPrice;
                    $sumWeight += $weight;
                    $totalQty += $qty;
                    ?><br>
                    &nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;Delivery Type = 
                    <?php 
                    $delivery = $product['item']['delivery_type'];
                    $deliveryType = App\Classes\CeckDeliveryType::checkDelitype($delivery);
                    echo $deliveryType;
                    ?><br>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="col-sm-1">
        
    </div>
</div>
<div class="row">
    <div class="col-sm-1">
    
    </div>
    <div class="col-sm-6">
        <strong>Total : {{$totalPrice}}</strong><br>
        <strong>Vat : {{$sumVat}}</strong><br>
        <strong>All Weight : {{$sumWeight}}</strong><br>
        <strong>
            Sub Total : 
            <?php
                $SubTotal = App\Classes\SubTotal::calsubTotal($totalPrice, $sumVat);
                echo $SubTotal;
            ?>
        </strong>
    </div>
    <div class="col-sm-1">
        
    </div>
</div>  
<hr>
<div class="row">
    <div class="col-sm-1">
    
    </div>
    <div class="col-sm-6">
        <a href="{{ url('/') }}/checkout" type="button" class="btn btn-success">Checkout</a>
    </div>
    <div class="col-sm-1">
        
    </div>
</div>  
@else
<div class="row">
    <div class="col-sm-1">
    
    </div>
    <div class="col-sm-6">
       <h2>No Items</h2>
       <a href="{{ url('/') }}/">Back</a>
    </div>
    <div class="col-sm-1">
        
    </div>
</div>  
@endif


@endsection