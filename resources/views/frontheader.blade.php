<!DOCTYPE HTML>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="@yield('description')"/>
		<meta charset="UTF-8">
		{{-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> --}}
		
		<title id="titleId">Cheap Printer Ink Cartridges, Laser Toner &amp; Inkjet Printer Cartridges</title>
		<link href="{{ asset('img/cartridge_zone_small.png') }}" rel="shortcut icon">
		<!-- Font Awesome CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/font-awesome.min.css') }}" media="all" />
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/bootstrap.min.css') }}" media="all" />
		<!-- Fanacy box -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/jquery.fancybox.min.css') }}" media="all" />
		<!-- Main style CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/style.css') }}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/style-v2.css') }}" media="all" />
		<!-- Responsive CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontassets/css/responsive.css') }}" media="all" />
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
		@media print {
			#myPrntbtn {
				display :  none;
			}
		}
		</style>
	</head>
	<body class="<?php if($home) echo 'default-body'; ?>">
		<!-- Page loader -->
		<div id="preloader"></div>
		<!-- header section start -->
		<header class="header">
			<div class="container submenus">
				<div class="row flexbox-center">
					<div class="col-lg-2 col-md-12 col-12">
						
						<div class="header-info logo-drop d-none d-md-block">
							<a href="{{ url('/') }}"><img class="logo-image" src="{{ asset('frontassets/img/cartridge_zone_logo.svg') }}" alt="logo" /></a>
						</div>
						<div class="header-info logo-drop d-block d-md-none">
							<a href="{{ url('/') }}"><img class="logo-image" src="<?php if($home): ?>{{ asset('frontassets/img/cartridge_zone_logo.svg') }}<?php else: ?>{{ asset('frontassets/img/cartridge_zone_logo.svg') }}<?php endif; ?>" alt="logo" /></a>
						</div>

						<button class="navbar-toggler mobile-menu-btn d-block d-lg-none" type="button">
							<svg viewBox="0 0 800 600">
                                <path class="path-line" d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200" id="top"></path>
                                <path class="path-line" d="M300,320 L540,320" id="middle"></path>
                                <path class="path-line" d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190" id="bottom" transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path>
                            </svg>
						</button>

						<div class="header-info header-info-nav-bar d-block d-lg-none">
							<div class="cart-btn cart-btn-setup" id="checkSession">
								<a class="nav-link" href="{{ url('/') }}/checkout/cart">
									<span class="icon">{{ Session::has('cart') ? Session::get('cart')->totalQty : '0' }}</span>
								</a>

							</div>
						</div>
						<input type="hidden" id="baseUrl" name="baseUrl" value="{{ url('/') }}">

					</div>
					<div class="col-xl-3 col-md-12">
						<div class="header-info header-info-search-<?php echo $home; ?>">
							<div class="formclass formclass-other">
								<input type="text" placeholder="e.g. DX7450" name="searchvalother" id="searchvalother" required />
								<button type="submit" onclick="presssubmitother();" class="theme-btn" id="searchvalotherbtn"><img src="{{ asset('frontassets/img/arrow_more_button.png') }}" alt="arrow_more_button" /></button>
							</div>
						</div>
					</div>


					<div class="col-lg-7 col-md-7 d-none d-lg-block nav-man-col">
						<div class="header-info header-info-nav-bar">
							<ul>
								<li><a href="callto:94114365179"><i class="fa fa-phone"></i>(+94) 11 436 51 79</a></li>
								<li><a href="#" class="delivery-terms">Free Delivery</a></li>
								<li><a href="{{ url('/') }}/contact">Contact</a></li>
								<br>
								<li><a href="#" data-id="item-1">Ink Cartridges</a></li>
								<li><a href="#" data-id="item-2">Toner Cartridges</a></li>
								<li><a href="#" data-id="item-3">Ribbon Cartridges</a></li>
								<li><a href="#" data-id="item-4">Bulk-Ink</a></li>
								<li><a href="#" data-id="item-5">Powder</a></li>
							</ul>

								<a class="nav-link" href="{{ url('/') }}/checkout/cart">
									<div class="cart-btn cart-btn-setup" id="checkSession">
										<span class="icon">{{ Session::has('cart') ? Session::get('cart')->totalQty : '0' }}</span>
									</div>
								</a>
							
						</div>
					</div>


				</div>
			</div>


			<div class="container">
				<div class="submenus">
					<div class="submenu-list" id="item-1">
						<ul>
							<?php 
								$brands = App\Classes\Brands::getBrands();
							?>
							@foreach ($brands as $brand)
								<li><a href="{{ url('/') }}/ink-cartridges/{{$brand->name}}.html" target="_blank" >Ink {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</div>
					<div class="submenu-list" id="item-2">
						<ul>
							<?php 
								$brands = App\Classes\Brands::getBrands();
							?>
							@foreach ($brands as $brand)
								<li><a href="{{ url('/') }}/toner-cartridges/{{$brand->name}}.html" target="_blank" >Toner {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</div>
					<div class="submenu-list" id="item-3">
						<ul>
							<?php 
								$brands = App\Classes\Brands::getBrands();
							?>
							@foreach ($brands as $brand)
								<li><a href="{{ url('/') }}/ribbon-cartridges/{{$brand->name}}.html" target="_blank" >Ribbon {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</div>
					<div class="submenu-list" id="item-4">
						<ul>
							<?php 
								$brands = App\Classes\Brands::getBrands();
							?>
							@foreach ($brands as $brand)
								<li><a href="{{ url('/') }}/bulk-ink/{{$brand->name}}.html" target="_blank" >Bulk {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</div>
					<div class="submenu-list" id="item-5">
						<ul>
							<?php 
								$brands = App\Classes\Brands::getBrands();
							?>
							@foreach ($brands as $brand)
								<li><a href="{{ url('/') }}/powder/{{$brand->name}}.html" target="_blank" >Powder {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>


			<nav class="navbar navbar-expand-lg d-block d-lg-none mobile-menu">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item active">
						<a class="nav-link" href="#">Ink Cartridges </a>
						<ul class="drop">
							<?php 
								$brands = App\Classes\Brands::getBrands();
							?>
							@foreach ($brands as $brand)
								<li  class="nav-item"><a class="nav-link" href="{{ url('/') }}/ink-cartridges/{{$brand->name}}.html" target="_blank" >Ink {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Toner Cartridges</a>
						<ul class="drop">
							<?php 
								$brands = App\Classes\Brands::getBrands();
							?>
							@foreach ($brands as $brand)
								<li  class="nav-item"><a class="nav-link" href="{{ url('/') }}/toner-cartridges/{{$brand->name}}.html" target="_blank" >Toner {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Ribbon Cartridges</a>
						<ul class="drop">
							<?php 

								$brands = App\Classes\Brands::getBrands();
								
							?>
							@foreach ($brands as $brand)
								<li  class="nav-item"><a class="nav-link" href="{{ url('/') }}/ribbon-cartridges/{{$brand->name}}.html" target="_blank" >Ribbon {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">Bulk-Ink</a>
						<ul class="drop">
							<?php 

								$brands = App\Classes\Brands::getBrands();
								
							?>
							@foreach ($brands as $brand)
								<li  class="nav-item"><a class="nav-link" href="{{ url('/') }}/bulk-ink/{{$brand->name}}.html" target="_blank" >Bulk {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">Powder</a>
						<ul class="drop">
							<?php 

								$brands = App\Classes\Brands::getBrands();
								
							?>
							@foreach ($brands as $brand)
								<li  class="nav-item"><a class="nav-link" href="{{ url('/') }}/powder/{{$brand->name}}.html" target="_blank" >Powder {{$brand->name}}</a></li>
							@endforeach
						</ul>
					</li>
				</ul>
			</nav>
		</header>
		<!-- header section end -->