<?php $home = 'home'; ?>
@include('frontheaderTwo')
        <!-- form area start -->
        <section class="form-area d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-content home-search-form">
                            <h2>Find the Right <a href="#">Cartridge</a> <span>for your Printer</span></h2>
                                <form class="formclass">
                                    <div class="filter-selection">
                                        <div class="icon-wrap cartridge"></div>
                                        <select name="section" id="section" class="search-select">
                                            <option value="all">All</option>
                                            <option value="cartridge">Cartridge</option>
                                            <option value="printer">Printer</option>
                                        </select>
                                    </div>
                                    <input type="text" placeholder="e.g. DX7450" name="searchval" id="searchval" required />
                                    <button type="button" onclick="presssubmit();" class="theme-btn" id="searchvalbtn"><img src="{{ asset('frontassets/img/arrow_more_button.png') }}" alt="arrow_more_button" /></button>
                                </form>
                            <p>Search by Printer Name or Cartridge Code</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- form area end -->
@include('frontfooterone')















































