@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Users List</h3>
            </div>

            @if (session('alert'))
                <div class="alert alert-success">
                    {{ session('alert') }}
                </div>
            @endif

            @if (session('alert2'))
                <div class="alert alert-danger">
                    {{ session('alert') }}
                </div>
            @endif

            <?php
              $sideRole = auth()->user()->role_id;
              $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
            ?>

          <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>E-mail</th>
                <th>Role</th>
                <th>Edit</th>
                <th>Delete</th>                
              </tr>
              </thead>
              <tbody>
                  <?php $i = 0; ?>
              @foreach($user as $user)
              @if($user->id != 1)
              <?php $i++ ?>
              <tr>
                <td>{{ $i }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->role->name }}</td>
                <td>
                  @foreach ($sidePermissions as $sidePermission)
                      @if ($sidePermission->name == 'User')
                        @if ($sidePermission->edit == 1)
                          <a href="{{ url('/') }}/user/{{ $user->id }}/edit"><i class="fa fa-edit" style="font-size:25px;  question:green"></i></a>
                        @else
                          <p style="color:red">No Access!!!</p>
                        @endif
                      @endif
                  @endforeach
                </td>
                <td>
                  @foreach ($sidePermissions as $sidePermission)
                      @if ($sidePermission->name == 'User')
                        @if ($sidePermission->delete == 1)
                          <a href="#" onclick="
                          var result = confirm('Are you Delete this one!');
                            if( result )
                            {
                                event.preventDefault();
                                document.getElementById('delete-form{{ $user->id }}').submit();
                            }
                    "><i class="fa fa-trash-o" style="font-size:25px;color:red"></i></a>
                    
                            <form id="delete-form{{ $user->id }}" action="{{ route('user.destroy',[$user->id]) }}"
                                    method="POST" style="display: non;">
                                    <input type="hidden" name="_method" value="delete">
                                    {{ csrf_field() }}
                            </form>
                        @else
                          <p style="color:red">No Access!!!</p>
                        @endif
                      @endif
                  @endforeach
                </td>
                        
              </tr>
              @endif
              @endforeach
              
              </tfoot>
            </table>
                 
            
            <div class="form-group col-md-12">
                <div class="form-group col-md-2">

                  @foreach ($sidePermissions as $sidePermission)
                      @if ($sidePermission->name == 'User')
                        @if ($sidePermission->edit == 1)

                        <p style="font-size: 16px; color: red; border: 1px solid green; border-radius: 3px; padding:5px"><b><a href="{{ url('/') }}/user/change/privileges/1"> <i class="fa fa-pencil" style="font-size:25px;  question:blue"></i></a> Change Admin Privileges</b></p>

                        <p style="font-size: 16px; color: red; border: 1px solid green; border-radius: 3px; padding:5px"><b><a href="{{ url('/') }}/user/change/privileges/2"><i class="fa fa-pencil" style="font-size:25px;  question:blue"></i></a> Change User Privileges</b></p>
                        
                        @endif
                      @endif
                  @endforeach

                </div>
                <div class="form-group col-md-10">
                  
                </div>
            </div>


          </div>


        </div>
        <!-- /.box -->


      </div>
    </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

@endsection