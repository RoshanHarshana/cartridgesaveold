@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <small>Privileges</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">View Privileges</h3>
            </div>

            @if (session('alert'))
                <div class="alert alert-success">
                    {{ session('alert') }}
                </div>
            @endif

            @if (session('alert2'))
                <div class="alert alert-danger">
                    {{ session('alert') }}
                </div>
            @endif

            <form method="POST" action="{{action('UsersController@updateprivileges')}}">
              {{ csrf_field() }}

            <input type="hidden" name="userRole" id="userRole" value="{{ $userRole }}">
            <label for="brandsimage" style="padding-left:10px; font-size: 16px; color: red;">
              @if($userRole == 1)
              Admin
              @elseif($userRole == 2)
              User
              @endif
            </label>
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>  
                  <th>View</th>   
                  <th>Edit</th> 
                  <th>Create</th> 
                  <th>Delete</th>      
                </tr>
                </thead>
                <tbody>
                    <?php $i = -2; ?>
                @foreach($priview as $priview)
                    <?php $i++ ?>
                  @if ($priview->name != 'Color' && $priview->name != 'Category')
                    <tr>
                      <td>{{ $i }}</td>
                        <td>
                          {{ $priview->name }}
                        </td> 
                        <td>
                        <?php
                        if( $priview->index == 1 )
                        {
                          ?>
                          <input type="checkbox" name="{{ $priview->name }}view" value="1" checked/>
                        <?php
                        }
                        else
                        {
                        ?>
                          <input type="checkbox" name="{{ $priview->name }}view" value="1" />
                        <?php
                        }
                        ?>
                        </td>

                        <td>
                        <?php
                        if( $priview->edit == 1 )
                        {
                          ?>
                          <input type="checkbox" name="{{ $priview->name }}edit" value="1" checked/>
                        <?php
                        }
                        else
                        {
                        ?>
                          <input type="checkbox" name="{{ $priview->name }}edit" value="1" />
                        <?php
                        }
                        ?>
                        </td>

                        <td>
                        <?php
                        if( $priview->create == 1 )
                        {
                          ?>
                          <input type="checkbox" name="{{ $priview->name }}create" value="1" checked/>
                        <?php
                        }
                        else
                        {
                        ?>
                          <input type="checkbox" name="{{ $priview->name }}create" value="1" />
                        <?php
                        }
                        ?>
                        </td>

                        <td>
                        <?php
                        if( $priview->delete == 1 )
                        {
                          ?>
                          <input type="checkbox" name="{{ $priview->name }}delete" value="1" checked/>
                        <?php
                        }
                        else
                        {
                        ?>
                          <input type="checkbox" name="{{ $priview->name }}delete" value="1" />
                        <?php
                        }
                        ?>
                        </td>
                      
                      
                    </tr>
                  @endif
                @endforeach
                
                </tfoot>
              </table>
                          
            </div>

            <div class="box-footer">
              <button id='submit' type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>


          </div>
          <!-- /.box -->


      </div>
    </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

@endsection