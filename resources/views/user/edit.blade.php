@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit
        <small>User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit User</h3>
                </div>
                
                @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif
                @if (session('alert2'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif

                {{ Form::model($user, array('route' => array('user.update', $user->id), 'method' => 'PUT', 'files' => true)) }}

                  <div class="box-body">
                    <div class="form-group col-md-8">
                      <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus placeholder="Name">

                              @if ($errors->has('name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                              @endif
                  </div>
                  <div class="form-group col-md-8">

                      <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required placeholder="Email">

                              @if ($errors->has('email'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                  </div>
                  <div class="form-group col-md-8">
                      <select class="form-control" id="role" name="role" required>
                          <option value="{{ $user->role_id }}">{{ $user->role->name }}</option>
                          <option value="1">Admin</option>
                          <option value="2">User</option>
                        </select>

                              @if ($errors->has('role'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('role') }}</strong>
                                  </span>
                              @endif
                  </div>
                  </div>

                  <div class="box-footer">
                    <div class="col-xs-6">
                            
                    </div>
                    <div class="col-xs-2">
                      <button id='submit' type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                    
                    </div>
                    <!-- /.col -->
                  </div>
                  {{ Form::close() }}
              </div>


          </div>
          
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

 


@endsection
