@extends('layouts.asset')
@section('content')

    <div class="register-box">
      
      <!-- /.login-logo -->
      <div class="register-box-body">
          <div class="login-logo">
              <a href="#"><img src="{{ asset('img/acmesoft_logo.png') }}" /></a>
            </div>
            <p class="login-box-msg">Register a new membership</p>

                    <form method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group has-feedback">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Name">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">

                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">

                        <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">

                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Retype password">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <select class="form-control" id="role" name="role" required>
                            <option>--please select--</option>
                            <option value="1">Admin</option>
                            <option value="2">User</option>
                          </select>

                                @if ($errors->has('role'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
                    </div>

                    <div class="row">
                        <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                            <input type="checkbox"> I agree to the <a href="#">terms</a>
                            </label>
                        </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    </form>
    
    
                    <div class="social-auth-links text-center">

                    </div>

                    <a href="{{ route('logout') }}" class="text-center">I already have a membership</a>
    
      </div>
      <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}" ></script>
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}" ></script>
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}" ></script>

    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' /* optional */
        });
      });
    </script>


@endsection