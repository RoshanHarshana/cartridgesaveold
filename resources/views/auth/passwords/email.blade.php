@extends('layouts.asset')

@section('content')



<div class="login-box">
      
    <!-- /.login-logo -->
    <div class="login-box-body">
        <div class="login-logo">
            <a href="#"><img src="{{ asset('img/acmesoft_logo.png') }}" /></a>
          </div>
      <p class="login-box-msg">Reset Password</p>

        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
  
      <form action="{{ route('password.email') }}" method="POST">
        @csrf

        <div class="form-group has-feedback">

                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
        </div>

        <div class="row">
                <div class="col-xs-8">
                  <div class="checkbox icheck">
    
    
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                <!-- /.col -->
              </div>

      </form>

  
    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

@endsection
