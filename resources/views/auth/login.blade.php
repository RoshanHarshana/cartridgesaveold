@extends('layouts.asset')
@section('content')

    <div class="login-box">
      
      <!-- /.login-logo -->
      <div class="login-box-body">
          <div class="login-logo">
              <a href="#"><img src="{{ asset('img/logo.png') }}" /></a>
            </div>
        <p class="login-box-msg">Sign in to start your session</p>
    
        <form action="{{ route('login') }}" method="POST">
          @csrf

          <div class="form-group has-feedback">

                              <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">

                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif

            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              {{-- <a href="{{ route('register') }}" class="text-center">Register a new membership</a> --}}
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">
                  {{ __('Login') }}
              </button>
            </div>
            <!-- /.col -->
          </div>
        </form>
    
      </div>
      <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->



    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' /* optional */
        });
      });
    </script>


@endsection