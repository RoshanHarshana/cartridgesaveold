@extends('layouts.asset')
@section('content')

<div class="wrapper">

    @include('header')

    @include('sidemenu')
  
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit
        <small>Printers</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Printers</h3>
                </div>
                
                @if (session('alert'))
                    <div class="alert alert-success">
                        {{ session('alert') }}
                    </div>
                @endif
                @if (session('alert2'))
                    <div class="alert alert-danger">
                        {{ session('alert2') }}
                    </div>
                @endif

                
                {{ Form::model($printer, array('route' => array('printer.update', $printer->id), 'method' => 'PUT', 'files' => true)) }}

                
                  <div class="box-body">
                  <div class="form-group col-md-12">
                      <label for="printername" control-label>Printer Name</label>
                      <input type="text" name='printername' class="form-control" id="printername" required value="{{ $printer->name }}" placeholder="Enter printer name!">
                    </div>
                    
                    <div class="form-group col-md-12">
                      <label for="printercode" control-label>Printer Code</label>
                      <input type="text" name='printercode' class="form-control" id="printercode" placeholder="Enter printer code!" value="{{ $printer->printer_code }}" required />
                    </div>

                    <div class="form-group col-md-12">
                      <label for="brandid">Brand</label>
                      <select class="form-control select2" id="brandid" name="brandid" style="width: 100%;" required>
                        <option value="{{ $printer->brand_id }}">{{ $printer->brand->name }}</option>
                        @foreach ($brands as $brand)
                          <option value="{{$brand->id}}">{{$brand->name}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-12">
                      <label for="printtype">Category</label>
                      <select class="form-control select2" id="printtype" name="printtype" style="width: 100%;" required>
                        <option value="{{ $printer->category_id }}">{{ $printer->category->name }}</option>
                        @foreach ($category as $category)
                          <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                      </select>
                    </div>

                  <div class="form-group col-md-6">
                    <label for="printersimage">File input</label>
                    <input type="file" id="image" value="{{ $printer->image }}" name="image" />
  
                    <p class="help-block">Upload printer image!</p>
                  </div>

                  <div class="form-group col-md-6">
                    <img src="{{ url('/') }}/image/{{ $printer->image }}" width="150px" height="150px" />
                  </div>

                  <div class="form-group col-md-12">
                    <label for="description" control-label>Description</label>
                    <textarea class="form-control" rows="5" name="description" id="description" placeholder="Enter feacher..." required>{{ $printer->description }}</textarea>
                  </div>

                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  {{ Form::close() }}

              </div>


          </div>
          <!--<div class="col-md-6">
              <div class="box box-primary">
              </div>
          </div>-->
    </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('footer')
  
    @include('rightside')
    
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

@endsection