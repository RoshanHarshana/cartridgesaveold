<!-- search name area start -->
@if ($resultPrintersCountNew > 1)
<section class="search-name">
    <div class="container">
        <div class="row">
            <div class="col-xl-2 col-lg-3 col-md-3 text-center text-md-left">
                <div class="search-item">
                    {{-- <img src="{{ url('/') }}/image/{{ $resultPrinters[0]->image }}" alt="search" /> --}}
                </div>
            </div>
            <div class="col-xl-8 col-lg-9 col-md-9 text-center text-md-left text-lg-center">
                <div class="display-table">
                    <div class="display-tablecell">
                        <div class="search-item">
                            <h2 class="note" style="color:red"><img src="{{ url('/') }}/frontassets/img/warning_sign.png" alt="" /> PLEASE NOTE:</h2>
                            <p class="note-para"><strong>You are currently viewing cartridges for {{$resultPrinters[0]->brand->name}} {{$resultPrinters[0]->category->name}} {{$resultPrinters[0]->printer_code}} printer. (which both printers use different cartridges).</strong></p>
                            
                            <?php
                                for ($i=0; $i < $dependCount; $i++) { 
                            ?>
                                <a style="color:#007bff" href="{{ url('/') }}/{{$resultPrintersThree[$i]->category->name}}/{{$resultPrintersThree[$i]->brand->name}}/{{$resultPrintersThree[$i]->printer_code}}/{{$resultPrintersThree[$i]->id}}.html">View Cartridges for {{$resultPrintersThree[$i]->brand->name}} {{$resultPrintersThree[$i]->category->name}} {{$resultPrintersThree[$i]->name}} </a><br>
                            <?php
                                }
                            ?>
                            
                            <!-- @foreach ($resultPrintersThree as $resultPrintersThree)
                                @if ($resultPrinters[0]->name != $resultPrintersThree->name)
                                    <a style="color:#007bff" href="{{ url('/') }}/{{$resultPrintersThree->category->name}}/{{$resultPrintersThree->brand->name}}/{{$resultPrintersThree->printer_code}}/{{$resultPrintersThree->id}}.html">View Cartridges for {{$resultPrintersThree->brand->name}} {{$resultPrintersThree->category->name}} {{$resultPrintersThree->name}} </a><br>
                                @endif
                            @endforeach -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <span></span>
            </div>
        </div>
    </div>
</section>
@endif



<section class="search-name">
    <div class="container">
        <div class="row">
            <div class="col-xl-2 col-lg-3 col-md-3 text-center text-md-left">
                <div class="search-item">
                    <img src="{{ url('/') }}/image/{{ $resultPrinters[0]->image }}" alt="search" />
                </div>
            </div>
            <div class="col-xl-8 col-lg-9 col-md-9 text-center text-md-left text-lg-center">
                <div class="display-table">
                    <div class="display-tablecell">
                        <div class="search-item">
                            <h2>Cartridges for {{$resultPrinters[0]->name}} printer</h2>
                            @if ($resultProductsCount == 0)
                                <p>No results...</p>
                            @else
                                <p>The following products are guaranteed to work in your {{$resultPrinters[0]->name}} printer</p>
                                <input type="hidden" id="urlprinterbrand" name="urlprinterbrand" value="{{$resultPrinters[0]->brand->name}}">
                                <input type="hidden" id="urlprintername" name="urlprintername" value="{{$resultPrinters[0]->name}}">
                                <input type="hidden" id="urlprintercode" name="urlprintercode" value="{{$resultPrinters[0]->printer_code}}">
                                <input type="hidden" id="urlprintercategory" name="urlprintercategory" value="{{$resultPrinters[0]->category->name}}">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <span></span>
            </div>
        </div>
    </div>
</section>
<!-- search name area end -->