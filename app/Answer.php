<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;

class Answer extends Model
{
    protected $fillable = [
        'question_id',
        'answer',
        'date',
    ];

    // relations
    public function answer()
    {
        return $this->belongsTo('App\answer', 'answer_id');
    }
    //End relations

    /**
     * Store a newly created answer in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $answer
     */
    public static function register($request,$beautymail){

        $ans = $request->input('answer');
        $date = date("Y-m-d");
        if($ans == '')
        {
            return redirect()->back()->with('alert2', 'Enter answer!');
        }
        else
        {
            $getResut = Question::find($request->input('questionid'));
            if($getResut)
            {
                $email = $getResut->email;
                $customer = $getResut->customer_name;
                if($email)
                {

                    $beautymail->send('email.answer',['getResut' => $getResut, 'answer' => $ans], function ($m) use ($getResut){
                        $m->from('hello@example.com', 'Add a new answer for your question!');
                        $m->to($getResut->email, $getResut->customer)->subject('Answer Reminder!');
                    });

                }
            }

            $answer = Answer::create([
                'answer' => $ans,
                'question_id' => $request->input('questionid'),
                'date' => $date
            ]);

            if($answer)
            {
                $question = Question::all();
                return view('answer.index',['question'=> $question]);
            }
            else
            {
                return redirect()->back()->with('alert2', 'Not Success!');
            }
        }

    }

    /**
     * Update the specified answer in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response 
     */
    public static function updaterecord($request, $id){

        $post = Answer::find($id);
        $post->answer = $request->input('answer');
        $post->save();

        $question = Question::all();
        return redirect('answer')->with('alert', 'Changed!');
    }

    /**
     * Update the specified answer's approve in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response 
     */
    public static function updateapprove($question, $approve){

        if($approve == 1)
        {
            $newApprove = 0;
        }
        else
        {
            $newApprove = 1;
        }
        $post = Question::find($question);
        $post->approve = $newApprove;
        $post->save();

        return redirect('answer')->with('alert', 'Changed!');
    }

    /**
     * Remove the specified answer in storage.
     *
     * @param  \Illuminate\Http\Request  $answer
     * @return \Illuminate\Http\Response $answerNew
     */
    public static function deleterecord($answer){

        $answer = Answer::find($answer->id);
        $answerNew = $answer->delete();

        return $answerNew;

    }
}
