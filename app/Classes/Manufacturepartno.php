<?php

namespace App\Classes;

use App\Attribute;
use App\Product;

class Manufacturepartno
{

    /**
     * get manufacture part no...
     *
     * 
     */
    public static function getmanufacturePart($proId)
    {
        $idProd = $proId;
        $attributes = Attribute::where('product_id', $idProd)->get();

        foreach ($attributes as $attribute)
        {
            if($attribute->name == 'manufacture_part_no')
            {
                $manufacture = $attribute->value;
                return $manufacture;
            }
        }
        
    }
}