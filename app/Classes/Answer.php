<?php

namespace App\Classes;

use App\Question;


class Answer
{

    /**
     * get questions and answers...
     *
     * 
     */
    public static function getAnswers($productAnsid)
    {
        $idProd = $productAnsid;
        $question = Question::where('product_id', $idProd)->where('approve', '=',  1)->get();

        return $question;
        
    }
    
}