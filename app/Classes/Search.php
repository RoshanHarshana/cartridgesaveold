<?php

namespace App\Classes;

use App\Product;
use App\Printer;


class Search
{

    /**
     * Search details
     *
     * 
     */
    public static function SearchbyName($searchid)
    {
        $resultMess = explode(' ', $searchid);
        $countresultMess = count($resultMess);

        if($countresultMess > 1)
        {
            $first_word = $resultMess[0];
            $second_word = $resultMess[1];

            $resultPrinters = Printer::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $first_word . '%')->paginate(20)->onEachSide(1);
            $resultPrintersNew = Printer::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $first_word . '%')->get();
            $resultProducts = Product::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0)->get();
            $articlesLink = $resultPrinters->render();
            // $resultProducts = Product::whereHas('printers' , function ($query) use ($searchid, $second_word)
            // {
            //     $query->where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $second_word . '%')->where('stock' , '!=',  0);
            // })->get();

            // $resultPrintersCount = count($resultPrinters);
            $resultPrintersCount = count($resultPrintersNew);
            $resultProductsCount = count($resultProducts);

            return view('search.index',compact('resultPrinters','resultProducts','searchid','resultPrintersCount','resultProductsCount','articlesLink'));
        }
        else
        {

            $resultPrinters = Printer::where('name', 'LIKE', '%' . $searchid . '%')->paginate(20)->onEachSide(1);
            $resultPrintersNew = Printer::where('name', 'LIKE', '%' . $searchid . '%')->get();
            $resultProducts = Product::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0)->get();
            $articlesLink = $resultPrinters->render();
            // $resultProducts = Product::whereHas('printers' , function ($query) use ($searchid)
            // {
            //     $query->where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0);
            // })->get();

            // $resultPrintersCount = count($resultPrinters);
            $resultPrintersCount = count($resultPrintersNew);
            $resultProductsCount = count($resultProducts);

            return view('search.index',compact('resultPrinters','resultProducts','searchid','resultPrintersCount','resultProductsCount','articlesLink'));
        }
    }

    /**
     * Search details
     *
     * 
     */
    public static function SearchinBrandName($brandId, $searchid, $catId)
    {
        $resultPrinters = Printer::where('name', 'LIKE', '%' . $searchid . '%')->where('brand_id', '=', $brandId)->where('category_id', '=', $catId)->paginate(20);
        $resultPrintersNew = Printer::where('name', 'LIKE', '%' . $searchid . '%')->where('brand_id', '=', $brandId)->where('category_id', '=', $catId)->get();
        $resultProducts = Product::where('name', 'LIKE', '%' . $searchid . '%')->where('brand_id', '=', $brandId)->where('category_id', '=', $catId)->where('stock' , '!=',  0)->paginate(20);
        $articlesLink = $resultPrinters->render();

        // $resultPrintersCount = count($resultPrinters);
        $resultPrintersCount = count($resultPrintersNew);
        $resultProductsCount = count($resultProducts);

        return view('search.index',compact('resultPrinters','resultProducts','searchid','resultPrintersCount','resultProductsCount','articlesLink'));
    }

    /**
     * Search details
     *
     * 
     */
    public static function SearchbyNameother($section,$searchid)
    {
        //dd("hii");
        $resultMess = explode(' ', $searchid);
        $countresultMess = count($resultMess);

        if($countresultMess > 1)
        {
            $first_word = $resultMess[0];
            $second_word = $resultMess[1];

            if($section == 'all')
            {
                $resultPrinters = Printer::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $first_word . '%')->paginate(20)->onEachSide(1);
                $resultPrintersNew = Printer::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $first_word . '%')->get();
                $resultProducts = Product::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0)->get();
                $articlesLink = $resultPrinters->render();
                // $resultProducts = Product::whereHas('printers' , function ($query) use ($searchid,$second_word)
                // {
                //     $query->where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $second_word . '%')->where('stock' , '!=',  0);
                // })->get();

                // $resultPrintersCount = count($resultPrinters);
                $resultPrintersCount = count($resultPrintersNew);
                $resultProductsCount = count($resultProducts);

                return view('search.index',compact('resultPrinters','resultProducts','searchid','resultPrintersCount','resultProductsCount','articlesLink'));
            }
            elseif($section == 'cartridge')
            {
                $resultPrinters = Printer::where('name', '=', '')->get();
                $resultProducts = Product::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0)->paginate(20)->onEachSide(1);
                $resultProductsNew = Product::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0)->get();
                $articlesLink = $resultProducts->render();
                // $resultProducts = Product::whereHas('printers' , function ($query) use ($searchid,$second_word)
                // {
                //     $query->where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $second_word . '%')->where('stock' , '!=',  0);
                // })->get();

                $resultPrintersCount = count($resultPrinters);
                // $resultProductsCount = count($resultProducts);
                $resultProductsCount = count($resultProductsNew);

                return view('search.index',compact('resultPrinters','resultProducts','searchid','resultPrintersCount','resultProductsCount','articlesLink'));
            }
            elseif($section == 'printer')
            {
                $resultPrinters = Printer::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $first_word . '%')->paginate(20)->onEachSide(1);
                $resultPrintersNew = Printer::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $first_word . '%')->get();
                $resultProducts = Product::where('name', '=', '')->where('stock' , '!=',  0)->get();
                $articlesLink = $resultPrinters->render();

                // $resultPrintersCount = count($resultPrinters);
                $resultPrintersCount = count($resultPrintersNew);
                $resultProductsCount = count($resultProducts);

                return view('search.index',compact('resultPrinters','resultProducts','searchid','resultPrintersCount','resultProductsCount','articlesLink'));
            } 
        }
        else
        {

            if($section == 'all')
            {
                // dd('hii');
                $resultPrinters = Printer::where('name', 'LIKE', '%' . $searchid . '%')->paginate(20)->onEachSide(1);
                $resultPrintersNew = Printer::where('name', 'LIKE', '%' . $searchid . '%')->get();
                $resultProducts = Product::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0)->get();
                $articlesLink = $resultPrinters->render();
                // $resultProducts = Product::whereHas('printers' , function ($query) use ($searchid)
                // {
                //     $query->where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0);
                // })->get();

                // $resultPrintersCount = count($resultPrinters);
                $resultPrintersCount = count($resultPrintersNew);
                $resultProductsCount = count($resultProducts);

                return view('search.index',compact('resultPrinters','resultProducts','searchid','resultPrintersCount','resultProductsCount','articlesLink'));
            }
            elseif($section == 'cartridge')
            {
                $resultPrinters = Printer::where('name', '=', '')->get();
                $resultProducts = Product::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0)->paginate(20)->onEachSide(1);
                $resultProductsNew = Product::where('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0)->get();
                $articlesLink = $resultProducts->render();
                // $resultProducts = Product::whereHas('printers' , function ($query) use ($searchid)
                // {
                //     $query->where('name', 'LIKE', '%' . $searchid . '%')->orwhere('name', 'LIKE', '%' . $searchid . '%')->orwhere('url', 'LIKE', '%' . $searchid . '%')->where('stock' , '!=',  0);
                // })->get();

                $resultPrintersCount = count($resultPrinters);
                // $resultProductsCount = count($resultProducts);
                $resultProductsCount = count($resultProductsNew);

                return view('search.index',compact('resultPrinters','resultProducts','searchid','resultPrintersCount','resultProductsCount','articlesLink'));
            }
            elseif($section == 'printer')
            {
                $resultPrinters = Printer::where('name', 'LIKE', '%' . $searchid . '%')->paginate(20)->onEachSide(1);
                $resultPrintersNew = Printer::where('name', 'LIKE', '%' . $searchid . '%')->get();
                $resultProducts = Product::where('name', '=', '')->where('stock' , '!=',  0)->get();
                $articlesLink = $resultPrinters->render();

                // $resultPrintersCount = count($resultPrinters);
                $resultPrintersCount = count($resultPrintersNew);
                $resultProductsCount = count($resultProducts);

                return view('search.index',compact('resultPrinters','resultProducts','searchid','resultPrintersCount','resultProductsCount', 'articlesLink'));
            } 
        }

    }

    /**
     * Search all details
     *
     * 
     */
    public static function CartridgeShow($brandname , $printername, $printerId)
    {
        //dd("hii");
        // $resultPrinters = Printer::where('printer_code', '=',  $printername)->get();
        $resultPrinters = Printer::where('id', '=',  $printerId)->get();
        // dd($resultPrinters[0]->category_id);

        $resultPrintersTwo = Printer::where('printer_code', 'LIKE', '%' .$printername. '%')->where('category_id', '=', $resultPrinters[0]->category_id)->get();
        // dd(count($resultPrintersTwo));

        if(count($resultPrintersTwo) == 1)
        {
            $resultPrintersTwo = Printer::where('printer_code', 'LIKE', '%' .$printername. '%')->get();
            $resultPrintersCountNew = count($resultPrintersTwo);
        }
        else
        {
            $resultPrintersTwo = 0;
            $resultPrintersCountNew = 0;
        }
        
        $resultPrintersThree = Printer::where('printer_code', 'LIKE', '%' .$printername. '%')->where('id', '!=', $printerId)->get();
        //dd($resultPrintersThree);

        // $resultProducts = Product::whereHas('printers' , function ($query) use ($printername,$brandname)
        // {
        //     $query->where('name', 'like', '%' .$printername. '%')->orwhere('name', 'like', '%' .$brandname. '%')->where('stock' , '!=',  0);
        // })->get();

        $resultProducts = Product::whereHas('printers' , function ($query) use ($printername,$brandname,$printerId)
        {
            $query->where('printer_code', 'like', '%' .$printername. '%')->where('printers.id', '=', $printerId)->where('stock' , '!=',  0);
        })->get();

        $resultProductsCount = count($resultProducts);
        // $resultPrintersCountNew = count($resultPrintersTwo);
        $dependCount = count($resultPrintersThree);

        return view('search.show',compact('resultProducts','resultProductsCount','resultPrinters','resultPrintersCountNew', 'dependCount', 'resultPrintersThree', 'resultPrintersTwo'));

    }

    /**
     * Search all details for count
     *
     * 
     */
    public static function countCartridgeShow($brandname , $printername)
    {
        $resultProducts = Product::whereHas('printers' , function ($query) use ($printername)
        {
            $query->where('name', 'like', $printername)->where('stock' , '!=',  0);
        })->get();

        $resultProductsCount = count($resultProducts);

        return $resultProductsCount;

    }
    
}