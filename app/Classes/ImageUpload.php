<?php

namespace App\Classes;

use Image;

use Illuminate\Database\Eloquent\Model;

class ImageUpload
{
    public static function uploadImage($image,$width,$height)
    {
        // create file name
        $filename = self::setFileName($image);

        // store the image
        // $location = public_path('image/'.$filename);
        $location = 'image/'.$filename;

        // resizeimage
        Image::make($image)->resize($width, $height)->save($location);

        return $filename;
    }

    public static function setFileName ($image){
    
        return time().'.'.$image->getClientOriginalExtension();
    
    }

    public static function checkSize($image)
    {
        // check image size.
        if($image == '')
        {
            $status = 'not';
        }
        else
        {
            $size = filesize($image);

            $check = ($size/(1024*1024));

            if($check > 2)
            {
                $status = 'not';
            }
            else
            {
                $status = 'yes';
            }
        }

        return $status;
    }

    public static function checkType($image)
    {
        // check image type.
        if($image == '')
        {
            $status = 'not';
        }
        else
        {
            $imageInfo = getimagesize($image);
            $type = $imageInfo['mime'];

            if($type == 'image/png' || $type == 'image/jpg' || $type == 'image/jpeg' || $type == 'image/PNG' || $type == 'image/JPG' || $type == 'image/JPEG')
            {
                $status = 'yes';
            }
            else
            {
                $status = 'not';
            }
        }

        return $status;
    }
}






// class ImageUpload  Unsupported image type
// {
//     public static function uploadImage($image,$type,$width=null,$height = null)
//     {

//         $filename = self::setFileName($image);
//         $location = public_path('image/'.$filename);
//         $resizeImage = self::resize($filename,'thumb');
//         Image::make($resizeImage)->save($location);
//         return $filename;
        
//     }

//     public static function setFileName ($image){
        
//         return time().'.'.$image->getClientOriginalExtension();

//     }

//     public static function resize($type,$image, $width=null,$height = null){

//         if($type == "thumb"){
//             return self::resizeThumb($image);
//         }
//         if($type == "full"){
//             return self::resizeFull($image);
//         }
//         if($type == "custom"){

//             return self::resizeCustom($image,$width,$height);
//         }
//     }

//     public static function resizeThumb ($image){

//         return $image->fit(200);


//     }

//     public static function resizeFull ($image){

//         return $image->resize(null, null);


//     }
//     public static function resizeCustom ($image,$width,$height){

//         return $image->resize($width, $height);

//     }

// }





?>