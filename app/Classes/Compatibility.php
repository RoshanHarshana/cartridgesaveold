<?php

namespace App\Classes;

use App\Attribute;
use App\Product;

class Compatibility
{

    /**
     * Gwt product details...
     *
     * 
     */
    public static function getCompatibility($proId)
    {
        $idProd = $proId;
        $attributes = Attribute::where('product_id', $idProd)->get();

        foreach ($attributes as $attribute)
        {
            if($attribute->name == 'related_compatibility')
            {
                $compatiProductId = $attribute->value;
                if ($compatiProductId != '--select--') 
                {
                    $compatiProduct = Product::where('id', $compatiProductId)->get();
                    return $compatiProduct;
                } 
                else 
                {
                    $compatiProduct = 0;
                    return $compatiProduct;
                }
            }
        }
        
    }
    
    /**
     * get product attributes values...
     *
     * 
     */
    public static function getCompatibilitydetails($proId)
    {
        $idProd = $proId;
        $attributes = Attribute::where('product_id', $idProd)->get();

        foreach ($attributes as $attribute)
        {
            if($attribute->name == 'compatibility')
            {
                $compatiName = $attribute->value;
                return $compatiName;
            }
        }
        
    }
    /**
     * get related products...
     *
     * 
     */
    public static function getRelatedproduct($proId)
    {
        $idProd = $proId;
        $attributes = Attribute::where('product_id', '=', $idProd)->get();

        foreach ($attributes as $attribute)
        {
            if($attribute->name == 'related_products')
            {
                $relatedProducts = $attribute->value;
                return $relatedProducts;
            }
        }
        //return $relatedProducts;
        
    }

    /**
     * Gwt product details...
     *
     * 
     */
    public static function getProductset($proId)
    {
        $idProd = $proId;
        $attributes = Attribute::where('product_id', $idProd)->get();

        foreach ($attributes as $attribute)
        {
            if($attribute->name == 'related_compatibility')
            {
                $compatiProductId = $attribute->value;
                if ($compatiProductId != '--select--') 
                {
                    $compatiProduct = $compatiProductId;
                    return $compatiProduct;
                } 
                else 
                {
                    $compatiProduct = '';
                    return $compatiProduct;
                }
            }
        }
        
    }

    /**
     * Gwt product details...
     *
     * 
     */
    public static function getProduct($compatibleProducts)
    {
        $compatiProduct = Product::where('id', $compatibleProducts)->get();
        return $compatiProduct;
        
    }
}