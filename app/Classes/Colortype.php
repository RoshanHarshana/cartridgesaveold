<?php

namespace App\Classes;

use App\Attribute;
use App\Product;

class Colortype
{

    /**
     * get color type...
     *
     * 
     */
    public static function getColortype($proId)
    {
        $idProd = $proId;
        $attributes = Attribute::where('product_id', $idProd)->get();

        foreach ($attributes as $attribute)
        {
            if($attribute->name == 'color_type')
            {
                $color = $attribute->value;
                return $color;
            }
        }
        
    }
}