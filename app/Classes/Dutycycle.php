<?php

namespace App\Classes;

use App\Attribute;
use App\Product;

class Dutycycle
{

    /**
     * get duty cycle...
     *
     * 
     */
    public static function getDutycycle($proId)
    {
        $idProd = $proId;
        $attributes = Attribute::where('product_id', $idProd)->get();

        foreach ($attributes as $attribute)
        {
            if($attribute->name == 'duty_cycle')
            {
                $duty = $attribute->value;
                return $duty;
            }
        }
        
    }
}