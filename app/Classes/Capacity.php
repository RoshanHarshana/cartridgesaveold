<?php

namespace App\Classes;

use App\Attribute;
use App\Product;

class Capacity
{

    /**
     * get duty cycle...
     *
     * 
     */
    public static function getCapacity($proId)
    {
        $idProd = $proId;
        $attributes = Attribute::where('product_id', $idProd)->get();

        foreach ($attributes as $attribute)
        {
            if($attribute->name == 'duty_cycle')
            {
                $duty = $attribute->value;
                return $duty;
            }
        }
        
    }
}