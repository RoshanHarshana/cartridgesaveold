<?php

namespace App\Classes;

class Datediff
{

    /**
     * get questions and answers...
     *
     * 
     */
    public static function calDatediff($oldDate)
    {
        $dateold=date_create($oldDate);
        $datenow=date_create(date("Y-m-d"));

        if ($datenow>$dateold) {
            $diff=date_diff($dateold,$datenow);
            $echoDate = $diff->format("Days Ago");
        } else {
            $echoDate = "1 Days Ago";
        }
        
        //echo $diff->format("Days Ago");

        return $echoDate;
        
    }
    
}