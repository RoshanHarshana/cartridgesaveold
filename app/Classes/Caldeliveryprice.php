<?php

namespace App\Classes;

use App\Diliveryrate;
use App\Product;
use Session;


class Caldeliveryprice
{

    /**
     * cal delivery charge...
     *
     * 
     */
    public static function calDeliveryprice($city, $sumweight)
    {
        $getCharge = Diliveryrate::where('id', 'LIKE',  $city)->get();
        $cart = Session::get('cart');
        $products = $cart->items;

        $sumDelivery = 0;

        foreach ($products as $product) {
           $deliveryType = $product['item']['delivery_type'];
           $weight = $product['item']['weight'];
           $qty = $product['qty'];
           $totalWeight = $weight*$qty;

            if ($deliveryType == 0) 
            {
                $calDelAmount = 0;
            } 
            else
            {
                $for_after_tkg = 0;
                
                if($totalWeight >= 1)
                {
                    $for_1s_tkg = (1 * $getCharge[0]->price_first_kg);

                    if(($totalWeight - 1)>0)
                    {
                        $for_after_tkg = (($totalWeight-1) * $getCharge[0]->price_after_first_kg);
                    }
                }
                else
                {
                    $for_1s_tkg = 0;
                    $for_after_tkg = 0;
                }

                $calDelAmount = ($for_1s_tkg + $for_after_tkg);
            } 

            //$sumDelivery += $calDelAmount;
            // else 
            // {
            //     if($totalWeight > 1)
            //     {
            //         $for_1s_tkg = (1 * $getCharge[0]->price_first_kg);

            //         if(($totalWeight - 1)>0)
            //         {
            //             $for_after_tkg = (($totalWeight-1) * $getCharge[0]->price_after_first_kg);
            //         }
            //     }
            //     else
            //     {
            //         $for_1s_tkg = 0;
            //         $for_after_tkg = 0;
            //     }

            //     $calDelAmount = ($for_1s_tkg + $for_after_tkg);
            // }

            $sumDelivery += $calDelAmount;
            
        }

        //echo $sum;
        return $sumDelivery;

    }


    /**
     * get questions and answers...
     *
     * 
     */
    public static function calDeliverypriceToone($city, $sumweight, $productId, $proQty)
    {
        $getCharge = Diliveryrate::where('id', 'LIKE',  $city)->get();
        $product = Product::where('id', '=',  $productId)->get();

        $sumDelivery = 0;

           $deliveryType = $product[0]['delivery_type'];
           $weight = $product[0]['weight'];
           $qty = $proQty;
           $totalWeight = $weight*$qty;

            if ($deliveryType == 0) 
            {
                $calDelAmount = 0;
            } 
            else
            {
                $for_after_tkg = 0;
                
                if($totalWeight >= 1)
                {
                    $for_1s_tkg = (1 * $getCharge[0]->price_first_kg);

                    if(($totalWeight - 1)>0)
                    {
                        $for_after_tkg = (($totalWeight-1) * $getCharge[0]->price_after_first_kg);
                    }
                }
                else
                {
                    $for_1s_tkg = 0;
                    $for_after_tkg = 0;
                }

                $calDelAmount = ($for_1s_tkg + $for_after_tkg);
            } 


            $sumDelivery += $calDelAmount;
            

        //echo $sum;
        return $sumDelivery;

    }
    
}