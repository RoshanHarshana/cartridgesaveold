<?php

namespace App\Classes;

use App\Product;

class FrontProduct
{

    /**
     * Search product details
     *
     * 
     */
    public static function ProductShow($printername)
    {
        //$modifyName = str_replace('-',' ',$printername);

        $resultProducts = Product::where('url', 'LIKE', $printername)->where('stock' , '!=',  0)->get();
        return view('search.showProduct',compact('resultProducts'));
    }
    
}