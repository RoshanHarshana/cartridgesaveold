<?php

namespace App\Classes;

class Calvat
{

    /**
     * Calculate ppvalue...
     *
     * 
     */
    public static function calvatOne($price, $vat, $qty)
    {
        $withvatPrice = (($price * ((100+$vat)/100))*$qty);
        return $withvatPrice;
    }
    public static function calvatTwo($price, $vat, $qty)
    {
        $vatPrice = (($price * ($vat/100)*$qty));
        return $vatPrice;
    }
    public static function withVat($price, $vat)
    {
        $withvatPrice = ($price * ((100+$vat)/100));
        return $withvatPrice;
    }
}