<?php

namespace App\Classes;

use App\Product;

class Productdetails
{

    /**
     * get Productdetails...
     *
     * 
     */
    public static function getProductdetails($productId)
    {
        $resultProducts = Product::where('id', '=', $productId)->get();
        return $resultProducts;
    }

    /**
     * calculate Price.
     *
     * 
     */
    public static function calPrice($qty,$price)
    {
        $total = $qty*$price;
        $totalPrice = number_format($total,2);
        return $totalPrice;
    }
    
}