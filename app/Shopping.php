<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Product;
use App\Classes\Cart;
use App\Classes\Caldeliveryprice;
use App\Order;
use App\Orderproduct;
use App\User;
use App\Address;
use App\Diliveryrate;
use Auth;
use DB;
use Mail;
use App\Http\Requests; 
use Session;
use Stripe\Stripe;
use Stripe\Charge;
use Log;
use App\Payment;
use Snowfire\Beautymail\Beautymail;

class Shopping extends Model
{
    /**
     * Store a newly created users and check mail in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $nameOld
     * @return \Illuminate\Http\Response $post
     */
    public static function checkemailForuser($request){

        $email = $request->input('email');
        $password = rand(1, 1000000);

        $getUsers = User::where('email', 'LIKE',  $email)->get();
        $countNumbers = count($getUsers);
        if ($countNumbers == 0) {
            $md5val = Hash::make($password);

            $user = User::create([
                'name' => "Customer",
                'email' => $request->input('email'),
                'password' => $md5val,
                'role_id' => "4"
            ]);

            $getuserId = $user->id;     
            //dd($getuserId);  
        } else {
            foreach ($getUsers as $getUser) {
                $getuserId = $getUser->id;
            }
        }
        $getAddress = Address::where('user_id', 'LIKE',  $getuserId)->orderBy('created_at', 'desc')->first();
        if ($getAddress) {
            $countAddNumbers = 1;
        }
        else
        {
            $countAddNumbers = 0;
        }
        $cart = Session::get('cart');

        $dilivery = Diliveryrate::orderBy('town', 'ASC')->get();

        $diliveryCharge = 0;
        //dd($countAddNumbers);
        return view('shop.checkout-details', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'email' => $email, 'getAddress' => $getAddress, 'customer' => $getUsers, 'countNumbers' => $countNumbers, 'getuserId' => $getuserId, 'countAddNumbers'=> $countAddNumbers, 'dilivery'=> $dilivery, 'diliveryCharge'=>$diliveryCharge]);
        //dd($oldCart);

    }

    /**
     * Store a newly created order,address and update user in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $nameOld
     * @return \Illuminate\Http\Response $post
     */
    public static function checkDelivery($request){

        $cart = Session::get('cart');

        $email = $request->input('email');
        $countNumbers = $request->input('countNumbers');
        $getuserId = $request->input('getuserId');
        $name = $request->input('name');
        $getAddressId = $request->input('getAddressId');

        //dd($getuserId);
        $lastname = $request->input('lastname');
        $phonenumber = $request->input('phonenumber');
        $addressOne = $request->input('addressOne');
        $addressTwo = $request->input('addressTwo');
        $city = $request->input('city');
        $postcode = $request->input('postcode');
        $companyname = $request->input('companyname');
        $province = $request->input('province');
        $total = $request->input('total');
        $vatTotal = $request->input('vatTotal');
        $sumweight = $request->input('sumweight');
        $withvatTotal = $request->input('withvatTotal');
        $totalqty = $request->input('totalqty');
        $date = date("Y-m-d");

        //cal dilivery charge....
        $calDelivery = Caldeliveryprice::calDeliveryprice($city, $sumweight);
        //dd($calDelivery);
        //end cal dilivery charge....

        $post = User::find($getuserId);
        $post->name = $name;
        $post->lastname = $lastname;
        $post->save();

        //Store order data
            $post = new Order;
            $post->date = $date;
            $post->user_id = $getuserId;
            $post->total = $total;
            $post->total_qty = $totalqty;
            $post->vat = $vatTotal;
            $post->save();

            $orderId = $post->id;

        //store product details
        foreach ($cart->items as $products) {

            $productId = $products['item']['id'];
            $proQty = $products['qty'];

            $calDeliveryCharge = Caldeliveryprice::calDeliverypriceToone($city, $sumweight, $productId, $proQty);

            $post = new Orderproduct;
            $post->product_id = $products['item']['id'];
            $post->order_id = $orderId;
            $post->total = $total;
            $post->product_name = $products['item']['name'];
            $post->delivery_charge = $calDeliveryCharge;
            $post->quantity = $proQty;
            $post->product_price = $products['item']['price'];;
            $post->save();
        }

        //Store address details
        if($getAddressId == 'no')
        {
            $post = new Address;
            $post->street_no = $addressOne;
            $post->street_name = $addressTwo;
            $post->city = $city;
            $post->postal_code = $postcode;
            $post->user_id = $getuserId;
            $post->province = $province;
            $post->company_name = $companyname;
            $post->order_id = $orderId;
            $post->phone = $phonenumber;
            $post->save();

            $addressId = $post->id;
        }
        else
        {
            //$post = Address::find($getAddressId);
            $post = new Address;
            $post->street_no = $addressOne;
            $post->street_name = $addressTwo;
            $post->city = $city;
            $post->postal_code = $postcode;
            $post->user_id = $getuserId;
            $post->province = $province;
            $post->company_name = $companyname;
            $post->order_id = $orderId;
            $post->phone = $phonenumber;
            $post->save();

            $addressId = $post->id;
        }

        $getAddress = Address::where('user_id', 'LIKE',  $getuserId)->get();
        $getUsers = User::where('email', 'LIKE',  $email)->get();
        $getCharge_for_cash_on = Diliveryrate::where('id', 'LIKE',  $city)->get();


        return view('shop.payment',['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'email' => $email, 'getAddress' => $getAddress, 'customer' => $getUsers, 'diliveryCharge'=>$calDelivery, 'getCharge'=>$getCharge_for_cash_on, 'orderId' => $orderId]);

    }


    /**
     * Store a newly created order,address and update user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $post
     */
    public static function forFinished($request, $beautymail){

        $order_id = $request->input('order_id');
        $pastAmu = DB::select('select delivery_total,user_id from orders where id = ?',[$order_id]);

        $userId = $pastAmu[0]->user_id;
        if($userId)
        {
            $getUsers = User::where('id', '=',  $userId)->get();
            $orders = Order::where('id', '=', $order_id)->get();

            $beautymail->send('email.order',['getUsers' => $getUsers, 'userId' => $userId, 'orders' => $orders], function ($m) use ($getUsers){
                $m->from('hello@example.com', 'Order Complete!');
                $m->to($getUsers[0]->email, $getUsers[0]->name)->subject('Order Reminder!');
            });
        }


        $diliveryCharge = $pastAmu[0]->delivery_total;
        $cart = Session::get('cart');

        return view('shop.done',['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'diliveryCharge' => $diliveryCharge]);

    }


     /**
     * distroy session
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $post
     */
    public static function done($request){

        $distroy = $request->session()->flush();

        return redirect(url('/'));
    }

     /**
     * update orders and payments
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $post
     */
    public static function getapidetails($request){

        $date = date('Y-m-d');
        $merchant_id = $request->input('merchant_id');
        $order_id = $request->input('order_id');
        $payhere_amount = $request->input('payhere_amount');
        $payhere_currency = $request->input('payhere_currency');
        $status_code = $request->input('status_code');
        $md5sig = $request->input('md5sig');
        // $payment_id = $request->input('payment_id');
        $payment_id = NULL;

         $merchant_secret = 'hoardings'; // Replace with your Merchant Secret (Can be found on your PayHere account's Settings page)

         $local_md5sig = strtoupper (md5 ( $merchant_id . $order_id . $payhere_amount . $payhere_currency . $status_code . strtoupper(md5($merchant_secret)) ) );

        if($payhere_amount == NULL)
        {
            $payhere_amount = 0;
        }

        if (($local_md5sig === $md5sig) AND ($status_code == 2) ){

            $pastAmu = DB::select('select total,vat from orders where id = ?',[$order_id]);
            $pastamupast = $pastAmu[0]->total;
            $vatpast = $pastAmu[0]->vat;

            $diliveryCharge = ($payhere_amount - ($pastamupast+$vatpast));

            // $insertTable = DB::insert('insert into payments (payment_type, merchant_id, payment_id, payhere_amount, payhere_currency, status_code, md5sig, custom_msg, date, order_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', ['card', $merchant_id, $payment_id, $payhere_amount, $payhere_currency, $status_code, $md5sig, 'Confirmed!', $date, $order_id]);

            $post = new Payment;
            $post->payment_type = 'card';
            $post->merchant_id = $merchant_id;
            $post->payment_id = $payment_id;
            $post->payhere_amount = $payhere_amount;
            $post->payhere_currency = $payhere_currency;
            $post->status_code = $status_code;
            $post->md5sig = $md5sig;
            $post->custom_msg = 'Confirmed!';
            $post->date = $date;
            $post->order_id = $order_id;
            $post->save();
            
            $updateAmu = DB::update('update orders set delivery_total = ?, status = ? where id = ?',[$diliveryCharge,'paid',$order_id]);

        }
        else
        {
            
        }

    }

    /**
     * update orders and payments
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $post
     */
    public static function getcashdetails($request, $beautymail){

        $date = date('Y-m-d');
        $merchant_id = 0;
        $order_id = $request->input('order_id');
        $payhere_amount = $request->input('amount');
        $payhere_currency = 0;
        $status_code = 0;
        $md5sig = 0;
        $payment_id = NULL;

            $pastAmu = DB::select('select total,vat from orders where id = ?',[$order_id]);
            $pastamupast = $pastAmu[0]->total;
            $vatpast = $pastAmu[0]->vat;

            $diliveryCharge = ($payhere_amount - ($pastamupast+$vatpast));

            // $insertTable = DB::insert('insert into payments (payment_type, merchant_id, payment_id, payhere_amount, payhere_currency, status_code, md5sig, custom_msg, date, order_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', ['cash on delivery', $merchant_id, $payment_id, $payhere_amount, 'LKR', $status_code, $md5sig, 'Confirmed!', $date, $order_id]);

            $post = new Payment;
            $post->payment_type = 'cash on delivery';
            $post->merchant_id = $merchant_id;
            $post->payment_id = $payment_id;
            $post->payhere_amount = $payhere_amount;
            $post->payhere_currency = $payhere_currency;
            $post->status_code = $status_code;
            $post->md5sig = $md5sig;
            $post->custom_msg = 'Confirmed!';
            $post->date = $date;
            $post->order_id = $order_id;
            $post->save();
            
            $updateAmu = DB::update('update orders set delivery_total = ?, status = ? where id = ?',[$diliveryCharge,'pending',$order_id]);

            $pastAmu = DB::select('select delivery_total,user_id from orders where id = ?',[$order_id]);

            $userId = $pastAmu[0]->user_id;
            if($userId)
            {
                $getUsers = User::where('id', '=',  $userId)->get();
                $orders = Order::where('id', '=', $order_id)->get();

                $beautymail->send('email.order',['getUsers' => $getUsers, 'userId' => $userId, 'orders' => $orders], function ($m) use ($getUsers){
                    $m->from('hello@example.com', 'Order Complete!');
                    $m->to($getUsers[0]->email, $getUsers[0]->name)->subject('Order Reminder!');
                });
            }

            $diliveryCharge = $pastAmu[0]->delivery_total;
            $cart = Session::get('cart');

            return view('shop.done',['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'diliveryCharge' => $diliveryCharge]);

    }
}
