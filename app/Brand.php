<?php

namespace App;

use App\Classes\ImageUpload;
use Illuminate\Database\Eloquent\Model;
use Storage;
use App\ImageUp;

class Brand extends Model
{
   
    protected $fillable = [
        'name',
        'description',
        'image',
    ];

    // relations

    public function printers()
    {
        return $this->hasMany('App\Printer');
    }
    public function products()
    {
        return $this->hasMany('App\Product');
    }

    // end relations

    /**
     * Store a newly created Brand in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $post
     */
    public static function register($request){

        $image = $request->file('image');

        $imagesize = ImageUpload::checkSize($image);
        $imagetype = ImageUpload::checkType($image);
        //dd($imagetype);

        if($imagesize == 'yes' && $imagetype == 'yes')
        {
            $post = new Brand;
            $post->name = $request->input('brandname');
            $post->description = $request->input('description');

            $width = 250;
            $height = 200;

            if ($request->hasfile('image'))
            {
                //$image = $request->file('image');

                $imageupload = ImageUpload::uploadImage($image,$width,$height);
                $post->image = $imageupload;

            }

            $post->save();

            if ($request->hasfile('image'))
            {
                //$image = $request->file('image');

                $imageupload = ImageUpload::uploadImage($image,$width,$height);

                // get extention
                $ext = pathinfo(storage_path().$imageupload, PATHINFO_EXTENSION);

                $post = new ImageUp;
                $post->name = $imageupload;
                $post->alt = '';
                $post->type = $ext;
                $post->save();
            }

            //return $post;
            if($post)
            {
                return redirect()->back()->with('alert', 'Success!');
            }
            else
            {
                return redirect()->back()->with('alert2', 'Not Success!');
            }

        }
        else
        {
            return redirect()->back()->with('alert2', 'Unsupported image type or size! It must be lower 2Mb and type is jpg, jpeg, or png!');
        }

    }

    /**
     * Update the specified Brand in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response 
     */
    public static function updaterecord($request, $id){

        $image = $request->file('image');

            $post = Brand::find($id);
            $post->name = $request->input('brandname');
            $post->description = $request->input('description');

            $width = 250;
            $height = 200;

            if ($request->hasfile('image'))
            {
                //$image = $request->file('image');

                $imageupload = ImageUpload::uploadImage($image,$width,$height);
                $post->image = $imageupload;

                $oldfilename = $post->image;

                $post->image =  $imageupload;

                Storage::delete($oldfilename);
            }
            $post->save();

            //save image details for image_ups table
            if ($request->hasfile('image'))
            {
                //$image = $request->file('image');

                $imageupload = ImageUpload::uploadImage($image,$width,$height);

                // get extention
                $ext = pathinfo(storage_path().$imageupload, PATHINFO_EXTENSION);

                $post = new ImageUp;
                $post->name = $imageupload;
                $post->alt = '';
                $post->type = $ext;
                $post->save();
            }

            $brandNew = Brand::all();
            return view('brand.index',['brand'=> $brandNew]);

    }

    /**
     * Remove the specified brand in storage.
     *
     * @param  \Illuminate\Http\Request  $brand
     * @return \Illuminate\Http\Response $brandNew
     */
    public static function deleterecord($brand){

        $brand = Brand::find( $brand->id );
        $brandNew = $brand->delete();

        return $brandNew;

    }
    
}
