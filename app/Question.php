<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;
use App\User;

class Question extends Model
{
    protected $fillable = [
        'question',
        'customer_name',
        'email',
        'date',
        'approve',
        'product_id',
    ];

    // relations
    public function Product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
    // End relations

    /**
     * Store a newly created Question in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $question
     */
    public static function register($request){

        $que = $request->input('question');
        $date = date("Y-m-d");
        if($que == '')
        {
            return redirect()->back()->with('alert2', 'Enter Question!');
        }
        else
        {
            $question = Question::create([
                'question' => $que,
                'product_id' => $request->input('productid'),
                'date' => $date
            ]);
            if($question)
            {
                return redirect()->back()->with('alert', 'Success!');
            }
            else
            {
                return redirect()->back()->with('alert2', 'Not Success!');
            }
        }

    }

    /**
     * Update the specified question in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response $post
     */
    public static function updaterecord($request, $id){

        $post = Question::find($id);
        $post->product_id = $request->input('productid');
        $post->question = $request->input('question');
        $post->save();

        return $post;

    }

    /**
     * Remove the specified question in storage.
     *
     * @param  \Illuminate\Http\Request  $question
     * @return \Illuminate\Http\Response $questionNew
     */
    public static function deleterecord($question){

        $question = Question::find($question->id);
        $questionNew = $question->delete();

        return $question;

    }

    /**
     * Remove the specified question in storage.
     *
     * @param  \Illuminate\Http\Request  $question
     * @return \Illuminate\Http\Response $questionNew
     */
    public static function deleterecordNew($question){
        $question = Question::find($question->id);
        $questionNew = $question->delete();

        return $question;

    }

    /**
     * Store a newly created Question in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $question
     */
    public static function registerCustomerque($request,$beautymail){

        //dd('hii');

        $que = $request->input('questions');
        $quename = $request->input('quename');
        $queproid = $request->input('queproid');
        $queemail = $request->input('queemail');

        $calString = strlen($que);
        if($calString < 1000)
        {
            $date = date("Y-m-d");

            $question = Question::create([
                'question' => $que,
                'product_id' => $queproid,
                'date' => $date,
                'customer_name' => $quename,
                'email' => $queemail
            ]);

            $user = User::where('role_id', '=', 1)->get();

            foreach($user as $user)
            {

                $beautymail->send('email.faq',['user' => $user, 'question' => $question], function ($m) use ($user){
                    $m->from('hello@example.com', 'Add New Question!');
                    $m->to($user->email, $user->name)->subject('Question Reminder!');
                });
        
            }

            if($question)
            {
                return redirect()->back()->with('alert', 'Success!');
            }
            else
            {
                return redirect()->back()->with('alert2', 'Not Success!');
            }
        }
        else
        {
            return redirect()->back()->with('alert2', 'Out of bound Question!');
        }
    }
}
