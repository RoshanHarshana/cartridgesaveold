<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderproduct extends Model
{
    protected $fillable = [
        'product_id', 
        'order_id',
        'total',
        'product_name',
        'delivery_charge',
        'quantity',
        'product_price',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
}
