<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'date', 
        'user_id',
        'total',
        'delivery_total',
        'total_qty',
        'vat',
        'payment_type',
        'note',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function orderproducts()
    {
        return $this->hasMany('App\Orderproduct');
    }
    public function addresses()
    {
        return $this->hasMany('App\Address');
    }
    public function payment()
    {
        return $this->hasone('App\Payment');
    }
}
