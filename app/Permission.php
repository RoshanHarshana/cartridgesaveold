<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //
    protected $fillable = [
        'name',
        'role_id',
        'edit',
        'index',
        'create',
        'delete',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }
}
