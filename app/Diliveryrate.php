<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diliveryrate extends Model
{
    public $table = "delivery_rates";
    protected $fillable = [
        'town',
        'price_first_kg',
        'price_after_first_kg',
        'cod_price_first_kg',
        'cod_price_after_first_kg',
        'province',
        'main_city',
        'code',
        'post_code'
    ];

    /**
     * Store a newly created dilivery rate in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $diliveryrate
     */
    public static function register($request){

        $diliveryrate = Diliveryrate::create([
            'town' => $request->input('town'),
            'price_first_kg' => $request->input('priceforfirst'),
            'price_after_first_kg' => $request->input('priceforafter'),
            'cod_price_first_kg' => $request->input('cashonpriceforfirst'),
            'cod_price_after_first_kg' => $request->input('cashonpriceforafter')
        ]);
        if($diliveryrate){
            return redirect()->back()->with('alert', 'Success!');
        }
        else{
            return redirect()->back()->with('alert2', 'Not Success!');
        }
    }

    /**
     * Update the specified color in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response $post
     */
    public static function updaterecord($request, $id){

        $post = Diliveryrate::find($id);
        $post->town = $request->input('town');
        $post->price_first_kg = $request->input('priceforfirst');
        $post->price_after_first_kg = $request->input('priceforafter');
        $post->cod_price_first_kg = $request->input('cashonpriceforfirst');
        $post->cod_price_after_first_kg = $request->input('cashonpriceforafter');
        $post->save();

        return $post;

    }

    /**
     * Remove the specified dilivery rate in storage.
     *
     * @param  \Illuminate\Http\Request  $diliveryrate
     * @return \Illuminate\Http\Response $color
     */
    public static function deleterecord($diliveryrate){

        $diliveryrate = Diliveryrate::find( $diliveryrate->id );
        $diliveryrateNew = $diliveryrate->delete();

        if($diliveryrate) {

            return redirect('diliveryrate')->with('alert', 'Deleted!');
        }
        else
        {
            return redirect('diliveryrate')->with('alert2','Color could not be deleted!');
        }

    }
}

