<?php

namespace App\Http\Controllers;

use App\Attributeset;
use Illuminate\Http\Request;

class AttributesetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attributeset  $attributeset
     * @return \Illuminate\Http\Response
     */
    public function show(Attributeset $attributeset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attributeset  $attributeset
     * @return \Illuminate\Http\Response
     */
    public function edit(Attributeset $attributeset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attributeset  $attributeset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attributeset $attributeset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attributeset  $attributeset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attributeset $attributeset)
    {
        //
    }
}
