<?php

namespace App\Http\Controllers;

use App\Printer;
use App\ImageUp;
use App\Brand;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;
use Image;
use Storage;
use Session;
use Auth;
use Redirect;
use App\Http\Controllers\Controller;

class PrintersController extends Controller
{
    /**
     * Controll user permissions...
     *
     * @return void
     */
    public function __construct()
    {
        if(Auth::user())
        {
            $sideRole = Auth::user()->role_id;
            $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
            foreach ($sidePermissions as $sidePermission) {
                if ($sidePermission->name == 'Printer') {
                    if ($sidePermission->index == 0) {
                        $this->middleware('guest', ['only' => ['index','show']]);
                    } elseif ($sidePermission->edit == 0) {
                        $this->middleware('guest', ['only' => ['edit','update']]);
                    }elseif ($sidePermission->create == 0) {
                        $this->middleware('guest', ['only' => ['create','store']]);
                    }elseif ($sidePermission->delete == 0) {
                        $this->middleware('guest', ['only' => ['destroy']]);
                    }     
                }
            }
        }
        else
        {
            $this->middleware('guest');
        }
    }
    
    /**
     * Display the data table.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $printers = Printer::all();
        return view('printer.index',['printer'=> $printers]);
    }

    /**
     * Show the form for creating a new printer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $brands = Brand::all();
        $category = Category::all();

        return view('printer.create',['brands'=> $brands],['category'=> $category]);
    }

    /**
     * Store a newly created printer in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response  $printer
     */
    public function store(Request $request)
    {

        $printer = Printer::register($request);

        return $printer;
    }

    /**
     *
     * @param  \App\Printer  $printer
     * @return \Illuminate\Http\Response
     */
    public function show(Printer $printer)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Printer  $printer,$brands
     * @return \Illuminate\Http\Response
     */
    public function edit(Printer $printer)
    {
        $brands = DB::table('brands')->get();
        $category = DB::table('categories')->get();
        $printer = Printer::find( $printer->id );
        return view('printer.edit',['printer'=>$printer])->with('brands',$brands)->with('category',$category);
    }

    /**
     * Update the specified printer in storage.
     *
     * @param  \Illuminate\Http\Request  $request,$image,$filename,$location
     * @param  \App\Printer  $printer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $printer = Printer::updaterecord($request, $id);

        return $printer;

    }

    /**
     * Remove the specified printer from storage.
     *
     * @param  \App\Printer  $printer,$findPrinter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Printer $printer)
    {
        $findPrinter = Printer::deleterecord($printer);
        return $findPrinter;
    }
}
