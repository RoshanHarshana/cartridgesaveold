<?php

namespace App\Http\Controllers;

use App\Brand;
use App\ImageUp;
use App\Classes\ImageUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;
use Image;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Session;
use Redirect;

class BrandsController extends Controller
{
    /**
     * Controll user permissions...
     *
     * @return void
     */
    public function __construct()
    {
        if(Auth::user())
        {
            $sideRole = Auth::user()->role_id;
            $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
            foreach ($sidePermissions as $sidePermission) {
                if ($sidePermission->name == 'Brand') {
                    if ($sidePermission->index == 0) {
                        $this->middleware('guest', ['only' => ['index','show']]);
                    } elseif ($sidePermission->edit == 0) {
                        $this->middleware('guest', ['only' => ['edit','update']]);
                    }elseif ($sidePermission->create == 0) {
                        $this->middleware('guest', ['only' => ['create','store']]);
                    }elseif ($sidePermission->delete == 0) {
                        $this->middleware('guest', ['only' => ['destroy']]);
                    }     
                }
            }
        }
        else
        {
            $this->middleware('guest');
        }
    }

    /**
     * Display a listing of the brand.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $brand = Brand::all();
        return view('brand.index',['brand'=> $brand]);

    }

    /**
     * Show the form for creating a new brand.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brand.create');
    }

    /**
     * Store a newly created brand in storage.
     *
     * @param  \Illuminate\Http\Request  $brand
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $brand = Brand::register($request);
    
            return $brand;
        
    }

    /**
     * Display the specified brand.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified brand.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        $brand = Brand::find( $brand->id );
        return view('brand.edit',['brand'=>$brand]);
    }

    /**
     * Update the specified brand in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $brand = Brand::updaterecord($request, $id);
        
        return $brand;
    }

    /**
     * Remove the specified brand from storage.
     *
     * @param  \App\Brand  $findBrand,$brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        
        $findBrand = Brand::deleterecord($brand);
        if($findBrand) {

            return redirect('brand')->with('alert', 'Deleted!');
        }

            return redirect('brand')->with('alert2','Brand could not be deleted!');
    }
}
