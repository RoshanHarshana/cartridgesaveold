<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App\Product;
use App\Printer;
use App\Brand;
use App\Classes\Search;
use App\Classes\FrontProduct;

class SearchController extends Controller
{

    /**
     * view printers and cartridge details.
     *
     * @param  \Illuminate\Http\Request  $resultPrinters,$resultCartridges,$searchid
     * @return \Illuminate\Http\Response
     */
    public function index($searchid)
    {

        if(isset($searchid)){

            $results = Search::SearchbyName($searchid);
            return $results;

        }

    }

    /**
     * view printers and cartridge details.
     *
     * @param  \Illuminate\Http\Request  $resultPrinters,$resultCartridges,$searchid
     * @return \Illuminate\Http\Response
     */
    public function indexOther($brandId, $searchid, $catId)
    {
        if(isset($searchid)){

            $results = Search::SearchinBrandName($brandId, $searchid, $catId);
            return $results;

        }

    }

    /**
     * view printers and cartridge details.
     *
     * @param  \Illuminate\Http\Request  $resultPrinters,$resultCartridges,$searchid
     * @return \Illuminate\Http\Response
     */
    public function other($section,$searchid)
    {

        if(isset($searchid)){

            $results = Search::SearchbyNameother($section,$searchid);
            return $results;

        }

    }

    /**
     * view ink cartridge printers.
     *
     * @param  \Illuminate\Http\Request  $brandname,$printername
     * @return \Illuminate\Http\Response
     */
    public function inkCartridgeShow($brandname , $printername, $printerId){

        $results = Search::CartridgeShow($brandname , $printername, $printerId);
        return $results;

    }

    /**
     * view toner cartridge printers.
     *
     * @param  \Illuminate\Http\Request  $brandname,$printername
     * @return \Illuminate\Http\Response
     */
    public function tonerCartridgeShow($brandname , $printername, $printerId){

        $results = Search::CartridgeShow($brandname , $printername, $printerId);
        return $results;

    }

    /**
     * view toner cartridge printers.
     *
     * @param  \Illuminate\Http\Request  $brandname,$printername
     * @return \Illuminate\Http\Response
     */
    public function ribbonCartridgeShow($brandname , $printername, $printerId){

        $results = Search::CartridgeShow($brandname , $printername, $printerId);
        return $results;

    }

    /**
     * view toner cartridge printers.
     *
     * @param  \Illuminate\Http\Request  $brandname,$printername
     * @return \Illuminate\Http\Response
     */
    public function bulkinkCartridgeShow($brandname , $printername, $printerId){

        $results = Search::CartridgeShow($brandname , $printername, $printerId);
        return $results;

    }

    /**
     * view toner cartridge printers.
     *
     * @param  \Illuminate\Http\Request  $brandname,$printername
     * @return \Illuminate\Http\Response
     */
    public function powderCartridgeShow($brandname , $printername, $printerId){

        $results = Search::CartridgeShow($brandname , $printername, $printerId);
        return $results;

    }

    /**
     * view all cartridge details.
     *
     * @param  \Illuminate\Http\Request  $printername,$resultCartridges
     * @return \Illuminate\Http\Response
     */
    public function printerShow($printername){

        $results = FrontProduct::ProductShow($printername);
        return $results;
    }

    /**
     *get cartridge for brands and categories.
     *
     * @param  \Illuminate\Http\Request  $category,$brandId
     * @return \Illuminate\Http\Response
     */
    public function getBrands($category,$brandname){

        if($category == 'ink-cartridges')
        {
            $catId = 1;
        }
        elseif($category == 'toner-cartridges')
        {
            $catId = 2;
        }
        elseif($category == 'ribbon-cartridges')
        {
            $catId = 3;
        }
        elseif($category == 'bulk-ink')
        {
            $catId = 4;
        }
        elseif($category == 'powder')
        {
            $catId = 5;
        }
        
        $resultBrands = Brand::where('name', '=', $brandname)->get();
        $brandId = $resultBrands[0]->id;

        //dd($catId." ".$brandId);

        $resultProducts = Product::where('category_id', '=', $catId)->where('brand_id', '=', $brandId)->get();
        $resultProductsCount = count($resultProducts);
        $resultPrinters = Printer::where('brand_id', '=', $brandId)->where('category_id', '=', $catId)->get();
        //$resultBrands = Brand::where('id', '=', $brandId)->get();

        //dd($resultProducts);

        return view('search.headerlink',compact('resultProducts','resultProductsCount','resultPrinters','resultBrands','catId'));
    }

}
