<?php

namespace App\Http\Controllers;

use App\Orderproduct;
use Illuminate\Http\Request;

class OrderproductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Orderproduct  $orderproduct
     * @return \Illuminate\Http\Response
     */
    public function show(Orderproduct $orderproduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Orderproduct  $orderproduct
     * @return \Illuminate\Http\Response
     */
    public function edit(Orderproduct $orderproduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Orderproduct  $orderproduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orderproduct $orderproduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Orderproduct  $orderproduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orderproduct $orderproduct)
    {
        //
    }
}
