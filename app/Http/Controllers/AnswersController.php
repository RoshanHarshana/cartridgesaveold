<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Redirect;
use Snowfire\Beautymail\Beautymail;

class AnswersController extends Controller
{
    /**
     * Controll user permissions...
     *
     * @return void
     */
    public function __construct()
    {
        if(Auth::user())
        {
            $sideRole = Auth::user()->role_id;
            $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
            foreach ($sidePermissions as $sidePermission) {
                if ($sidePermission->name == 'FAQ') {
                    if ($sidePermission->index == 0) {
                        $this->middleware('guest', ['only' => ['index','show']]);
                    } elseif ($sidePermission->edit == 0) {
                        $this->middleware('guest', ['only' => ['edit','update','updateApprove']]);
                    }elseif ($sidePermission->create == 0) {
                        $this->middleware('guest', ['only' => ['create','store']]);
                    }elseif ($sidePermission->delete == 0) {
                        $this->middleware('guest', ['only' => ['destroy']]);
                    }     
                }
            }
        }
        else
        {
            $this->middleware('guest');
        }
    }

    /**
     * Display a listing of the answer.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = Question::orderBy('created_at', 'desc')->get();
        return view('answer.index',['question'=> $question]);
    }

    /**
     * Show the form for creating a new answer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($question)
    {
        $question = Question::find($question);
        return view('answer.create',['question'=> $question]);
    }

    /**
     * Store a newly created answer in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Beautymail $beautymail)
    {
        $answer = Answer::register($request,$beautymail);
        return $answer;
    }

    /**
     * Display the specified answer.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function show(Answer $answer)
    {
        //
    }

    /**
     * Show the form for editing the specified answer.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Answer $answer)
    {
        $answer = Answer::find($answer->id);
        return view('answer.edit',['answer'=> $answer]);
    }

    /**
     * Update the specified answer in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer = Answer::updaterecord($request, $id);
        return $answer;
    }

    /**
     * Update the specified answer in storage.
     *
     * @param  \Illuminate\Http\Request  $answer, $approve
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function updateApprove($question , $approve){

        $approve = Answer::updateapprove($question, $approve);

        return $approve;

    }

    /**
     * Remove the specified answer from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        $findAnswer = Answer::deleterecord($answer);
        if($findAnswer) {

            return redirect('answer')->with('alert', 'Deleted!');
        }

            return redirect('answer')->with('alert2','Answer could not be deleted!');
    }
}
