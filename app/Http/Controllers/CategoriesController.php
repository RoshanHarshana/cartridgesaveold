<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Redirect;

class CategoriesController extends Controller
{
    /**
     * Controll user permissions...
     *
     * @return void
     */
    public function __construct()
    {
        if(Auth::user())
        {
            $sideRole = Auth::user()->role_id;
            $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
            foreach ($sidePermissions as $sidePermission) {
                if ($sidePermission->name == 'Category') {
                    if ($sidePermission->index == 0) {
                        $this->middleware('guest', ['only' => ['index','show']]);
                    } elseif ($sidePermission->edit == 0) {
                        $this->middleware('guest', ['only' => ['edit','update']]);
                    }elseif ($sidePermission->create == 0) {
                        $this->middleware('guest', ['only' => ['create','store']]);
                    }elseif ($sidePermission->delete == 0) {
                        $this->middleware('guest', ['only' => ['destroy']]);
                    }     
                }
            }
        }
        else
        {
            $this->middleware('guest');
        }
    }

    /**
     * Display a listing of the category.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('category.index',['category'=> $category]);
    }

    /**
     * Show the form for creating a new category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('category.create');
    }

    /**
     * Store a newly created category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response return alert
     */
    public function store(Request $request)
    {
        
        $category = Category::register($request);
        
        if($category){
            return redirect()->back()->with('alert', 'Success!');
        }
        else{
            return redirect()->back()->with('alert2', 'Not Success!');
        }
    }

    /**
     * Display the specified category.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //

    }

    /**
     * Show the form for editing the specified category.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response return alert
     */
    public function edit(Category $category)
    {
        //
        $category = Category::find( $category->id );
        return view('category.edit',['category'=>$category]);
    }

    /**
     * Update the specified category in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response return alert
     */
    public function update(Request $request, $id)
    {
        
        $category = Category::updaterecord($request, $id);

        $category = Category::all();
        return view('category.index',['category'=> $category]);
    }

    /**
     * Remove the specified category from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response return alert
     */
    public function destroy(Category $category)
    {
        //
        $category = Category::deleterecord($category);
        
        if($category) {

            return redirect('category')->with('alert', 'Deleted!');
        }
        else
        {
            return redirect('category')->with('alert2','Category could not be deleted!');
        }
    }
}
