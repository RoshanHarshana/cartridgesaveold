<?php

namespace App\Http\Controllers;

use App\User;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Redirect;

class UsersController extends Controller
{
    /**
     * Controll user permissions...
     *
     * @return void
     */
    public function __construct()
    {
        if(Auth::user())
        {
            $sideRole = Auth::user()->role_id;
            $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
            foreach ($sidePermissions as $sidePermission) {
                if ($sidePermission->name == 'User') {
                    if ($sidePermission->index == 0) {
                        $this->middleware('guest', ['only' => ['index','show','privileges']]);
                    } elseif ($sidePermission->edit == 0) {
                        $this->middleware('guest', ['only' => ['edit','update','updateprivileges']]);
                    }elseif ($sidePermission->create == 0) {
                        $this->middleware('guest', ['only' => ['create','store']]);
                    }elseif ($sidePermission->delete == 0) {
                        $this->middleware('guest', ['only' => ['destroy']]);
                    }     
                }
            }
        }
        else
        {
            $this->middleware('guest');
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('user.index',['user'=>$user]);
    }

    /**
     * Display all users.
     *
     * @param  \Illuminate\Http\Request  $user
     * @return \Illuminate\Http\Response
     */
    public function privileges($user)
    {
        $userRole = $user;
        $priview = Permission::where('role_id', $user)->get();
        return view('user.show',['priview'=>$priview],['userRole'=>$userRole]);
        
    }

    /**
     * Display all users.
     *
     * @param  \Illuminate\Http\Request  $role
     * @return \Illuminate\Http\Response
     */
    public function updateprivileges(Request $request)
    {

        $user = User::updateprivilegesNew($request);
        return $user;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::register($request);
        return $user;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = User::find( $user->id );
        return view('user.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $printer = User::updaterecord($request, $id);

        $user = User::all();
        return view('user.index',['user'=>$user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user = User::deleterecord($user);
        return $user;
    }
}
