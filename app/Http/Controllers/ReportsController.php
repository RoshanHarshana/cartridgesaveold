<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Product;
use App\Diliveryrate;
use App\Order;
use App\Orderproduct;
use View;
use Auth;

class ReportsController extends Controller
{
    /**
     * Controll user permissions...
     *
     * @return void
     */
    public function __construct()
    {
        if(Auth::user())
        {
            $sideRole = Auth::user()->role_id;
            if ($sideRole == 2 || $sideRole ==4) {

                    $this->middleware('guest', ['only' => ['orderreport','paymentreport','viewOrderDetails','salesreport','searchsales']]); 

            }
        }
        else
        {
            $this->middleware('guest');
        }
    }

    /**
     * Display all orders
     *
     * @return \Illuminate\Http\Response
     */
    public function orderreport()
    {
        $orders = Order::orderBy('created_at', 'DESC')->get();
        return view('reports.order',['orders'=> $orders]);
    }

    /**
     * Display all Payments
     *
     * @return \Illuminate\Http\Response
     */
    public function paymentreport()
    {
        $payments = Payment::orderBy('created_at', 'DESC')->get();
        return view('reports.payment',['payments'=> $payments]);
    }

    /**
     * View order details
     *
     * @return \Illuminate\Http\Response
     */
    public function viewOrderDetails($orderId)
    {
        $orders = Order::where('id', '=', $orderId)->get();
        //$payments = Payment::where('order_id', '=', $orderId)->get();
        return view('reports.order_view',['orders'=> $orders]);
    }

    /**
     * View sales details
     *
     * @return \Illuminate\Http\Response
     */
    public function salesreport()
    {
        $orders = Order::where('status', '=', 'paid')->orderBy('created_at', 'DESC')->get();
        $products = Product::all();
        $diliveryrates = Diliveryrate::all();
        return view('reports.sales',['orders' => $orders, 'products' => $products, 'diliveryrates' => $diliveryrates]);
    }

    /**
     * filter sales report...
     *
     * @return \Illuminate\Http\Response
     */
    public function searchsales(Request $request)
    {
        $startDate = $request->input('startdate');
        $newStartdate = date("Y-m-d", strtotime($startDate));

        $enddate = $request->input('enddate');
        $newEnddate = date("Y-m-d", strtotime($enddate));

        $cartridge = $request->input('cartridge');
        $city = $request->input('city');

        if ($startDate !='' && $enddate !='' && $cartridge !='' && $city !='') 
        {
            $orders = Order::whereHas('orderproducts' , function ($query) use ($cartridge,$newStartdate,$newEnddate)
            {
                $query->where('product_id', '=', $cartridge)->where('status', '=', 'paid')->whereBetween('created_at', [$newStartdate, $newEnddate])->groupBy('product_id')->orderBy('created_at', 'DESC');
            })->WhereHas('addresses', function($q) use ($city) {
                $q->where('id', '=', $city);
            })->get();
        } 
        elseif ($startDate !='' && $enddate !='' && $cartridge !='' && $city =='')
        {
            $orders = Order::whereHas('orderproducts' , function ($query) use ($cartridge,$newStartdate,$newEnddate)
            {
                $query->where('product_id', '=', $cartridge)->where('status', '=', 'paid')->whereBetween('created_at', [$newStartdate, $newEnddate])->groupBy('product_id')->orderBy('created_at', 'DESC');
            })->get();
        }
        elseif ($startDate !='' && $enddate !='' && $cartridge =='' && $city =='')
        {
            $orders = Order::where('status', '=', 'paid')->whereBetween('created_at', [$newStartdate, $newEnddate])->orderBy('created_at', 'DESC')->get();
        }
        elseif ($startDate !='' && $enddate !='' && $cartridge =='' && $city !='')
        {
            $orders = Order::whereHas('orderproducts' , function ($query) use ($newStartdate,$newEnddate)
            {
                $query->where('status', '=', 'paid')->whereBetween('created_at', [$newStartdate, $newEnddate])->groupBy('product_id')->orderBy('created_at', 'DESC');
            })->WhereHas('addresses', function($q) use ($city) {
                $q->where('id', '=', $city);
            })->get();
        }

        $products = Product::all();
        $diliveryrates = Diliveryrate::all();
        return view('reports.sales',['orders' => $orders, 'products' => $products, 'diliveryrates' => $diliveryrates]);

    }
     /**
     * View order details
     *
     * @return \Illuminate\Http\Response
     */
    public function mail()
    {
        $text = 'Hello';
        return view('email.test',['text'=>$text]);
    }
}
