<?php

namespace App\Http\Controllers;

use App\Question;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Redirect;

class QuestionsController extends Controller
{
    /**
     * Controll user permissions...
     *
     * @return void
     */
    public function __construct()
    {
        if(Auth::user())
        {
            $sideRole = Auth::user()->role_id;
            $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
            foreach ($sidePermissions as $sidePermission) {
                if ($sidePermission->name == 'FAQ') {
                    if ($sidePermission->index == 0) {
                        $this->middleware('guest', ['only' => ['index','show']]);
                    } elseif ($sidePermission->edit == 0) {
                        $this->middleware('guest', ['only' => ['edit','update']]);
                    }elseif ($sidePermission->create == 0) {
                        $this->middleware('guest', ['only' => ['create','store']]);
                    }elseif ($sidePermission->delete == 0) {
                        $this->middleware('guest', ['only' => ['destroy','destroyDashboard']]);
                    }     
                }
            }
        }
        else
        {
            $this->middleware('guest');
        }
    }

    /**
     * Display a listing of the question.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = Question::all();
        return view('question.index',['question'=> $question]);
    }

    /**
     * Show the form for creating a new question.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = Product::all();
        return view('question.create',['product'=> $product]);
    }

    /**
     * Store a newly created question in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question = Question::register($request);
        return $question;
    }

    /**
     * Display the specified question.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified question.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        $product = Product::all();
        $question = Question::find( $question->id );
        return view('question.edit',['question'=>$question],['product'=>$product]);
    }

    /**
     * Update the specified question in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = Question::updaterecord($request, $id);
        
        $questionNew = Question::all();
        return view('question.index',['question'=> $questionNew]);
    }

    /**
     * Remove the specified question from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $findQuestion = Question::deleterecord($question);
        if($findQuestion) {

            return redirect('question')->with('alert', 'Deleted!');
        }

            return redirect('question')->with('alert2','Question could not be deleted!');
    }

    /**
     * Remove the specified question from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroyDashboard(Question $question)
    {
        $findQuestion = Question::deleterecordNew($question);
        if($findQuestion) {

            return redirect('admin')->with('alert', 'Deleted!');;
        }

            return redirect('admin')->with('alert2','Question could not be deleted!');
    }

}
