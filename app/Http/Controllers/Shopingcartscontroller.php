<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Product;
use App\Classes\Cart;
use App\Classes\Apirequest;
use App\Order;
use App\User;
use App\Address;
use App\Shopping;
use App\Diliveryrate;
use Auth;
use DB;
use App\Http\Requests; 
use Session;
use Stripe\Stripe;
use Stripe\Charge;
use App\Payment;
use App\Attribute;
use Snowfire\Beautymail\Beautymail;

class Shopingcartscontroller extends Controller
{

    /**
     * Add to card.
     *
     * 
     */
    public function getAddToCart(Request $request, $id, $UrlPast, $count, $stock)
    {
        $getProducts = Product::where('id', '=',  $id)->get();
        $realStock = $getProducts[0]->stock;

        $product = Product::find($id);
        $product->stock = $realStock-$count;
        $product->save();

        for($i = 1; $i <= $count; $i++)
        {
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->add($product, $id);
            $request->session()->put('cart', $cart);
        }

        $attributes = Attribute::where('product_id', '=', $id)->where('name', '=', 'product_part_no')->get();
        $attributesName = $attributes[0]->value;

        $otherattributes = Attribute::where('value', '=', $attributesName)->where('name', '=', 'product_part_no')->where('product_id', '!=', $id)->get();

        if($otherattributes)
        {
            foreach($otherattributes as $otherattributes)
            {
                $attrProId = $otherattributes->product_id;
                $product = Product::find($attrProId);
                $product->stock = $realStock-$count;
                $product->save();
            }
        }

        return redirect($UrlPast);
    }

    /**
     * Add to card only product
     *
     * 
     */
    public function getAddToCartonlyproduct(Request $request, $id, $stock, $count, $UrlPast)
    {
        //dd($count);
        $getProducts = Product::where('id', '=',  $id)->get();
        $realStock = $getProducts[0]->stock;

        $product = Product::find($id);
        $product->stock = $realStock-$count;
        $product->save();

        for($i = 1; $i <= $count; $i++)
        {
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->add($product, $id);
            $request->session()->put('cart', $cart);
        }

        $attributes = Attribute::where('product_id', '=', $id)->where('name', '=', 'product_part_no')->get();
        $attributesName = $attributes[0]->value;

        $otherattributes = Attribute::where('value', '=', $attributesName)->where('name', '=', 'product_part_no')->where('product_id', '!=', $id)->get();

        if($otherattributes)
        {
            foreach($otherattributes as $otherattributes)
            {
                $attrProId = $otherattributes->product_id;
                $product = Product::find($attrProId);
                $product->stock = $realStock-$count;
                $product->save();
            }
        }

        return redirect($UrlPast);
    }


    public function getAddToCartTwo(Request $request, $id, $count, $stock)
    {
        //dd('hii');
        $getProducts = Product::where('id', '=',  $id)->get();
        $realStock = $getProducts[0]->stock;

        $product = Product::find($id);
        $product->stock = $realStock-$count;
        $product->save();

        for($i = 1; $i <= $count; $i++)
        {
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->add($product, $id);
            $request->session()->put('cart', $cart);
        }

        $attributes = Attribute::where('product_id', '=', $id)->where('name', '=', 'product_part_no')->get();
        $attributesName = $attributes[0]->value;

        $otherattributes = Attribute::where('value', '=', $attributesName)->where('name', '=', 'product_part_no')->where('product_id', '!=', $id)->get();

        if($otherattributes)
        {
            foreach($otherattributes as $otherattributes)
            {
                $attrProId = $otherattributes->product_id;
                $product = Product::find($attrProId);
                $product->stock = $realStock-$count;
                $product->save();
            }
        }
        
        // $resStrNew = "/".$category."/".$brand."/".$printer;
        // return redirect($resStrNew);
    }

    // change cart
    public function getAddToCartChange(Request $request, $id, $count, $stock, $side)
    {
        $getProducts = Product::where('id', '=',  $id)->get();
        $realStock = $getProducts[0]->stock;

        if($side == 'min')
        {
            $newStock = $realStock + 1;
        }
        elseif($side == 'max')
        {
            $newStock = $realStock - 1;
        }

        $product = Product::find($id);
        $product->stock = $newStock;
        $product->save();

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        $request->session()->put('cart', $cart);

        for($i = 1; $i <= $count; $i++)
        {
            $product = Product::find($id);
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->add($product, $id);
            $request->session()->put('cart', $cart);
        }

        $attributes = Attribute::where('product_id', '=', $id)->where('name', '=', 'product_part_no')->get();
        $attributesName = $attributes[0]->value;

        $otherattributes = Attribute::where('value', '=', $attributesName)->where('name', '=', 'product_part_no')->where('product_id', '!=', $id)->get();

        if($otherattributes)
        {
            foreach($otherattributes as $otherattributes)
            {
                $attrProId = $otherattributes->product_id;
                $product = Product::find($attrProId);
                $product->stock = $newStock;
                $product->save();
            }
        }
        
        // $resStrNew = "/".$category."/".$brand."/".$printer;
        // return redirect($resStrNew);
    }

    /**
     * remove one item.
     *
     * 
     */
    public function getReduceByOne($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);
        Session::put('cart', $cart);
        
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return redirect()->action('Shopingcartscontroller@getCart');
    }

    /**
     * remove all items.
     *
     * 
     */
    public function getRemoveItem($id, $stock, $count)
    {
        $getProducts = Product::where('id', '=',  $id)->get();
        $realStock = $getProducts[0]->stock;

        $product = Product::find($id);
        $product->stock = $realStock + $count;
        $product->save(); 

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);

        if(count($cart->items) >0)
        {
            Session::put('cart', $cart);
        }
        else{
            Session::forget('cart');
        }
        
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        $attributes = Attribute::where('product_id', '=', $id)->where('name', '=', 'product_part_no')->get();
        $attributesName = $attributes[0]->value;

        $otherattributes = Attribute::where('value', '=', $attributesName)->where('name', '=', 'product_part_no')->where('product_id', '!=', $id)->get();

        if($otherattributes)
        {
            foreach($otherattributes as $otherattributes)
            {
                $attrProId = $otherattributes->product_id;
                $product = Product::find($attrProId);
                $product->stock = $realStock + $count;
                $product->save();
            }
        }
        
        return redirect()->action('Shopingcartscontroller@getCart');
    }

    /**
     * view billing information.
     *
     * 
     */
    public function getCart()
    {
        if(!Session::has('cart'))
        {
            return view('shop.shopping-cart', ['products' => null]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $diliveryCharge = 0;
        return view('shop.shopping-cart', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'diliveryCharge'=>$diliveryCharge]);
    }

    /**
     * after press checkout button.
     *
     * 
     */
    public function getCheckout()
    {
        if(!Session::has('cart'))
        {
            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;
        $diliveryCharge = 0;
        return view('shop.checkout',['products' => $cart->items , 'totalPrice' => $cart->totalPrice, 'total'=>$total, 'diliveryCharge'=>$diliveryCharge]);
    }

    /**
     * submit and store billing details.
     *
     * 
     */
    public function postCheckout(Request $request)
    {
        if(!Session::has('cart'))
        {
            return redirect()->route('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        Stripe::setApiKey('sk_test_U6fmtshF7XdrpbS5D10QqMz7');
        try {
            $charge=Charge::create(array(
                "amount" => $cart->totalPrice * 100,
                "currency" => "usd",
                "source" => $request->input('stripeToken'), //obtained with Stripe.js
                "description" => "Test Change"
            ));

            $order = new Order();
            $order->cart = serialize($cart);
            $order->address = $request->input('address');
            $order->name = $request->input('name');
            $order->payment_id = $charge->id;

            Auth::user()->orders()->save($order);

        } catch (\Exception $e){
            return redirect()->route('checkout')->with('error', $e->getMessage());
        }

        Session::forget('cart');
        return redirect()->route('search.show')->with('success', 'Successfully purchased products!');
    }

    /**
     * checkemail.
     *
     * 
     */
    public function checkemail(Request $request)
    {
        $result = Shopping::checkemailForuser($request);
        return $result;

    }

    /**
     * cal delivery rates.
     *
     * 
     */
    public function delivery(Request $request)
    {

        $result = Shopping::checkDelivery($request);
        return $result;

    }

    /**
     * for fineshed.
     *
     * 
     */
    public function finished(Request $request, Beautymail $beautymail)
    {

        $result = Shopping::forFinished($request,$beautymail);
        return $result;

    }

    /**
     * delete session.
     *
     * 
     */
    public function done(Request $request)
    {

        $result = Shopping::done($request);
        return $result;

    }
    
    /**
     * for analize details
     *
     * 
     */
    public function getapidetails(Request $request)
    {
        $result = Shopping::getapidetails($request);
        return $result;
    }

    /**
     * for getcashdetails details
     *
     * 
     */
    public function getcashdetails(Request $request, Beautymail $beautymail)
    {
        $result = Shopping::getcashdetails($request,$beautymail);
        return $result;
    }

    /**
     * view contact details
     *
     * 
     */
    public function contact()
    {
        return view('contact');
    }

    /**
     * get city details
     *
     * 
     */
    public function getcityDetails($id)
    {
        $cityDetails = Diliveryrate::find($id);
        return response()->json(['success' => true, 'cityDetails' => $cityDetails]);
    }
}
