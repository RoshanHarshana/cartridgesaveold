<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\User;
use App\Order;
use Snowfire\Beautymail\Beautymail;

class CustomerController extends Controller
{
    /**
     * Add the customer question from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function customerQuestion(Request $request, Beautymail $beautymail)
    {
        $question = Question::registerCustomerque($request,$beautymail);
        return $question;
    }

    /**
     * Add the customer question from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function forviewOrders($userId)
    {
        $orders = Order::where('user_id', '=', $userId)->orderBy('created_at', 'DESC')->get();
        return view('customer.viewhistry',['orders' => $orders]);
    }
}