<?php

namespace App\Http\Controllers;

use App\Diliveryrate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Redirect;

class DeliveryratesController extends Controller
{
    /**
     * Controll user permissions...
     *
     * @return void
     */
    public function __construct()
    {
        if(Auth::user())
        {
            $sideRole = Auth::user()->role_id;
            $sidePermissions = DB::select('select * from permissions where role_id = ?', [$sideRole]);
            foreach ($sidePermissions as $sidePermission) {
                if ($sidePermission->name == 'Rate') {
                    if ($sidePermission->index == 0) {
                        $this->middleware('guest', ['only' => ['index','show']]);
                    } elseif ($sidePermission->edit == 0) {
                        $this->middleware('guest', ['only' => ['edit','update']]);
                    }elseif ($sidePermission->create == 0) {
                        $this->middleware('guest', ['only' => ['create','store']]);
                    }elseif ($sidePermission->delete == 0) {
                        $this->middleware('guest', ['only' => ['destroy']]);
                    }     
                }
            }
        }
        else
        {
            $this->middleware('guest');
        }
    }
    
    /**
     * Display a listing of the dilivery rate.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dilivery = Diliveryrate::all();
        return view('diliveryrate.index',['dilivery'=> $dilivery]);
    }

    /**
     * Show the form for creating a new dilivery rate.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('diliveryrate.create');
    }

    /**
     * Store a newly created dilivery rate in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dilivery = Diliveryrate::register($request);

        return $dilivery;
    }

    /**
     * Display the specified dilivery rate.
     *
     * @param  \App\Diliveryrate  $diliveryrate
     * @return \Illuminate\Http\Response
     */
    public function show(Diliveryrate $diliveryrate)
    {
        //
    }

    /**
     * Show the form for editing the specified dilivery rate.
     *
     * @param  \App\Diliveryrate  $diliveryrate
     * @return \Illuminate\Http\Response
     */
    public function edit(Diliveryrate $diliveryrate)
    {
        $diliveryrate = Diliveryrate::find( $diliveryrate->id );
        return view('diliveryrate.edit',['dilivery'=>$diliveryrate]);
    }

    /**
     * Update the specified dilivery rate in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Diliveryrate  $diliveryrate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $diliveryrate = Diliveryrate::updaterecord($request, $id);

        $diliveryrateNew = Diliveryrate::all();
        return view('diliveryrate.index',['dilivery'=> $diliveryrateNew]);
    }

    /**
     * Remove the specified dilivery rate from storage.
     *
     * @param  \App\Diliveryrate  $diliveryrate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Diliveryrate $diliveryrate)
    {
        $finddiliveryrate = Diliveryrate::deleterecord($diliveryrate);
        
        return $finddiliveryrate;
    }
}
