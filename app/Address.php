<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'street_name', 
        'city',
        'province',
        'postal_code',
        'address_type',
        'company_name',
        'user_id',
        'order_id',
        'phone',
        'street_no'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
}
