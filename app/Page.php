<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'product_id', 
        'color_id',
        'pages_nos',
        'multiply',
        'capacity',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
    public function color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }
}
