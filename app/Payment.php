<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'payment_type',
        'merchant_id',
        'payment_id',
        'payhere_amount',
        'payhere_currency',
        'status_code',
        'md5sig',
        'custom_msg',
        'date',
        'order_id'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
}
