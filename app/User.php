<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Permission;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'lastname',
        'email', 
        'password',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // relations
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
    public function addresses()
    {
        return $this->hasMany('App\Address');
    }
    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }
    //end relations

    /**
     * Store a newly created Printer in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $nameOld
     * @return \Illuminate\Http\Response $post
     */
    public static function register($request){

        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role'=> 'required|int|max:11',
        ]);

        if($validatedData)
        {
            $md5val = Hash::make($request->input('password'));

            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $md5val,
                'role_id' => $request->input('role')
            ]);

            if($user){
                return redirect()->back()->with('alert', 'Success!');
            }
            else
            {
                return redirect()->back()->with('alert2', 'Not Success!');
            }
        }
        else
        {
            return $validatedData;
        }

    }

    /**
     * Update the specified Printer in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response 
     */
    public static function updaterecord($request, $id){

        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'role'=> 'required|int|max:11',
        ]);

        if($validatedData)
        {
            $post = User::find($id);
            $post->name = $request->input('name');
            $post->email = $request->input('email');
            $post->role_id = $request->input('role');
            $post->save();

        }
        else
        {
            return $validatedData;
        }

    }

    /**
     * Remove the specified printer in storage.
     *
     * @param  \Illuminate\Http\Request  $printer
     * @return \Illuminate\Http\Response $findPrinter
     */
    public static function deleterecord($user){

        $finduser = User::find( $user->id );
        if($finduser->delete()) {

            return redirect('user')->with('alert', 'Deleted!');
        }

            return redirect('user')->with('error','Printer could not be deleted!');

    }


    /**
     * change previlages
     *
     * @param  \Illuminate\Http\Request  $printer
     * @return \Illuminate\Http\Response $findPrinter
     */
    public static function updateprivilegesNew($request){

        $userRole = $request->input('userRole');
        $permission = Permission::where('role_id', $userRole)->delete();

        //save Category
        $post = new Permission;
        $post->name = "Category";
        $post->role_id = $userRole;
        $check = $request->input('Categoryview');
        if($check == 1)
        {
            $post->index = 1;
        }
        else
        {
            $post->index = 0;
        }

        $check = $request->input('Categoryedit');
        if($check == 1)
        {
            $post->edit = 1;
        }
        else
        {
            $post->edit = 0;
        }

        $check = $request->input('Categorycreate');
        if($check == 1)
        {
            $post->create = 1;
        }
        else
        {
            $post->create = 0;
        }

        $check = $request->input('Categorydelete');
        if($check == 1)
        {
            $post->delete = 1;
        }
        else
        {
            $post->delete = 0;
        }
        $post->save();
        // dd($post);


        //save Color
        $post = new Permission;
        $post->name = "Color";
        $post->role_id = $userRole;
        $check = $request->input('Colorview');
        if($check == 1)
        {
            $post->index = 1;
        }
        else
        {
            $post->index = 0;
        }

        $check = $request->input('Coloredit');
        if($check == 1)
        {
            $post->edit = 1;
        }
        else
        {
            $post->edit = 0;
        }

        $check = $request->input('Colorcreate');
        if($check == 1)
        {
            $post->create = 1;
        }
        else
        {
            $post->create = 0;
        }

        $check = $request->input('Colordelete');
        if($check == 1)
        {
            $post->delete = 1;
        }
        else
        {
            $post->delete = 0;
        }
        $post->save();


        //save Brand
        $post = new Permission;
        $post->name = "Brand";
        $post->role_id = $userRole;
        $check = $request->input('Brandview');
        if($check == 1)
        {
            $post->index = 1;
        }
        else
        {
            $post->index = 0;
        }

        $check = $request->input('Brandedit');
        if($check == 1)
        {
            $post->edit = 1;
        }
        else
        {
            $post->edit = 0;
        }

        $check = $request->input('Brandcreate');
        if($check == 1)
        {
            $post->create = 1;
        }
        else
        {
            $post->create = 0;
        }

        $check = $request->input('Branddelete');
        if($check == 1)
        {
            $post->delete = 1;
        }
        else
        {
            $post->delete = 0;
        }
        $post->save();


        //save Printer
        $post = new Permission;
        $post->name = "Printer";
        $post->role_id = $userRole;
        $check = $request->input('Printerview');
        if($check == 1)
        {
            $post->index = 1;
        }
        else
        {
            $post->index = 0;
        }

        $check = $request->input('Printeredit');
        if($check == 1)
        {
            $post->edit = 1;
        }
        else
        {
            $post->edit = 0;
        }

        $check = $request->input('Printercreate');
        if($check == 1)
        {
            $post->create = 1;
        }
        else
        {
            $post->create = 0;
        }

        $check = $request->input('Printerdelete');
        if($check == 1)
        {
            $post->delete = 1;
        }
        else
        {
            $post->delete = 0;
        }
        $post->save();


        //save Product
        $post = new Permission;
        $post->name = "Product";
        $post->role_id = $userRole;
        $check = $request->input('Productview');
        if($check == 1)
        {
            $post->index = 1;
        }
        else
        {
            $post->index = 0;
        }

        $check = $request->input('Productedit');
        if($check == 1)
        {
            $post->edit = 1;
        }
        else
        {
            $post->edit = 0;
        }

        $check = $request->input('Productcreate');
        if($check == 1)
        {
            $post->create = 1;
        }
        else
        {
            $post->create = 0;
        }

        $check = $request->input('Productdelete');
        if($check == 1)
        {
            $post->delete = 1;
        }
        else
        {
            $post->delete = 0;
        }
        $post->save();


        //save FAQ
        $post = new Permission;
        $post->name = "FAQ";
        $post->role_id = $userRole;
        $check = $request->input('FAQview');
        if($check == 1)
        {
            $post->index = 1;
        }
        else
        {
            $post->index = 0;
        }

        $check = $request->input('FAQedit');
        if($check == 1)
        {
            $post->edit = 1;
        }
        else
        {
            $post->edit = 0;
        }

        $check = $request->input('FAQcreate');
        if($check == 1)
        {
            $post->create = 1;
        }
        else
        {
            $post->create = 0;
        }

        $check = $request->input('FAQdelete');
        if($check == 1)
        {
            $post->delete = 1;
        }
        else
        {
            $post->delete = 0;
        }
        $post->save();


        //save Rate
        $post = new Permission;
        $post->name = "Rate";
        $post->role_id = $userRole;
        $check = $request->input('Rateview');
        if($check == 1)
        {
            $post->index = 1;
        }
        else
        {
            $post->index = 0;
        }

        $check = $request->input('Rateedit');
        if($check == 1)
        {
            $post->edit = 1;
        }
        else
        {
            $post->edit = 0;
        }

        $check = $request->input('Ratecreate');
        if($check == 1)
        {
            $post->create = 1;
        }
        else
        {
            $post->create = 0;
        }

        $check = $request->input('Ratedelete');
        if($check == 1)
        {
            $post->delete = 1;
        }
        else
        {
            $post->delete = 0;
        }
        $post->save();


        //save User
        $post = new Permission;
        $post->name = "User";
        $post->role_id = $userRole;
        $check = $request->input('Userview');
        if($check == 1)
        {
            $post->index = 1;
        }
        else
        {
            $post->index = 0;
        }

        $check = $request->input('Useredit');
        if($check == 1)
        {
            $post->edit = 1;
        }
        else
        {
            $post->edit = 0;
        }

        $check = $request->input('Usercreate');
        if($check == 1)
        {
            $post->create = 1;
        }
        else
        {
            $post->create = 0;
        }

        $check = $request->input('Userdelete');
        if($check == 1)
        {
            $post->delete = 1;
        }
        else
        {
            $post->delete = 0;
        }
        $post->save();


        $priview = Permission::where('role_id', $userRole)->get();
        return view('user.show',['priview'=>$priview],['userRole'=>$userRole]);


    }
}
