<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = [
        'name', 
    ];

    // relations

    public function pages()
    {
        return $this->hasMany('App\Page');
    }

    // End relations

    /**
     * Store a newly created color in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $color
     */
    public static function register($request){

        $color = Color::create([
            'name' => $request->input('colorname')
        ]);

        return $color;
    }

    /**
     * Update the specified color in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response $post
     */
    public static function updaterecord($request, $id){

        $post = Color::find($id);
        $post->name = $request->input('colorname');
        $post->save();

        return $post;

    }

    /**
     * Remove the specified color in storage.
     *
     * @param  \Illuminate\Http\Request  $color
     * @return \Illuminate\Http\Response $color
     */
    public static function deleterecord($color){

        $color = Color::find( $color->id );
        $colorNew = $color->delete();

        if($color) {

            return redirect('color')->with('alert', 'Deleted!');
        }
        else
        {
            return redirect('color')->with('alert2','Color could not be deleted!');
        }

    }
}
