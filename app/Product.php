<?php

namespace App;

use App\Classes\ImageUpload;
use Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\ImageUp;

class Product extends Model
{
    protected $fillable = [
        'name',
        'category_id',
        'image',
        'price',
        'stock',
        'reorder_level',
        'delivery_type',
        'description',
        'url',
        'vat',
        'brand_id',
    ];

    // relations

    public function printers()
    {
        return $this->belongsToMany('App\Printer');
    }
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
    public function pages()
    {
        return $this->hasMany('App\Page');
    }
    public function features()
    {
        return $this->hasMany('App\Feature');
    }
    public function questions()
    {
        return $this->hasMany('App\Question');
    }
    public function orderproducts()
    {
        return $this->hasMany('App\Orderproduct');
    }
    public function attributes()
    {
        return $this->hasMany('App\Attribute');
    }
    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id');
    }

    // end relations

    /**
     * Store a newly created Product in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $post, return
     */
    public static function register($request){

        $width = 400;
        $height = 400;

        $nameOld = $request->input('productname');
        $urlOld = $request->input('urlcode');

        $recompatibility = $request->input('recompatibility');
        if ($recompatibility) {
            $numrecompatibility = count($recompatibility);
        } else {
            $numrecompatibility = 0;
        }

        // Get duplicate value
        $hasNameorUrl = DB::table('products')
                    ->where('url', [$urlOld])
                    ->orWhere('name', [$nameOld])
                    ->get();

        // Check duplicate value
        if($hasNameorUrl->isEmpty())
        {
            if ($numrecompatibility <= 4)
            {
                $descrip = $request->input('description');
                $calString = strlen($descrip);

                if($calString < 1000)
                {
                    $image = $request->file('image');

                    $imagesize = ImageUpload::checkSize($image);
                    $imagetype = ImageUpload::checkType($image);
                    //dd($imagetype);

                    if($imagesize == 'yes' && $imagetype == 'yes')
                    {
                        
                        $post = new Product;
                        $post->name = $request->input('productname');
                        $post->category_id = $catId = $request->input('category');
                        $post->brand_id = $request->input('brandid');
                        $post->price = $request->input('price');
                        $post->stock = $request->input('stock');
                        $post->reorder_level = $request->input('reorderlevel');
                        $post->delivery_type = $request->input('deliverytype');
                        $post->description = $request->input('description');
                        $post->url = $request->input('urlcode');
                        $post->vat = $request->input('vat');
                        $post->weight = $request->input('weight');

                        // store image
                        //dd(filesize($request->file('image')));
                        if ($request->hasfile('image'))
                        {

                            $imageupload = ImageUpload::uploadImage($image,$width,$height);
                            $post->image = $imageupload;

                        }

                        $post->save();

                        $saveID = $post->id;

                        // store memy to many relation data
                        $printers = $request->input('printers');
                        $count = count($printers);

                        for($i=0; $i<$count; $i++)
                        {
                                $post->printers()->attach($printers[$i]);
                        }

                        // store data to pages table
                        $categoryCheck = $request->input('category');
                        if ($categoryCheck != 'ribbon-cartridges') 
                        {
                            $colortype = $request->input('colortype');
                            $pages = $request->input('pages');
                            $multiply = $request->input('multiply');
                            $capacity = $request->input('capacity');
                            $newCount = count($colortype);
                            for($i=0; $i<$newCount; $i++)
                            {
                                $post = new Page;
                                    $post->product_id = $saveID;
                                    $post->color_id = $colortype[$i];
                                    $post->pages_nos = $pages[$i];
                                    $post->multiply = $multiply[$i];
                                    $post->capacity = $capacity[$i];

                                $post->save();
                            }
                        } 

                        // store image details
                        if ($request->hasfile('image'))
                        {
                            $image = $request->file('image');

                            $imageupload = ImageUpload::uploadImage($image,$width,$height);

                            // get extention
                            $ext = pathinfo(storage_path().$imageupload, PATHINFO_EXTENSION);

                            $post = new ImageUp;
                            $post->name = $imageupload;
                            $post->alt = '';
                            $post->type = $ext;
                            $post->save();
                        }

                        // store data to featuresone table
                        $featuresone = $request->input('featuresone');
                        $neWCount = count($featuresone);
                        for($i=0; $i<$neWCount; $i++)
                        {
                            $feacher = $featuresone[$i];
                            if ($feacher != '') 
                            {
                                $post = new Feature;
                                $post->product_id = $saveID;
                                $post->name = $featuresone[$i];
                                $post->save();
                            } 
                        }
                        
                        // store data to attributs table
                        $post = new Attribute;
                        $post->name = "color_type";
                        $post->value = $request->input('color');
                        $post->product_id = $saveID;
                        $post->save();

                        $post = new Attribute;
                        $post->name = "duty_cycle";
                        $post->value = $request->input('dutycycle');
                        $post->product_id = $saveID;
                        $post->save();

                        $post = new Attribute;
                        $post->name = "manufacture_part_no";
                        $post->value = $request->input('manpernum');
                        $post->product_id = $saveID;
                        $post->save();

                        $post = new Attribute;
                        $post->name = "product_part_no";
                        $post->value = $request->input('proparnum');
                        $post->product_id = $saveID;
                        $post->save();

                        $post = new Attribute;
                        $post->name = "compatibility";
                        $post->value = $request->input('compatibility');
                        $post->product_id = $saveID;
                        $post->save();

                        $post = new Attribute;
                        $post->name = "related_compatibility";
                        $products = $request->input('recompatibility');
                        if ($products == '') 
                        {
                            $post->value = "";
                        } 
                        else 
                        {
                            $post->value = $products = implode(',', $products);
                        }
                        
                        $post->product_id = $saveID;
                        $post->save();

                        $post = new Attribute;
                        $post->name = "related_products";
                        $products = $request->input('reproduct');
                        if($products=='')
                        {
                            $post->value = "";
                        }
                        else
                        {
                            $post->value = $products = implode(',', $products);
                        }
                        $post->product_id = $saveID;
                        $post->save();


                        return redirect()->back()->with('alert', 'Insert Success!');


                    }
                    else
                    {
                        return redirect()->back()->with('alert2', 'Unsupported image type or size! It must be lower 2Mb and type is jpg, jpeg, or png!');
                    }
                }
                else
                {
                    return redirect()->back()->with('alert2', 'Out of length Description field!');
                }
            } 
            else 
            {
                return redirect()->back()->with('alert2', 'Please select only three related compatibility product!');
            }
        }
        else
        {
            return redirect()->back()->with('alert2', 'Use another name or url code. Insert Not Success!');
        }  

    }

    /**
     * Update the specified product in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response return
     */
    public static function updaterecord($request, $id){

        $width = 400;
        $height = 400;

        $descrip = $request->input('description');
        $recompatibility = $request->input('recompatibility');
        if ($recompatibility) {
            $numrecompatibility = count($recompatibility);
        } else {
            $numrecompatibility = 0;
        }

        $calString = strlen($descrip);

        if($calString < 1000)
        {
            if ($numrecompatibility <= 4)
            {
            
                $image = $request->file('image');

                $post = Product::find($id);
                $post->name = $request->input('productname');
                $post->category_id = $catId = $request->input('category');
                $post->brand_id = $request->input('brandid');
                $post->price = $request->input('price');
                $post->stock = $request->input('stock');
                $post->reorder_level = $request->input('reorderlevel');
                $post->delivery_type = $request->input('deliverytype');
                $post->description = $request->input('description');
                $post->url = $request->input('urlcode');
                $post->vat = $request->input('vat');
                $post->weight = $request->input('weight');

                // store image
                if ($request->hasfile('image'))
                {

                    $imageupload = ImageUpload::uploadImage($image,$width,$height);
                    $post->image = $imageupload;

                    $oldfilename = $post->image;

                    $post->image =  $imageupload;

                    Storage::delete($oldfilename);
                }
                $post->save();

                $saveID = $id;

                // store memy to many relation data

                if (!empty($request->input('printers')))
                {
                    $printers = $request->input('printers');
                    $count = count($printers);

                    $post->printers()->detach();

                    for($i=0; $i<$count; $i++)
                    {

                        $post->printers()->attach($printers[$i]);

                    }
                }

                $pages = Page::where('product_id', $saveID)->delete();

                // store data to pages table
                $colortype = $request->input('colortype');
                $pages = $request->input('pages');
                $multiply = $request->input('multiply');
                $capacity = $request->input('capacity');
                $newCount = count($colortype);
                for($i=0; $i<$newCount; $i++)
                {

                        $colorNew = $colortype[$i];
                        $pageNew = $pages[$i];
                        $multyNew = $multiply[$i];
                        $capaNew = $capacity[$i];

                        if($colorNew !='' || $pageNew !='' || $multyNew !='' || $capaNew !='')
                        {
                            $post = new Page;

                            $post->product_id = $saveID;
                            $post->color_id = $colortype[$i];
                            $post->pages_nos = $pages[$i];
                            $post->multiply = $multiply[$i];
                            $post->capacity = $capacity[$i];

                            $post->save();
                        }

                    
                }

                $feature = Feature::where('product_id', $saveID)->delete();

                // store data to featuresone table
                $featuresone = $request->input('featuresone');
                $neWCount = count($featuresone);
                for($i=0; $i<$neWCount; $i++)
                {
                    $nameNew = $featuresone[$i];
                    if($nameNew != '')
                    {
                        $post = new Feature;

                        $post->product_id = $saveID;
                        $post->name = $featuresone[$i];

                        $post->save();
                    }
                }

                $attribute = Attribute::where('product_id', $saveID)->delete();

                // store data to attributs table
                $post = new Attribute;
                $post->name = "color_type";
                $post->value = $request->input('color');
                $post->product_id = $saveID;
                $post->save();

                $post = new Attribute;
                $post->name = "duty_cycle";
                $post->value = $request->input('dutycycle');
                $post->product_id = $saveID;
                $post->save();

                $post = new Attribute;
                $post->name = "manufacture_part_no";
                $post->value = $request->input('manpernum');
                $post->product_id = $saveID;
                $post->save();

                $post = new Attribute;
                $post->name = "product_part_no";
                $post->value = $request->input('proparnum');
                $post->product_id = $saveID;
                $post->save();

                $post = new Attribute;
                $post->name = "compatibility";
                $post->value = $request->input('compatibility');
                $post->product_id = $saveID;
                $post->save();

                $post = new Attribute;
                $post->name = "related_compatibility";
                $products = $request->input('recompatibility');
                if ($products == '') 
                {
                    $post->value = "";
                } 
                else 
                {
                    $post->value = $products = implode(',', $products);
                }
                $post->product_id = $saveID;
                $post->save();

                $post = new Attribute;
                $post->name = "related_products";
                $products = $request->input('reproduct');
                if($products=='')
                {
                    $post->value = "";
                }
                else
                {
                    $post->value = $products = implode(',', $products);
                }
                $post->product_id = $saveID;
                $post->save();

                //save image details for image_ups table
                if ($request->hasfile('image'))
                {
                    $image = $request->file('image');

                    $imageupload = ImageUpload::uploadImage($image,$width,$height);

                    // get extention
                    $ext = pathinfo(storage_path().$imageupload, PATHINFO_EXTENSION);

                    $post = new ImageUp;
                    $post->name = $imageupload;
                    $post->alt = '';
                    $post->type = $ext;
                    $post->save();
                }
                
                $products = Product::all();
                return view('product.index',['products'=> $products]);
            } 
            else 
            {
                return redirect()->back()->with('alert2', 'Please select only three related compatibility product!');
            }
        }
        else
        {
            return redirect()->back()->with('alert2', 'Out of length Description field!');
        }

    }

    /**
     * Remove the specified product in storage.
     *
     * @param  \Illuminate\Http\Request  $product
     * @return \Illuminate\Http\Response $findPrinter
     */
    public static function deleterecord($product){

        $findProduct = Product::find( $product->id );
        if($findProduct->delete()) {

            return redirect()->route('product.index')
            ->with('alert', 'Delete successfully!');
        }

            return back()->withInput()->with('alert','Could not be deleted!');

    }

    /**
     * edit the specified product in storage.
     *
     * @param  \Illuminate\Http\Request  $product
     * @return \Illuminate\Http\Response $findPrinter
     */
    public static function editproduct($product){

        $printers = Printer::all();
        $category = Category::all();
        $idProd = $product->id;
        $attributes = Attribute::where('product_id', $idProd)->get();
        $features = DB::select('select * from features where product_id = ?', [$idProd]);
        $product = Product::find( $product->id );
        $pages = Page::where('product_id', $idProd)->get();
        $prod = DB::table("products")->pluck("name","id");
        $color = DB::table("colors")->pluck("name","id");
        $brands = Brand::all();
        return view('product.edit',compact('product','category','printers','color','prod','attributes','features','pages','brands'));

    }
}
