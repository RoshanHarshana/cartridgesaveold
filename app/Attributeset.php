<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attributeset extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
}
