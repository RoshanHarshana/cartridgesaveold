<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageUp extends Model
{
    protected $fillable = [
        'name',
        'alt',
        'type',
    ];
}
