<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 
        'maincat_id',
    ];


    // relations

    public function printers()
    {
        return $this->hasMany('App\Printer');
    }
    public function products()
    {
        return $this->hasMany('App\Product');
    }
    public function attributesets()
    {
        return $this->belongsToMany('App\Attributeset');
    }

    // end relations

    /**
     * Store a newly created Category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response $category
     */
    public static function register($request){

        $category = Category::create([
            'name' => $request->input('categoryname')
        ]);

        return $category;

    }

    /**
     * Update the specified Category in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response $post
     */
    public static function updaterecord($request, $id){

        $post = Category::find($id);
        $post->name = $request->input('categoryname');
        $post->save();

        return $post;

    }

    /**
     * Remove the specified Category in storage.
     *
     * @param  \Illuminate\Http\Request  $category
     * @return \Illuminate\Http\Response $category
     */
    public static function deleterecord($category){

        $category = Category::find( $category->id );
        $categoryNew = $category->delete();

        return $category;

    }

}
