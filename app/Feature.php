<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $fillable = [
        'product_id', 
        'name',
    ];

    public function products()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
