<?php

namespace App;

use App\Classes\ImageUpload;
use Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\ImageUp;

class Printer extends Model
{
    //
    protected $fillable = [
        'name',
        'image',
        'brand_id',
        'category_id',
        'description',
        'printer_code',
    ];

    // Relations

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id');
    }
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    // End relations


    /**
     * Store a newly created Printer in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $nameOld
     * @return \Illuminate\Http\Response $post
     */
    public static function register($request){

        $nameOld = $request->input('printername');
        $hasName = DB::select('select name from printers where name = ?', [$nameOld]);
        if(!$hasName)
        {
            $descrip = $request->input('description');
            $calString = strlen($descrip);

            if($calString < 1000)
            {

                $image = $request->file('image');

                $imagesize = ImageUpload::checkSize($image);
                $imagetype = ImageUpload::checkType($image);
                //dd($imagetype);

                if($imagesize == 'yes' && $imagetype == 'yes')
                {

                    $post = new Printer;
                    $post->name = $request->input('printername');
                    $post->brand_id = $request->input('brandid');
                    $post->category_id = $request->input('printtype');
                    $post->description = $request->input('description');
                    $post->printer_code = $request->input('printercode');

                    $width = 200;
                    $height = 200;

                    if ($request->hasfile('image'))
                    {
                        //$image = $request->file('image');

                        $imageupload = ImageUpload::uploadImage($image,$width,$height);
                        $post->image = $imageupload;

                    }
                    $post->save();

                    if ($request->hasfile('image'))
                    {
                        //$image = $request->file('image');

                        $imageupload = ImageUpload::uploadImage($image,$width,$height);

                        // get extention
                        $ext = pathinfo(storage_path().$imageupload, PATHINFO_EXTENSION);

                        $post = new ImageUp;
                        $post->name = $imageupload;
                        $post->alt = '';
                        $post->type = $ext;
                        $post->save();
                    }
                    return redirect()->back()->with('alert', 'Insert Success!');

                }
                else
                {
                    return redirect()->back()->with('alert2', 'Unsupported image type or size! It must be lower 2Mb and type is jpg, jpeg, or png!');
                }
            }
            else
            {
                return redirect()->back()->with('alert2', 'Out of length Description field!');
            }
        }
        else
        {
            return redirect()->back()->with('alert2', 'Use another name. Insert Not Success!');
        }

    }


    /**
     * Update the specified Printer in storage.
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return \Illuminate\Http\Response 
     */
    public static function updaterecord($request, $id){


        $descrip = $request->input('description');
        $calString = strlen($descrip);

        if($calString < 1000)
        {
            $image = $request->file('image');

                $post = Printer::find($id);
                $post->name = $request->input('printername');
                $post->brand_id = $request->input('brandid');
                $post->category_id = $request->input('printtype');
                $post->description = $request->input('description');
                $post->printer_code = $request->input('printercode');

                $width = 200;
                $height = 200;

                if ($request->hasfile('image'))
                {
                    //$image = $request->file('image');

                    $imageupload = ImageUpload::uploadImage($image,$width,$height);
                    $post->image = $imageupload;

                    $oldfilename = $post->image;

                    $post->image =  $imageupload;

                    Storage::delete($oldfilename);
                }
                $post->save();

                //save image details for image_ups table
                if ($request->hasfile('image'))
                {
                    //$image = $request->file('image');

                    $imageupload = ImageUpload::uploadImage($image,$width,$height);

                    // get extention
                    $ext = pathinfo(storage_path().$imageupload, PATHINFO_EXTENSION);

                    $post = new ImageUp;
                    $post->name = $imageupload;
                    $post->alt = '';
                    $post->type = $ext;
                    $post->save();
                }

                $printersNew = Printer::all();
                return view('printer.index',['printer'=> $printersNew]);
        }
        else
        {
            return redirect()->back()->with('alert2', 'Out of length Description field!');
        }

    }

    /**
     * Remove the specified printer in storage.
     *
     * @param  \Illuminate\Http\Request  $printer
     * @return \Illuminate\Http\Response $findPrinter
     */
    public static function deleterecord($printer){

        $findPrinter = Printer::find( $printer->id );
        if($findPrinter->delete()) {

            return redirect('printer')->with('alert', 'Deleted!');
        }

            return redirect('printer')->with('error','Printer could not be deleted!');

    }
}
