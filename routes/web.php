<?php

use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| 
|
*/

//default route...


//for test mail
Route::get('/test', function()
{
	$beautymail = app()->make(Snowfire\Beautymail\Beautymail::class);
    $beautymail->send('email.welcome', [], function($message)
    {
        $message
			->from('kasunsevensigns@gmail.com')
			->to('roshanharshanadr@gmail.com', 'John Smith')
			->subject('Welcome!');
    });

});
//for end test mail




Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//home routes...
Route::get('/home', 'HomeController@index')->name('home');

//back end routes...
Route::group(['middleware' => ['auth']], function(){
    
        Route::resource('admin','AdminController');
    
        //brand forms route...
        Route::resource('brand','BrandsController');
    
        //printer forms route...
        Route::resource('printer','PrintersController');
    
        //Product forms route...
        Route::resource('product','ProductsController');
    
        //Category forms route...
        Route::resource('category','CategoriesController');
    
        //Color forms route...
        Route::resource('color','ColorsController');
    
        //FAQ forms route...
        Route::resource('question','QuestionsController');

        //User forms route...
        Route::resource('user','QuestionsController');
    
        //Answer forms route...
        Route::resource('answer','AnswersController');
        Route::get('/answer/create/{question}','AnswersController@create');
        Route::get('/answer/{question}/{approve}','AnswersController@updateApprove');
        Route::delete('/question/delete/{question}', 'QuestionsController@destroyDashboard');
    
        //Diliveryrate forms route...
        Route::resource('diliveryrate','DeliveryratesController');
    
        //User register forms route...
        Route::resource('user','UsersController');
        Route::get('/user/change/privileges/{user}','UsersController@privileges');
        Route::post('/user/privileges/update','UsersController@updateprivileges');

        //for repors
        Route::get('/orderreport','ReportsController@orderreport');
        Route::get('/paymentreport','ReportsController@paymentreport');
        Route::get('/salesreport','ReportsController@salesreport');
        Route::get('/mail','ReportsController@mail');
        Route::post('/searchsales','ReportsController@searchsales')->name('searchsales');
        Route::get('/order/view/{orderId}','ReportsController@viewOrderDetails');

        Route::get('/searchsales', function () {

            return redirect('salesreport');

        });

});

//Aauth routes...
Route::resource('passwords','UseraccountController');

//model route...
Route::get('/model', 'ModelController@index');

//for search results...
Route::get('/search={search_id}','SearchController@index');
Route::get('/search/{brandId}/{search_id}/{catId}','SearchController@indexOther');
Route::get('/search={section}/{search_id}','SearchController@other');
Route::get('/toner-cartridges/{brandname}/{printername}/{printerId}.html','SearchController@tonerCartridgeShow');
Route::get('/ink-cartridges/{brandname}/{printername}/{printerId}.html','SearchController@inkCartridgeShow');
Route::get('/ribbon-cartridges/{brandname}/{printername}/{printerId}.html','SearchController@ribbonCartridgeShow');
Route::get('/bulk-ink/{brandname}/{printername}/{printerId}.html','SearchController@bulkinkCartridgeShow');
Route::get('/powder/{brandname}/{printername}/{printerId}.html','SearchController@powderCartridgeShow');
Route::get('/{printer_name}.html','SearchController@printerShow');

//for customer...
Route::get('/customer/view/{userId}','CustomerController@forviewOrders');

Route::get('/checkout/cart','Shopingcartscontroller@getCart');
Route::get('/add-to-cart/{id}/{UrlPast}/{count}/{stock}','Shopingcartscontroller@getAddToCart');
Route::get('/add-to-cart/product/{id}/{stock}/{count}/{UrlPast}','Shopingcartscontroller@getAddToCartonlyproduct');
// Route::get('/add-to-cart/{id}/{count}/{category}/{brand}/{printer}','Shopingcartscontroller@getAddToCartTwo');
Route::get('/add-to-cart/for-this-printer/{id}/{count}/printer/{stock}','Shopingcartscontroller@getAddToCartTwo');
Route::get('/change/product/{id}/{count}/{stock}/{side}','Shopingcartscontroller@getAddToCartChange');
Route::get('/contact','Shopingcartscontroller@contact');
Route::get('/city/{id}','Shopingcartscontroller@getcityDetails');

Route::get('/reduce/{id}','Shopingcartscontroller@getReduceByOne');
Route::get('/remove/{id}/{stock}/{count}','Shopingcartscontroller@getRemoveItem');
Route::get('/checkout','Shopingcartscontroller@getCheckout');
Route::get('/{category}/{brandname}.html','SearchController@getBrands');
//Route::post('/checkout','Shopingcartscontroller@postCheckout');
Route::post('/checkout',[
    'uses' => 'Shopingcartscontroller@postCheckout',
    'as' => 'checkout'
]);
Route::post('/checkemail',[
    'uses' => 'Shopingcartscontroller@checkemail',
    'as' => 'checkemail'
]);
Route::post('/delivery',[
    'uses' => 'Shopingcartscontroller@delivery',
    'as' => 'delivery'
]);
Route::any('/finished',[
    'uses' => 'Shopingcartscontroller@finished',
    'as' => 'finished'
]);

Route::any('/getapidetails','Shopingcartscontroller@getapidetails')->name('getapidetails');
Route::post('/getcashdetails','Shopingcartscontroller@getcashdetails')->name('getcashdetails');
Route::post('/customerQuestion', 'CustomerController@customerQuestion')->name('customerQuestion');
Route::post('/done', 'Shopingcartscontroller@done')->name('done');
Route::get('/checkemail', function () {

    return redirect('checkout');

});
Route::get('/delivery', function () {

    return redirect('delivery');

});
