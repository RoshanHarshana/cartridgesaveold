-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~xenial.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 30, 2019 at 06:21 PM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.2.14-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cartridgesavedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `street_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` int(11) NOT NULL,
  `address_type` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `phone` int(11) NOT NULL,
  `street_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `street_name`, `city`, `province`, `postal_code`, `address_type`, `company_name`, `user_id`, `order_id`, `phone`, `street_no`, `created_at`, `updated_at`) VALUES
(1, 'Malwaththa', 'Kurunegala', 'Sabaragamuwa', 71000, NULL, 'dhfhfhf', 3, 2, 713663398, 'u/32', '2019-01-29 02:32:14', '2019-01-29 02:32:14'),
(2, 'Malwaththa', 'Kurunegala', 'Sabaragamuwa', 71000, NULL, 'Harshana', 3, 3, 713663398, 'u/32', '2019-01-30 07:12:30', '2019-01-30 07:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `answer` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `answer`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, 'This is good for your usage sir...', '2019-01-30', '2019-01-30 00:00:11', '2019-01-30 00:00:11'),
(2, 2, 'Yes!', '2019-01-30', '2019-01-30 00:00:34', '2019-01-30 00:00:34'),
(3, 3, 'Your own personal product dashboard with easier access to support, product and service information, relevant solutions, and more just for you! \r\nRegister for an account or sign in to see all the benefits now!', '2019-01-30', '2019-01-30 00:01:38', '2019-01-30 00:01:38'),
(4, 2, 'Your own personal product dashboard with easier access to support, product and service information, relevant solutions, and more just for you! \r\nRegister for an account or sign in to see all the benefits now!', '2019-01-30', '2019-01-30 00:01:52', '2019-01-30 00:01:52'),
(5, 2, 'Your own personal product dashboard with easier access to support, product and service information, relevant solutions, and more just for you! \r\nRegister for an account or sign in to see all the benefits now!', '2019-01-30', '2019-01-30 00:02:43', '2019-01-30 00:02:43');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `value`, `product_id`, `created_at`, `updated_at`) VALUES
(8, 'color_type', 'Yellow', 2, '2019-01-28 03:52:40', '2019-01-28 03:52:40'),
(9, 'duty_cycle', 'reeryrg', 2, '2019-01-28 03:52:40', '2019-01-28 03:52:40'),
(10, 'manufacture_part_no', 'teteet3334344', 2, '2019-01-28 03:52:40', '2019-01-28 03:52:40'),
(11, 'product_part_no', 'dgfdgdg32423', 2, '2019-01-28 03:52:40', '2019-01-28 03:52:40'),
(12, 'compatibility', 'Compatible', 2, '2019-01-28 03:52:41', '2019-01-28 03:52:41'),
(13, 'related_compatibility', '1', 2, '2019-01-28 03:52:41', '2019-01-28 03:52:41'),
(14, 'related_products', '2,4,3', 2, '2019-01-28 03:52:41', '2019-01-28 03:52:41'),
(22, 'color_type', 'futyty', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(23, 'duty_cycle', 'tetet', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(24, 'manufacture_part_no', 'ertertret', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(25, 'product_part_no', 'rtertyr', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(26, 'compatibility', 'Genuine', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(27, 'related_compatibility', '3', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(28, 'related_products', '3,2', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(36, 'color_type', 'bgfhfg', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(37, 'duty_cycle', 'fhfghjgfh', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(38, 'manufacture_part_no', 'fgjgjg', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(39, 'product_part_no', 'hhgjghj', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(40, 'compatibility', 'Compatible', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(41, 'related_compatibility', '4', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(42, 'related_products', '2,4', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(43, 'color_type', 'Red', 1, '2019-01-28 05:14:35', '2019-01-28 05:14:35'),
(44, 'duty_cycle', 'Enter duty cycle...', 1, '2019-01-28 05:14:35', '2019-01-28 05:14:35'),
(45, 'manufacture_part_no', '645765hp', 1, '2019-01-28 05:14:36', '2019-01-28 05:14:36'),
(46, 'product_part_no', 'hp564565', 1, '2019-01-28 05:14:36', '2019-01-28 05:14:36'),
(47, 'compatibility', 'Compatible', 1, '2019-01-28 05:14:36', '2019-01-28 05:14:36'),
(48, 'related_compatibility', '2', 1, '2019-01-28 05:14:36', '2019-01-28 05:14:36'),
(49, 'related_products', '2,4,3', 1, '2019-01-28 05:14:36', '2019-01-28 05:14:36');

-- --------------------------------------------------------

--
-- Table structure for table `attributesets`
--

CREATE TABLE `attributesets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributesets`
--

INSERT INTO `attributesets` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'color_type', 'color_type', '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(2, 'duty_cycle', 'duty_cycle', '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(3, 'manufacture_part_no', 'manufacture_part_no', '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(4, 'product_part_no', 'product_part_no', '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(5, 'compatibility', 'compatibility', '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(6, 'related_compatibility', 'related_compatibility', '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(7, 'related_products', 'related_products', '2019-01-28 02:06:00', '2019-01-28 02:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `attributeset_catergory`
--

CREATE TABLE `attributeset_catergory` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Hp', 'This is a Hp brand.....', '1548661795.png', '2019-01-28 02:19:55', '2019-01-28 02:19:55'),
(2, 'Brother', 'This is a Brother brand...', '1548661844.png', '2019-01-28 02:20:44', '2019-01-28 02:20:44'),
(3, 'Canon', 'This is a Canon Brands....', '1548661889.png', '2019-01-28 02:21:29', '2019-01-28 02:21:29');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maincat_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `maincat_id`, `created_at`, `updated_at`) VALUES
(1, 'ink-cartridges', NULL, '2019-01-28 02:06:02', '2019-01-28 02:06:02'),
(2, 'toner-cartridges', NULL, '2019-01-28 02:06:02', '2019-01-28 02:06:02'),
(3, 'ribbon-cartridges', NULL, '2019-01-28 02:06:02', '2019-01-28 02:06:02');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'black', '2019-01-28 02:06:02', '2019-01-28 02:06:02'),
(2, 'cyan', '2019-01-28 02:06:02', '2019-01-28 02:06:02'),
(3, 'magenta', '2019-01-28 02:06:02', '2019-01-28 02:06:02'),
(4, 'yellow', '2019-01-28 02:06:02', '2019-01-28 02:06:02'),
(5, 'color', '2019-01-28 02:06:02', '2019-01-28 02:06:02');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_rates`
--

CREATE TABLE `delivery_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `town` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_first_kg` double(10,2) NOT NULL,
  `price_after_first_kg` double(10,2) DEFAULT NULL,
  `cod_price_first_kg` double(10,2) NOT NULL,
  `cod_price_after_first_kg` double(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `name`, `product_id`, `created_at`, `updated_at`) VALUES
(5, 'Cat', 2, '2019-01-28 03:52:40', '2019-01-28 03:52:40'),
(6, 'Dog', 2, '2019-01-28 03:52:40', '2019-01-28 03:52:40'),
(10, 'gkhkjk', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(11, 'hjkhjk', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(12, 'hjkhjhjk', 4, '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(17, 'Car', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(18, 'Bus', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(19, 'Van', 3, '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(20, 'One', 1, '2019-01-28 05:14:35', '2019-01-28 05:14:35'),
(21, 'Two', 1, '2019-01-28 05:14:35', '2019-01-28 05:14:35'),
(22, 'Three', 1, '2019-01-28 05:14:35', '2019-01-28 05:14:35'),
(23, 'Four', 1, '2019-01-28 05:14:35', '2019-01-28 05:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `image_ups`
--

CREATE TABLE `image_ups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_ups`
--

INSERT INTO `image_ups` (`id`, `name`, `alt`, `type`, `created_at`, `updated_at`) VALUES
(1, '1548661796.png', '', 'png', '2019-01-28 02:19:56', '2019-01-28 02:19:56'),
(2, '1548661844.png', '', 'png', '2019-01-28 02:20:45', '2019-01-28 02:20:45'),
(3, '1548661889.png', '', 'png', '2019-01-28 02:21:29', '2019-01-28 02:21:29'),
(4, '1548663621.jpg', '', 'jpg', '2019-01-28 02:50:21', '2019-01-28 02:50:21'),
(5, '1548665318.jpg', '', 'jpg', '2019-01-28 03:18:39', '2019-01-28 03:18:39'),
(6, '1548665374.jpg', '', 'jpg', '2019-01-28 03:19:34', '2019-01-28 03:19:34'),
(7, '1548665401.jpg', '', 'jpg', '2019-01-28 03:20:01', '2019-01-28 03:20:01'),
(8, '1548665484.jpg', '', 'jpg', '2019-01-28 03:21:25', '2019-01-28 03:21:25'),
(9, '1548665513.jpg', '', 'jpg', '2019-01-28 03:21:53', '2019-01-28 03:21:53'),
(10, '1548667155.jpg', '', 'jpg', '2019-01-28 03:49:15', '2019-01-28 03:49:15'),
(11, '1548667359.jpg', '', 'jpg', '2019-01-28 03:52:40', '2019-01-28 03:52:40'),
(12, '1548667647.jpg', '', 'jpg', '2019-01-28 03:57:28', '2019-01-28 03:57:28'),
(13, '1548667793.jpg', '', 'jpg', '2019-01-28 03:59:53', '2019-01-28 03:59:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_roles_table', 1),
(2, '2014_10_12_000001_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_10_30_071837_cteate_categories_table', 1),
(5, '2018_10_30_193816_create_brands_table', 1),
(6, '2018_11_01_191513_create_printers_table', 1),
(7, '2018_11_27_063334_create_products_table', 1),
(8, '2018_11_27_073024_create_colors_table', 1),
(9, '2018_11_27_073833_cteate_pages_table', 1),
(10, '2018_11_27_083809_cteate_features_table', 1),
(11, '2018_11_29_042535_create_orders_table', 1),
(12, '2018_11_29_042540_create_payments_table', 1),
(13, '2018_11_29_045020_create_order_products_table', 1),
(14, '2018_11_29_050646_create_addresses_table', 1),
(15, '2018_11_29_051216_create_delivery_rates_table', 1),
(16, '2018_11_29_053203_create_questions_table', 1),
(17, '2018_11_29_054002_create_answers_table', 1),
(18, '2018_12_05_052735_create_images_table', 1),
(19, '2018_12_05_054054_create_attributes_table', 1),
(20, '2018_12_05_054108_create_attributesets_table', 1),
(21, '2018_12_05_054208_create_catergory_attributeset_table', 1),
(22, '2018_12_05_055810_create_product_printer_table', 1),
(23, '2018_12_05_061658_create_permissions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `total` double(10,2) NOT NULL,
  `delivery_total` double(10,2) DEFAULT NULL,
  `total_qty` int(11) NOT NULL,
  `vat` double(10,2) NOT NULL,
  `payment_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `date`, `user_id`, `total`, `delivery_total`, `total_qty`, `vat`, `payment_type`, `note`, `status`, `created_at`, `updated_at`) VALUES
(1, '2019-01-29', 3, 900.00, NULL, 2, 35.00, NULL, NULL, 'pending', '2019-01-29 02:13:49', '2019-01-29 02:13:49'),
(2, '2019-01-29', 3, 900.00, NULL, 2, 35.00, NULL, NULL, 'pending', '2019-01-29 02:32:14', '2019-01-29 02:32:14'),
(3, '2019-01-30', 3, 400.00, NULL, 1, 20.00, NULL, NULL, 'pending', '2019-01-30 07:12:30', '2019-01-30 07:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `total` double(10,2) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_charge` double(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `color_id` int(10) UNSIGNED NOT NULL,
  `pages_nos` int(11) NOT NULL,
  `multiply` int(11) NOT NULL,
  `capacity` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `product_id`, `color_id`, `pages_nos`, `multiply`, `capacity`, `created_at`, `updated_at`) VALUES
(5, 2, 5, 500, 1, '2', '2019-01-28 03:52:39', '2019-01-28 03:52:39'),
(6, 2, 1, 300, 2, '1', '2019-01-28 03:52:39', '2019-01-28 03:52:39'),
(10, 4, 1, 500, 1, '800 ml', '2019-01-28 03:59:53', '2019-01-28 03:59:53'),
(15, 3, 2, 40, 1, '50 ml', '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(16, 3, 3, 50, 1, '50 ml', '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(17, 3, 4, 70, 1, '60 ml', '2019-01-28 04:01:43', '2019-01-28 04:01:43'),
(18, 1, 2, 200, 2, '40 ml', '2019-01-28 05:14:35', '2019-01-28 05:14:35'),
(19, 1, 3, 300, 1, '50 ml', '2019-01-28 05:14:35', '2019-01-28 05:14:35'),
(20, 1, 4, 250, 3, '60 ml', '2019-01-28 05:14:35', '2019-01-28 05:14:35'),
(21, 1, 5, 210, 2, '20 ml', '2019-01-28 05:14:35', '2019-01-28 05:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `payhere_amount` double(10,2) NOT NULL,
  `payhere_currency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_code` int(11) DEFAULT NULL,
  `md5sig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_msg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `index` tinyint(1) NOT NULL DEFAULT '1',
  `create` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `role_id`, `edit`, `index`, `create`, `delete`, `created_at`, `updated_at`) VALUES
(1, 'Category', 1, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(2, 'Color', 1, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(3, 'Brand', 1, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(4, 'Printer', 1, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(5, 'Product', 1, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(6, 'FAQ', 1, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(7, 'Rate', 1, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(8, 'User', 1, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(9, 'Category', 2, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(10, 'Color', 2, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(11, 'Brand', 2, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(12, 'Printer', 2, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(13, 'Product', 2, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(14, 'FAQ', 2, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(15, 'Rate', 2, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(16, 'User', 2, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(17, 'Category', 3, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(18, 'Color', 3, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(19, 'Brand', 3, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(20, 'Printer', 3, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(21, 'Product', 3, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(22, 'FAQ', 3, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(23, 'Rate', 3, 1, 1, 1, 1, '2019-01-28 02:06:01', '2019-01-28 02:06:01'),
(24, 'User', 3, 1, 1, 1, 1, '2019-01-28 02:06:02', '2019-01-28 02:06:02');

-- --------------------------------------------------------

--
-- Table structure for table `printers`
--

CREATE TABLE `printers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `printer_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `printers`
--

INSERT INTO `printers` (`id`, `name`, `image`, `brand_id`, `category_id`, `description`, `printer_code`, `created_at`, `updated_at`) VALUES
(1, 'Hp-blue', '1548663620.jpg', 1, 1, 'Looking for a versatile All-in-One printer with unrivaled performance, an easy to use home printer, or business printers with capabilities to meet your growing needs? Look no further than printers from HP. Whether it’s a stylish, affordable home printer you’re after that will give you professional quality, or dependable, cost-effective business printers to meet all your business needs without breaking the bank, HP has the right printer deals for you. From simple print jobs to complex workflows, HP has the printers to meet your needs.', 'hp001', '2019-01-28 02:50:21', '2019-01-28 02:51:45'),
(2, 'Hp-honey', '1548665318.jpg', 1, 2, 'As business printing needs change, HP is changing with them. The next generation of printing is smart, secure, in color, and completely transforming the copier experience. Leading the charge is the HP Color LaserJet A3 Multifunction printers. Our A3 MFPs and printers offer professional-quality results you expect from HP, maximum uptime and minimal interruptions, and the world’s most secure printing(1).', 'hp002', '2019-01-28 03:18:38', '2019-01-28 03:18:38'),
(3, 'Brother-hope', '1548665374.jpg', 2, 1, 'As business printing needs change, HP is changing with them. The next generation of printing is smart, secure, in color, and completely transforming the copier experience. Leading the charge is the HP Color LaserJet A3 Multifunction printers. Our A3 MFPs and printers offer professional-quality results you expect from HP, maximum uptime and minimal interruptions, and the world’s most secure printing(1).', 'br-001', '2019-01-28 03:19:34', '2019-01-28 03:19:34'),
(4, 'Brother-shoot', '1548665401.jpg', 2, 2, 'As business printing needs change, HP is changing with them. The next generation of printing is smart, secure, in color, and completely transforming the copier experience. Leading the charge is the HP Color LaserJet A3 Multifunction printers. Our A3 MFPs and printers offer professional-quality results you expect from HP, maximum uptime and minimal interruptions, and the world’s most secure printing(1).', 'br-002', '2019-01-28 03:20:01', '2019-01-28 03:20:16'),
(5, 'Canon-hurry 9134567', '1548665484.jpg', 3, 1, 'As business printing needs change, HP is changing with them. The next generation of printing is smart, secure, in color, and completely transforming the copier experience. Leading the charge is the HP Color LaserJet A3 Multifunction printers. Our A3 MFPs and printers offer professional-quality results you expect from HP, maximum uptime and minimal interruptions, and the world’s most secure printing(1).', 'cn-001', '2019-01-28 03:21:24', '2019-01-28 03:21:24'),
(6, 'Canon-hurry', '1548665513.jpg', 3, 2, 'As business printing needs change, HP is changing with them. The next generation of printing is smart, secure, in color, and completely transforming the copier experience. Leading the charge is the HP Color LaserJet A3 Multifunction printers. Our A3 MFPs and printers offer professional-quality results you expect from HP, maximum uptime and minimal interruptions, and the world’s most secure printing(1).', 'cn-002', '2019-01-28 03:21:53', '2019-01-28 03:21:53');

-- --------------------------------------------------------

--
-- Table structure for table `printer_product`
--

CREATE TABLE `printer_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `printer_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `printer_product`
--

INSERT INTO `printer_product` (`id`, `printer_id`, `product_id`, `created_at`, `updated_at`) VALUES
(5, 1, 2, NULL, NULL),
(6, 2, 2, NULL, NULL),
(10, 1, 4, NULL, NULL),
(11, 3, 4, NULL, NULL),
(16, 1, 3, NULL, NULL),
(17, 3, 3, NULL, NULL),
(18, 4, 3, NULL, NULL),
(19, 1, 1, NULL, NULL),
(20, 2, 1, NULL, NULL),
(21, 3, 1, NULL, NULL),
(22, 4, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `brand_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  `reorder_level` int(11) NOT NULL,
  `delivery_type` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat` double(10,2) NOT NULL,
  `weight` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category_id`, `brand_id`, `image`, `price`, `stock`, `reorder_level`, `delivery_type`, `description`, `url`, `vat`, `weight`, `created_at`, `updated_at`) VALUES
(1, 'HP OfficeJet Pro 8710 All-in-One Printer', 1, 1, '1548667154.jpg', 400.00, 40, 5, 0, 'As business printing needs change, HP is changing with them. The next generation of printing is smart, secure, in color, and completely transforming the copier experience. Leading the charge is the HP Color LaserJet A3 Multifunction printers. Our A3 MFPs and printers offer professional-quality results you expect from HP, maximum uptime and minimal interruptions, and the world’s most secure printing(1).', 'hp', 5.00, 49.98, '2019-01-28 03:49:15', '2019-01-28 03:49:15'),
(2, 'HP OfficeJet 5255 All-in-One Printer', 2, 1, '1548667358.jpg', 500.00, 50, 7, 0, 'As business printing needs change, HP is changing with them. The next generation of printing is smart, secure, in color, and completely transforming the copier experience. Leading the charge is the HP Color LaserJet A3 Multifunction printers. Our A3 MFPs and printers offer professional-quality results you expect from HP, maximum uptime and minimal interruptions, and the world’s most secure printing(1).', 'hp-dghfdgf', 3.00, 50.00, '2019-01-28 03:52:39', '2019-01-28 03:52:39'),
(3, 'Brother MFC-L2710DW Compact Monochrome Laser', 2, 2, '1548667647.jpg', 600.00, 40, 3, 0, 'The Brother MFC-L2710DW All-in-One is a great choice for home or small offices that need full functionality in a compact footprint. The up to 50-sheet automatic document feeder enables multi-page copying and scanning while its class leading print speeds of up to 32 pages per minute (1) provide increased efficiency. Choose built-in wireless and Ethernet network interfaces for sharing with multiple users on your network or connect locally to a single computer via USB interface. The 250-sheet paper capacity helps improve efficiency with fewer refills and handles letter or legal sized paper, while Brother Genuine high-yield replacement cartridges deliver up to twice the standard print volume to help lower printing costs (3).', 'brother-fghfhg', 6.00, 90.00, '2019-01-28 03:57:27', '2019-01-28 03:57:27'),
(4, 'Brother MFC-9340CDW Color All-in-One Lawser Printer', 2, 2, '1548667792.jpg', 500.00, 50, 4, 0, 'The Brother MFC-L2710DW All-in-One is a great choice for home or small offices that need full functionality in a compact footprint. The up to 50-sheet automatic document feeder enables multi-page copying and scanning while its class leading print speeds of up to 32 pages per minute (1) provide increased efficiency. Choose built-in wireless and Ethernet network interfaces for sharing with multiple users on your network or connect locally to a single computer via USB interface. The 250-sheet paper capacity helps improve efficiency with fewer refills and handles letter or legal sized paper, while Brother Genuine high-yield replacement cartridges deliver up to twice the standard print volume to help lower printing costs (3).', 'brother-zvxcvv', 10.00, 699.96, '2019-01-28 03:59:52', '2019-01-28 03:59:52');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `approve` tinyint(1) NOT NULL DEFAULT '1',
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `customer_name`, `email`, `date`, `approve`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 'What is this product?', NULL, NULL, '2019-01-30', 1, 1, '2019-01-29 23:58:36', '2019-01-29 23:58:36'),
(2, 'Is it good for our hp printers man?', NULL, NULL, '2019-01-30', 1, 1, '2019-01-29 23:59:19', '2019-01-29 23:59:19'),
(3, 'Your own personal product dashboard with easier access to support, product and service information, relevant solutions, and more just for you! \r\nRegister for an account or sign in to see all the benefits now!', NULL, NULL, '2019-01-30', 1, 1, '2019-01-30 00:01:25', '2019-01-30 00:01:25'),
(6, '\';dhkldtmhkfmhlfg[hfh', 'Roshan', 'roshan@gmail.com', '2019-01-30', 1, 1, '2019-01-30 07:05:03', '2019-01-30 07:05:03'),
(7, 'hjghjkghkghk', 'Roshan', 'roshan@gmail.com', '2019-01-30', 1, 1, '2019-01-30 07:10:00', '2019-01-30 07:10:00'),
(8, 'bnm,bgm,bh.;m\';bn.,b.,\'./bj\'\\,jbn,jh', 'Roshan', 'roshan@gmail.com', '2019-01-30', 1, 1, '2019-01-30 07:10:44', '2019-01-30 07:10:44'),
(9, '121', 'Roshan', 'roshan@gmail.com', '2019-01-30', 1, 1, '2019-01-30 07:13:03', '2019-01-30 07:13:03'),
(10, 'hgkghkhjk', 'Roshan', 'roshan@gmail.com', '2019-01-30', 1, 1, '2019-01-30 07:17:20', '2019-01-30 07:17:20');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2019-01-28 02:05:59', '2019-01-28 02:05:59'),
(2, 'User', '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(3, 'Super Admin', '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(4, 'Customer', '2019-01-28 02:06:00', '2019-01-28 02:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', NULL, 'superadmin@gmail.com', '$2y$10$CcnqNfbGoXEgQi.gm1piXODj.yM99xXFat85gJrPhW/4vb72uXw4O', 3, NULL, '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(2, 'Admin', NULL, 'admin@gmail.com', '$2y$10$CcnqNfbGoXEgQi.gm1piXODj.yM99xXFat85gJrPhW/4vb72uXw4O', 1, NULL, '2019-01-28 02:06:00', '2019-01-28 02:06:00'),
(3, 'Roshan', 'Harshana', 'danushkajayarathna123789@gmail.com', '$2y$10$MT1B8eTzlCRYxyLkc/W4qu.RD9tSV8EH4Yflvj0uK5T.syXfkV35W', 4, NULL, '2019-01-29 02:08:29', '2019-01-29 02:09:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_user_id_foreign` (`user_id`),
  ADD KEY `addresses_order_id_foreign` (`order_id`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_question_id_foreign` (`question_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attributes_product_id_foreign` (`product_id`);

--
-- Indexes for table `attributesets`
--
ALTER TABLE `attributesets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributeset_catergory`
--
ALTER TABLE `attributeset_catergory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attributeset_catergory_attribute_id_foreign` (`attribute_id`),
  ADD KEY `attributeset_catergory_category_id_foreign` (`category_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_rates`
--
ALTER TABLE `delivery_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `features_product_id_foreign` (`product_id`);

--
-- Indexes for table `image_ups`
--
ALTER TABLE `image_ups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_product_id_foreign` (`product_id`),
  ADD KEY `pages_color_id_foreign` (`color_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_order_id_foreign` (`order_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `printers`
--
ALTER TABLE `printers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `printers_name_unique` (`name`),
  ADD KEY `printers_brand_id_foreign` (`brand_id`),
  ADD KEY `printers_category_id_foreign` (`category_id`);

--
-- Indexes for table `printer_product`
--
ALTER TABLE `printer_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `printer_product_printer_id_foreign` (`printer_id`),
  ADD KEY `printer_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_name_unique` (`name`),
  ADD UNIQUE KEY `products_url_unique` (`url`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_product_id_foreign` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `attributesets`
--
ALTER TABLE `attributesets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `attributeset_catergory`
--
ALTER TABLE `attributeset_catergory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `delivery_rates`
--
ALTER TABLE `delivery_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `image_ups`
--
ALTER TABLE `image_ups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `printers`
--
ALTER TABLE `printers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `printer_product`
--
ALTER TABLE `printer_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attributes`
--
ALTER TABLE `attributes`
  ADD CONSTRAINT `attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attributeset_catergory`
--
ALTER TABLE `attributeset_catergory`
  ADD CONSTRAINT `attributeset_catergory_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attributeset_catergory_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `features`
--
ALTER TABLE `features`
  ADD CONSTRAINT `features_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pages_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `printers`
--
ALTER TABLE `printers`
  ADD CONSTRAINT `printers_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `printers_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `printer_product`
--
ALTER TABLE `printer_product`
  ADD CONSTRAINT `printer_product_printer_id_foreign` FOREIGN KEY (`printer_id`) REFERENCES `printers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `printer_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
