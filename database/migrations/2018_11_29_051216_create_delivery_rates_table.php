<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('town');
            $table->float('price_first_kg', 10, 2)->default(0.00);
            $table->float('price_after_first_kg', 10, 2)->default(0.00);
            $table->float('cod_price_first_kg', 10, 2)->default(0.00);
            $table->float('cod_price_after_first_kg', 10, 2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_rates');
    }
}
