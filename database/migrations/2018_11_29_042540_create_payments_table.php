<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_type');
            $table->integer('merchant_id')->nullable();
            $table->integer('payment_id')->nullable();
            $table->float('payhere_amount', 10, 2);
            $table->string('payhere_currency', 10)->nullable();
            $table->integer('status_code')->nullable();
            $table->string('md5sig')->nullable();
            $table->string('custom_msg')->nullable();
            $table->date('date')->nullable();
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
