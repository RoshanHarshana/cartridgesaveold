<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddNewColumnsToDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_rates', function (Blueprint $table) {
            
            $table->string('province')->nullable();
            $table->string('main_city')->nullable();
            $table->string('code')->nullable();
            $table->integer('post_code')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_rates', function (Blueprint $table) {
            
            $table->dropColumn('province');
            $table->dropColumn('main_city');
            $table->dropColumn('code');
            $table->dropColumn('post_code');
            
        });
    }
}
