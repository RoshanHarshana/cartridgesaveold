<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\Role([
            'id' => 1,
            'name' => 'Admin'
        ]);
        $role->save();

        $rolewo = new \App\Role([
            'id' => 2,
            'name' => 'User'
        ]);
        $rolewo->save();

        $rolesuper = new \App\Role([
            'id' => 3,
            'name' => 'Super Admin'
        ]);
        $rolesuper->save();

        $rolecustom = new \App\Role([
            'id' => 4,
            'name' => 'Customer'
        ]);
        $rolecustom->save();
    }
}
