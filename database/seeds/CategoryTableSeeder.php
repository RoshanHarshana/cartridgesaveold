<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $category = new \App\Category([
            'id' => 1,
            'name' => 'ink-cartridges'
        ]);
        $category->save();

        $category = new \App\Category([
            'id' => 2,
            'name' => 'toner-cartridges'
        ]);
        $category->save();

        $category = new \App\Category([
            'id' => 3,
            'name' => 'ribbon-cartridges'
        ]);
        $category->save();

        $category = new \App\Category([
            'id' => 4,
            'name' => 'bulk-ink'
        ]);
        $category->save();

        $category = new \App\Category([
            'id' => 5,
            'name' => 'powder'
        ]);
        $category->save();
    }
}
