<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
 
        $role = Role::findById(1);


        $permission = Permission::findById(1);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(2);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(3);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(4);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(5);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(6);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(7);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(8);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(9);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(10);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(11);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(12);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(13);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(14);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(15);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(16);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(17);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(18);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(19);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(20);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(21);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(22);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(23);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(24);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(25);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(26);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(27);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(28);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(29);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(30);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(31);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(32);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(33);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(34);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(35);
        $role->givePermissionTo($permission);

        $permission = Permission::findById(36);
        $role->givePermissionTo($permission);

    }
}
