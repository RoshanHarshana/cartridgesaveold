<?php

use Illuminate\Database\Seeder;

class AttributesetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attribute = new \App\Attributeset([
            'name' => 'color_type',
            'description' => 'color_type'
        ]);
        $attribute->save();

        $attribute = new \App\Attributeset([
            'name' => 'duty_cycle',
            'description' => 'duty_cycle'
        ]);
        $attribute->save();

        $attribute = new \App\Attributeset([
            'name' => 'manufacture_part_no',
            'description' => 'manufacture_part_no'
        ]);
        $attribute->save();

        $attribute = new \App\Attributeset([
            'name' => 'product_part_no',
            'description' => 'product_part_no'
        ]);
        $attribute->save();

        $attribute = new \App\Attributeset([
            'name' => 'compatibility',
            'description' => 'compatibility'
        ]);
        $attribute->save();

        $attribute = new \App\Attributeset([
            'name' => 'related_compatibility',
            'description' => 'related_compatibility'
        ]);
        $attribute->save();

        $attribute = new \App\Attributeset([
            'name' => 'related_products',
            'description' => 'related_products'
        ]);
        $attribute->save();
    }
}
