<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User([
            'id' => 1,
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'password' => '$2y$10$CcnqNfbGoXEgQi.gm1piXODj.yM99xXFat85gJrPhW/4vb72uXw4O',
            'role_id' => 3
        ]);
        $user->save();
        $user = new \App\User([
            'id' => 2,
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => '$2y$10$CcnqNfbGoXEgQi.gm1piXODj.yM99xXFat85gJrPhW/4vb72uXw4O',
            'role_id' => 1
        ]);
        $user->save();
    }
}
