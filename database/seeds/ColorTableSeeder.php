<?php

use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $color = new \App\Color([
            'id' => 1,
            'name' => 'black'
        ]);
        $color->save();

        $color = new \App\Color([
            'id' => 2,
            'name' => 'cyan'
        ]);
        $color->save();

        $color = new \App\Color([
            'id' => 3,
            'name' => 'magenta'
        ]);
        $color->save();

        $color = new \App\Color([
            'id' => 4,
            'name' => 'yellow'
        ]);
        $color->save();

        $color = new \App\Color([
            'id' => 5,
            'name' => 'color'
        ]);
        $color->save();

        $color = new \App\Color([
            'id' => 6,
            'name' => 'light cyan'
        ]);
        $color->save();

        $color = new \App\Color([
            'id' => 7,
            'name' => 'light magenta'
        ]);
        $color->save();
    }
}
