<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $role = new \Spatie\Permission\Models\Permission([
            'id' => 1,
            'name' => 'category add',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 2,
            'name' => 'category edit',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 3,
            'name' => 'category view',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 4,
            'name' => 'category delete',
        ]);
        $role->save();



        $role = new \Spatie\Permission\Models\Permission([
            'id' => 5,
            'name' => 'color add',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 6,
            'name' => 'color edit',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 7,
            'name' => 'color view',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 8,
            'name' => 'color delete',
        ]);
        $role->save();



        $role = new \Spatie\Permission\Models\Permission([
            'id' => 9,
            'name' => 'brand add',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 10,
            'name' => 'brand edit',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 11,
            'name' => 'brand view',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 12,
            'name' => 'brand delete',
        ]);
        $role->save();    



        $role = new \Spatie\Permission\Models\Permission([
            'id' => 13,
            'name' => 'printer add',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 14,
            'name' => 'printer edit',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 15,
            'name' => 'printer view',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 16,
            'name' => 'printer delete',
        ]);
        $role->save();



        $role = new \Spatie\Permission\Models\Permission([
            'id' => 17,
            'name' => 'product add',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 18,
            'name' => 'product edit',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 19,
            'name' => 'product view',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 20,
            'name' => 'product delete',
        ]);
        $role->save();



        $role = new \Spatie\Permission\Models\Permission([
            'id' => 21,
            'name' => 'question add',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 22,
            'name' => 'question edit',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 23,
            'name' => 'question view',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 24,
            'name' => 'question delete',
        ]);
        $role->save();


        $role = new \Spatie\Permission\Models\Permission([
            'name' => 'answer add',
            'id' => 25,
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 26,
            'name' => 'answer edit',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 27,
            'name' => 'answer view',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 28,
            'name' => 'answer delete',
        ]);
        $role->save();



        $role = new \Spatie\Permission\Models\Permission([
            'id' => 29,
            'name' => 'rates add',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 30,
            'name' => 'rates edit',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 31,
            'name' => 'rates view',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 32,
            'name' => 'rates delete',
        ]);
        $role->save();



        $role = new \Spatie\Permission\Models\Permission([
            'id' => 33,
            'name' => 'user add',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 34,
            'name' => 'user edit',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 35,
            'name' => 'user view',
        ]);
        $role->save();

        $role = new \Spatie\Permission\Models\Permission([
            'id' => 36,
            'name' => 'user delete',
        ]);
        $role->save();


    }
}
