<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $per = new \App\Permission([
            'name' => 'Category',
            'role_id' => '1'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Color',
            'role_id' => '1'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Brand',
            'role_id' => '1'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Printer',
            'role_id' => '1'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Product',
            'role_id' => '1'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'FAQ',
            'role_id' => '1'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Rate',
            'role_id' => '1'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'User',
            'role_id' => '1'
        ]);
        $per->save();


        

        
        $per = new \App\Permission([
            'name' => 'Category',
            'role_id' => '2'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Color',
            'role_id' => '2'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Brand',
            'role_id' => '2'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Printer',
            'role_id' => '2'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Product',
            'role_id' => '2'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'FAQ',
            'role_id' => '2'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Rate',
            'role_id' => '2'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'User',
            'role_id' => '2'
        ]);
        $per->save();





        $per = new \App\Permission([
            'name' => 'Category',
            'role_id' => '3'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Color',
            'role_id' => '3'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Brand',
            'role_id' => '3'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Printer',
            'role_id' => '3'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Product',
            'role_id' => '3'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'FAQ',
            'role_id' => '3'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'Rate',
            'role_id' => '3'
        ]);
        $per->save();

        $per = new \App\Permission([
            'name' => 'User',
            'role_id' => '3'
        ]);
        $per->save();
    }
}
