var Script = function () {

    $().ready(function() {

        var validate = false;
		     
		validate = $("#clientformstore").validate({
            rules: {		
                
                clientid: "required", 
                clientnic: "required", 
                clientname: "required", 
                clientaddress: "required",
                clientcontact: "required"				
 				
            },			
            messages: { 
                
                clientid: "Please enter Id!",
                clientnic: "Please enter nic!",
                clientname: "Please enter name!" ,
                clientaddress: "Please enter address!",							
                clientcontact: "Please enter contact number!"								                
            },

            submitHandler: function(form) { // for demo

            jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('client.store') }}",
                  method: 'post',
                  data: {
                    clientid: $('#clientid').val(),
                    clientnic: $('#clientnic').val(),
                    clientname: $('#clientname').val(),
                    clientaddress: $('#clientaddress').val(),
                    clientcontact: $('#clientcontact').val()
                    
                  },
                  
                  success: function(data){
                        jQuery.each(data.errors, function(key, value){
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>'+value+'</p>');
                        });
                    }
                    
                  });

            return false;
            }


         
        });





        validate = $("#clientformedit").validate({
            rules: {		
                
                clientid: "required", 
                clientnic: "required", 
                clientname: "required", 
                clientaddress: "required",
                clientcontact: "required"				
 				
            },			
            messages: { 
                
                clientid: "Please enter Id!",
                clientnic: "Please enter nic!",
                clientname: "Please enter name!" ,
                clientaddress: "Please enter address!",							
                clientcontact: "Please enter contact number!"								                
            },

            submitHandler: function(form) { // for demo

            jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('client.update') }}",
                  method: 'post',
                  data: {
                    clientid: $('#clientid').val(),
                    clientnic: $('#clientnic').val(),
                    clientname: $('#clientname').val(),
                    clientaddress: $('#clientaddress').val(),
                    clientcontact: $('#clientcontact').val()
                    
                  },
                  
                  success: function(data){
                        jQuery.each(data.errors, function(key, value){
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>'+value+'</p>');
                        });
                    }
                    
                  });

            return false;
            }


         
        });
	
    



        validate = $("#vehicleformstore").validate({
            rules: {		
                
                clientid: "required", 
                vehicleno: "required", 
                chassisno: "required", 
                engineno: "required",
                vehiclemodel: "required",
                vehiclemake: "required",
                vehiclecolor: "required",
                vehicleimage: "required"
                
 				
            },			
            messages: { 
                
                clientid: "Please select client!",
                vehicleno: "Please enter vehicle no!",
                chassisno: "Please enter chassis no!" ,
                engineno: "Please enter engine no!",							
                vehiclemodel: "Please enter vehicle model!",
                vehiclemake: "Please enter vehicle make!",
                vehiclecolor: "Please enter vehicle color!",
                vehicleimage: "Please enter vehicle image!"


            },

            submitHandler: function(form) { // for demo

            jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('vehicle.store') }}",
                  method: 'post',
                  data: {
                    clientid: $('#clientid').val(),
                    vehicleno: $('#vehicleno').val(),
                    chassisno: $('#chassisno').val(),
                    engineno: $('#engineno').val(),
                    vehiclemodel: $('#vehiclemodel').val(),
                    vehiclemake: $('#vehiclemake').val(),
                    vehiclecolor: $('#vehiclecolor').val(),
                    vehicleimage: $('#vehicleimage').val()
                  },
                  success: function(data){
                        jQuery.each(data.errors, function(key, value){
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>'+value+'</p>');
                        });
                    }
                    
                  });

            return false;
            }


         
        });







        validate = $("#vehicleformedit").validate({
            rules: {		
                
                clientid: "required", 
                vehicleno: "required", 
                chassisno: "required", 
                engineno: "required",
                vehiclemodel: "required",
                vehiclemake: "required",
                vehiclecolor: "required",
                vehicleimage: "required"
                
 				
            },			
            messages: { 
                
                clientid: "Please select client!",
                vehicleno: "Please enter vehicle no!",
                chassisno: "Please enter chassis no!" ,
                engineno: "Please enter engine no!",							
                vehiclemodel: "Please enter vehicle model!",
                vehiclemake: "Please enter vehicle make!",
                vehiclecolor: "Please enter vehicle color!",
                vehicleimage: "Please enter vehicle image!"


            },

            submitHandler: function(form) { // for demo

            jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('vehicle.update') }}",
                  method: 'post',
                  data: {
                    clientid: $('#clientid').val(),
                    vehicleno: $('#vehicleno').val(),
                    chassisno: $('#chassisno').val(),
                    engineno: $('#engineno').val(),
                    vehiclemodel: $('#vehiclemodel').val(),
                    vehiclemake: $('#vehiclemake').val(),
                    vehiclecolor: $('#vehiclecolor').val(),
                    vehicleimage: $('#vehicleimage').val()
                  },
                  success: function(data){
                        jQuery.each(data.errors, function(key, value){
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>'+value+'</p>');
                        });
                    }
                    
                  });

            return false;
            }


         
        });







        validate = $("#agreementformstore").validate({
            rules: {		
                
                
                agreementno: "required",
                agreementdate: "required",
                clientid: "required", 
                vehicleid: "required", 
                
 				
            },			
            messages: { 
                
                
                agreementno: "Please enter agreement no!",							
                agreementdate: "Please select agreement date!",
                clientid: "Please select client!",
                vehicleid: "Please select vehicle!"


            },

            submitHandler: function(form) { // for demo

            jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('agreement.store') }}",
                  method: 'post',
                  data: {
                    clientid: $('#clientid').val(),
                    vehicleid: $('#vehicleid').val(),
                    agreementno: $('#agreementno').val(),
                    agreementdate: $('#agreementdate').val()
                  },
                  success: function(data){
                        jQuery.each(data.errors, function(key, value){
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>'+value+'</p>');
                        });
                    }
                    
                  });

            return false;
            }


         
        });







        validate = $("#agreementformedit").validate({
            rules: {		
                
                clientid: "required", 
                vehicleid: "required", 
                agreementno: "required",
                agreementdate: "required"
                
 				
            },			
            messages: { 
                
                clientid: "Please select client!",
                vehicleid: "Please select vehicle!",
                agreementno: "Please enter agreement no!",							
                agreementdate: "Please select agreement date!"


            },

            submitHandler: function(form) { // for demo

            jQuery.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('agreement.update') }}",
                  method: 'post',
                  data: {
                    clientid: $('#clientid').val(),
                    vehicleid: $('#vehicleid').val(),
                    agreementno: $('#agreementno').val(),
                    agreementdate: $('#agreementdate').val()
                  },
                  success: function(data){
                        jQuery.each(data.errors, function(key, value){
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<p>'+value+'</p>');
                        });
                    }
                    
                  });

            return false;
            }


         
        });





        

      
    });


}();