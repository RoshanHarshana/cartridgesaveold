(function ($) {
	
	/*----------------------------
    START - Scroll to Top
    ------------------------------ */
	// $(window).on('scroll', function() {
	// 	if ($(this).scrollTop() > 600) {
	// 		$('.scrollToTop').fadeIn();
	// 	} else {
	// 		$('.scrollToTop').fadeOut();
	// 	}
	// });
	// $('.scrollToTop').on('click', function () {
	// 	$('html, body').animate({scrollTop : 0},2000);
	// 	return false;
	// });
	
	/*----------------------------
    START - Preloader
    ------------------------------ */
    jQuery(window).load(function () {
        jQuery("#preloader").fadeOut(500);
	});
	

	$('.search-select').on('change', function(data){
		let el = $(this).parent().find('.icon-wrap');
		let removeClass = el[0].classList[1];
		el.removeClass(removeClass);
		if(el.hasClass(this.value)){

		} else {
			el.addClass(this.value)
		}

	});

	$('.quantity-wrap .quantity-wrap-btn').on('click', function(){
		let input = $(this).parents('.single-item').find('.quantity-wrap-input');
		if($(this).hasClass('in')) {
			input.val(parseInt(input.val()) + 1);
		}

		if($(this).hasClass('de')) {
			if(input.val() > 1) {
				input.val(parseInt(input.val()) - 1);
			}
		}
	});











// add to cart
	$('.product-add-cart').on('click', function(e){
		e.preventDefault();
		let pid = $(this).attr('data-id');
		let url = $(this).attr('data-url');
		let input = $(this).parents('.single-item').find('.quantity-wrap-input').val();
		var baseUrl = $('#baseUrl').val();
		let stock = $(this).attr('data-stock');
		//alert(stock);
		// alert(input);
		if(Number(stock) > Number(input))
		{
			$.get(baseUrl+"/add-to-cart/"+pid+"/"+url+"/"+input+"/"+stock, function(data){
			
				$.fancybox.open({
					src  : '#confirm-content',
					type : 'inline',
					modal: true	
				});
				
				$( ".confirm-reload" ).on( "click", function() {
					location.reload();
				});
			});
		}
		else
		{
			$('#showCartridgestock').text(stock);

			$.fancybox.open({
				src  : '#confirm-stock',
				type : 'inline',
				modal: true	
			});
			
			$( ".confirm-reload" ).on( "click", function() {
				location.reload();
			});
		}
	});

	$('.product-add-cart-show').on('click', function(e){
		e.preventDefault();
		let pid = $(this).attr('data-id');
		//let url = $(this).attr('data-url');
		let input = $(this).parents('.single-item').find('.quantity-wrap-input').val();
		var baseUrl = $('#baseUrl').val();
		let stock = $(this).attr('data-stock');

		//alert(baseUrl+"/add-to-cart/for-this-printer/"+pid+"/"+input+"/printer");
		if(Number(stock) > Number(input))
		{
			$.get(baseUrl+"/add-to-cart/for-this-printer/"+pid+"/"+input+"/printer/"+stock, function(data){
			
				$.fancybox.open({
					src  : '#confirm-content',
					type : 'inline',
					modal: true	
				});
				
				$( ".confirm-reload" ).on( "click", function() {
					location.reload();
				});
			})
		}
		else
		{
			$('#showCartridgestock').text(stock);

			$.fancybox.open({
				src  : '#confirm-stock',
				type : 'inline',
				modal: true	
			});
			
			$( ".confirm-reload" ).on( "click", function() {
				location.reload();
			});
		}
	});

	$('.product-add-cart-mobile').on('click', function(e){
		e.preventDefault();
		let pid = $(this).attr('data-id');
		let url = $(this).attr('data-url');
		let input = $(this).parents('.single-item').find('.quantity-wrap-input-mobile').val();
		var baseUrl = $('#baseUrl').val();
		let stock = $(this).attr('data-stock');
		//alert(input);
		if(Number(stock) > Number(input))
		{
			$.get(baseUrl+"/add-to-cart/"+pid+"/"+url+"/"+input+"/"+stock, function(data){
			
				$.fancybox.open({
					src  : '#confirm-content',
					type : 'inline',
					modal: true	
				});
				
				$( ".confirm-reload" ).on( "click", function() {
					location.reload();
				});
			})
		}
		else
		{
			$('#showCartridgestock').text(stock);

			$.fancybox.open({
				src  : '#confirm-stock',
				type : 'inline',
				modal: true	
			});
			
			$( ".confirm-reload" ).on( "click", function() {
				location.reload();
			});
		}

	});

	$('.product-add-cart-mobile-show').on('click', function(e){
		e.preventDefault();
		let pid = $(this).attr('data-id');
		//let url = $(this).attr('data-url');
		let input = $(this).parents('.single-item').find('.quantity-wrap-input-mobile-show').val();
		var baseUrl = $('#baseUrl').val();
		let stock = $(this).attr('data-stock');

		//alert(baseUrl+"/add-to-cart/for-this-printer/"+pid+"/"+input+"/printer");
		if(Number(stock) > Number(input))
		{
			$.get(baseUrl+"/add-to-cart/for-this-printer/"+pid+"/"+input+"/printer/"+stock, function(data){
			
				$.fancybox.open({
					src  : '#confirm-content',
					type : 'inline',
					modal: true	
				});
				
				$( ".confirm-reload" ).on( "click", function() {
					location.reload();
				});
			})
		}
		else
		{
			$('#showCartridgestock').text(stock);

			$.fancybox.open({
				src  : '#confirm-stock',
				type : 'inline',
				modal: true	
			});
			
			$( ".confirm-reload" ).on( "click", function() {
				location.reload();
			});
		}

	});

	$('.product-add-cart-product').on('click', function(e){
		e.preventDefault();
		let pid = $(this).attr('data-id');
		let url = $(this).attr('data-url');
		let input = $(this).parents('.single-item').find('.quantity-wrap-input-product').val();
		var baseUrl = $('#baseUrl').val();
		let stock = $(this).attr('data-stock');

		if (Number(stock) > Number(input)) 
		{
			$.get(baseUrl+"/add-to-cart/product/"+pid+"/"+stock+"/"+input+"/"+url+".html", function(data){

				$.fancybox.open({
					src  : '#confirm-content',
					type : 'inline',
					modal: true	
				});
				
				$( ".confirm-reload" ).on( "click", function() {
					location.reload();
				});
			})
		} 
		else 
		{
			$('#showCartridgestock').text(stock);

			$.fancybox.open({
				src  : '#confirm-stock',
				type : 'inline',
				modal: true	
			});
			
			$( ".confirm-reload" ).on( "click", function() {
				location.reload();
			});
		}

	});

	$('.product-add-cart-mobile-product').on('click', function(e){
		e.preventDefault();
		let pid = $(this).attr('data-id');
		let url = $(this).attr('data-url');
		let input = $(this).parents('.single-item').find('.quantity-wrap-input-mobile-product').val();
		var baseUrl = $('#baseUrl').val();
		let stock = $(this).attr('data-stock');

		if (Number(stock) > Number(input)) 
		{
			$.get(baseUrl+"/add-to-cart/product/"+pid+"/"+stock+"/"+input+"/"+url+".html", function(data){

				$.fancybox.open({
					src  : '#confirm-content',
					type : 'inline',
					modal: true	
				});
				
				$( ".confirm-reload" ).on( "click", function() {
					location.reload();
				});
			})
		} 
		else 
		{
			$('#showCartridgestock').text(stock);

			$.fancybox.open({
				src  : '#confirm-stock',
				type : 'inline',
				modal: true	
			});
			
			$( ".confirm-reload" ).on( "click", function() {
				location.reload();
			});
		}

	});


	//for product change

	$('.product-cart-change').on('click', function(e){
		e.preventDefault();
		let pid = $(this).attr('data-id');
		let input = $(this).parents('.clearfix').find('.quantity-wrap-input-change').val();
		var baseUrl = $('#baseUrl').val();
		let stock = $(this).attr('data-stock');
		let side = $(this).attr('data-side');
		let qty = $(this).attr('data-qty');

		// alert(input);
		// alert(stock);
		// alert(side);
		// alert(qty);
		if(side == 'max')
		{
			if(Number(stock) > 1)
			{
				$.get(baseUrl+"/change/product/"+pid+"/"+Number(input)+"/"+Number(stock)+"/"+side, function(data){
					location.reload();
				})
			}
			else
			{
				$('#showCartridgestockcart').text(stock);

				$.fancybox.open({
					src  : '#confirm-stockCart',
					type : 'inline',
					modal: true	
				});
				
				$( ".confirm-reload" ).on( "click", function() {
					location.reload();
				});
			}
		}
		else
		{
			if(Number(qty) == 1)
			{
				$.get(baseUrl+"/remove/"+pid+"/"+Number(stock)+"/"+Number(input), function(data){
					location.reload();
				})
			}
			else
			{
				$.get(baseUrl+"/change/product/"+pid+"/"+Number(input)+"/"+Number(stock)+"/"+side, function(data){
					location.reload();
				})
			}
		}

	});

// add to cart end

// remove
$('.product-cart-remove').on('click', function(e){
	e.preventDefault();
	let pid = $(this).attr('data-id');
	let input = $(this).parents('.clearfix').find('.quantity-wrap-input-change').val();
	var baseUrl = $('#baseUrl').val();
	let stock = $(this).attr('data-stock');

	//alert(input);

	$.get(baseUrl+"/remove/"+pid+"/"+Number(stock)+"/"+Number(input), function(data){
		location.reload();
	})

});
// end remove






// delivery terms and conditions
$('.delivery-terms').on('click', function(e){
	e.preventDefault();

	$.fancybox.open({
		src  : '#confirm-terms',
		type : 'inline',
		modal: false	
	});
	
	$( ".confirm-reload" ).on( "click", function() {
		location.reload();
	});

});
//End delivery terms and conditions





	

	$('.questions-btn').on('click', function(){
		let form = $(this).attr('href');
		$(form).fadeIn();
	});

	var timer;

	$(".header-info-nav-bar ul li a").on("mouseover", function() {
		clearTimeout(timer);
			openSubmenu($(this));
	}).on("mouseleave", function() {
		timer = setTimeout(
			closeSubmenu($(this))
		, 1000);
	});	

	function openSubmenu(el) {
		$('.submenu-list#'+el.attr('data-id')).addClass("open");
		//el.children('.submenu-list').addClass("open");
	}
	function closeSubmenu(el) {
		$('.submenu-list#'+el.attr('data-id')).removeClass("open");
		//el.children('.submenu-list').removeClass("open");
	}

	$('.submenu-list').on('mouseenter', function(){
		$(this).addClass('open');
	}).on("mouseleave", function() {
		$(this).removeClass('open');
	});	


	$('input[type=radio]').on('change', function(e){
		if(this.value == 'cardpay') {
			$('.card-pay-wrap').fadeIn();
		} else {
			$('.card-pay-wrap').fadeOut();
		}
	});

	$('.mobile-menu-btn').on('click', function(){
		$(this).toggleClass('cross');
		$('.mobile-menu').toggleClass('open');
		$('html').toggleClass('overflow-hidden');
	})

	$(".mobile-menu ul li a").on("click", function() {
		$(this).parent().find('.drop').toggleClass('open');
	});

}(jQuery));
